class NotificationModel {
  final String imageUrl;
  final String title;
  final String subtitle;
  final String date;

  NotificationModel(this.imageUrl, this.title, this.subtitle, this.date);
}
