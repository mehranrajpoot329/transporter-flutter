class DrawerItemModel {
  final String title;
  final String iconUrl;
  final String routeToGo;

  DrawerItemModel(
      {required this.title, required this.iconUrl, required this.routeToGo});
}
