// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserAdapter extends TypeAdapter<User> {
  @override
  final int typeId = 0;

  @override
  User read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return User(
      id: fields[0] as int?,
      firstName: fields[1] as String?,
      lastName: fields[2] as String?,
      email: fields[3] as String?,
      phoneNo: fields[4] as String?,
      imageUrl: fields[5] as String?,
      lat: fields[6] as String?,
      lon: fields[7] as String?,
      fcmToken: fields[8] as String?,
      age: fields[9] as String?,
      gender: fields[10] as String?,
      userName: fields[11] as String?,
      etaNo: fields[12] as String?,
      userType: fields[13] as String?,
      token: fields[14] as String?,
      jobOffered: fields[15] as String?,
      cnicBack: fields[17] as String?,
      cnicFront: fields[16] as String?,
      licenceBack: fields[19] as String?,
      licenceFront: fields[18] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, User obj) {
    writer
      ..writeByte(20)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.firstName)
      ..writeByte(2)
      ..write(obj.lastName)
      ..writeByte(3)
      ..write(obj.email)
      ..writeByte(4)
      ..write(obj.phoneNo)
      ..writeByte(5)
      ..write(obj.imageUrl)
      ..writeByte(6)
      ..write(obj.lat)
      ..writeByte(7)
      ..write(obj.lon)
      ..writeByte(8)
      ..write(obj.fcmToken)
      ..writeByte(9)
      ..write(obj.age)
      ..writeByte(10)
      ..write(obj.gender)
      ..writeByte(11)
      ..write(obj.userName)
      ..writeByte(12)
      ..write(obj.etaNo)
      ..writeByte(13)
      ..write(obj.userType)
      ..writeByte(14)
      ..write(obj.token)
      ..writeByte(15)
      ..write(obj.jobOffered)
      ..writeByte(16)
      ..write(obj.cnicFront)
      ..writeByte(17)
      ..write(obj.cnicBack)
      ..writeByte(18)
      ..write(obj.licenceFront)
      ..writeByte(19)
      ..write(obj.licenceBack);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
