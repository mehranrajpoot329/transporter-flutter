class OrdersModelData {
  int? id;
  int? vehicleId;
  String? capacity;
  String? note;
  String? rejectedBy;
  String? rejectedById;
  String? cancelledById;
  int? acceptedBy;
  String? status;
  String? state;
  String? startedAt;
  String? completedAt;
  String? price;
  String? tax;
  String? totalPrice;
  String? paymentMethod;
  bool? completedByVendor;
  bool? completedByCustomer;
  String? sourceLat;
  String? sourceLon;
  String? destinationLat;
  String? destinationLon;
  String? serviceId;
  int? customerId;
  int? vendorId;
  String? vehicle;
  String? mSourceLocationAddress;
  String? mDestinationLocationAddress;

  OrdersModelData({
    this.id,
    this.vehicleId,
    this.capacity,
    this.note,
    this.rejectedBy,
    this.rejectedById,
    this.cancelledById,
    this.acceptedBy,
    this.status,
    this.state,
    this.startedAt,
    this.completedAt,
    this.price,
    this.tax,
    this.totalPrice,
    this.paymentMethod,
    this.completedByVendor,
    this.completedByCustomer,
    this.sourceLat,
    this.sourceLon,
    this.destinationLat,
    this.destinationLon,
    this.serviceId,
    this.customerId,
    this.vendorId,
    this.vehicle,
  });
  OrdersModelData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    vehicleId = json['vehicle_id']?.toInt();
    capacity = json['capacity']?.toString();
    note = json['note']?.toString();
    rejectedBy = json['rejected_by']?.toString();
    rejectedById = json['rejected_by_id']?.toString();
    cancelledById = json['cancelled_by_id']?.toString();
    acceptedBy = json['accepted_by']?.toInt();
    status = json['status']?.toString();
    state = json['state']?.toString();
    startedAt = json['started_at']?.toString();
    completedAt = json['completed_at']?.toString();
    price = json['price']?.toString();
    tax = json['tax']?.toString();
    totalPrice = json['total_price']?.toString();
    paymentMethod = json['payment_method']?.toString();
    completedByVendor = json['completed_by_vendor'];
    completedByCustomer = json['completed_by_customer'];
    sourceLat = json['source_lat']?.toString();
    sourceLon = json['source_lon']?.toString();
    destinationLat = json['destination_lat']?.toString();
    destinationLon = json['destination_lon']?.toString();
    serviceId = json['service_id']?.toString();
    customerId = json['customer_id']?.toInt();
    vendorId = json['vendor_id']?.toInt();
    vehicle = json['vehicle']?.toString();
  }
}
