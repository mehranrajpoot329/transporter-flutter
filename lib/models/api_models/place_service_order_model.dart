import 'package:hive/hive.dart';
import 'package:transporter_customer/models/api_models/place_service_customer_data.dart';

part 'place_service_order_model.g.dart';

@HiveType(typeId: 2)
class PlaceServiceOrderModelData {
  @HiveField(0)
  String? status;
  @HiveField(1)
  String? state;
  @HiveField(2)
  String? startedAt;
  @HiveField(3)
  String? completedAt;
  @HiveField(4)
  bool? completedByVendor;
  @HiveField(5)
  bool? completedByCustomer;
  @HiveField(6)
  int? id;
  @HiveField(7)
  String? sourceLat;
  @HiveField(8)
  String? destinationLat;
  @HiveField(9)
  String? vehicleId;
  @HiveField(10)
  String? capacity;
  @HiveField(11)
  String? paymentMethod;
  @HiveField(12)
  String? sourceLon;
  @HiveField(13)
  String? destinationLon;
  @HiveField(14)
  int? customerId;
  @HiveField(15)
  PlaceServiceOrderModelDataCustomer? customer;

  PlaceServiceOrderModelData({
    this.status,
    this.state,
    this.startedAt,
    this.completedAt,
    this.completedByVendor,
    this.completedByCustomer,
    this.id,
    this.sourceLat,
    this.destinationLat,
    this.vehicleId,
    this.capacity,
    this.paymentMethod,
    this.sourceLon,
    this.destinationLon,
    this.customerId,
    this.customer,
  });

  PlaceServiceOrderModelData.fromJson(Map<String, dynamic> json) {
    status = json['status']?.toString();
    state = json['state']?.toString();
    startedAt = json['started_at']?.toString();
    completedAt = json['completed_at']?.toString();
    completedByVendor = json['completed_by_vendor'];
    completedByCustomer = json['completed_by_customer'];
    id = json['id']?.toInt();
    sourceLat = json['source_lat']?.toString();
    destinationLat = json['destination_lat']?.toString();
    vehicleId = json['vehicle_id']?.toString();
    capacity = json['capacity']?.toString();
    paymentMethod = json['payment_method']?.toString();
    sourceLon = json['source_lon']?.toString();
    destinationLon = json['destination_lon']?.toString();
    customerId = json['customer_id']?.toInt();
    customer = (json['customer'] != null)
        ? PlaceServiceOrderModelDataCustomer.fromJson(json['customer'])
        : null;
  }
}
