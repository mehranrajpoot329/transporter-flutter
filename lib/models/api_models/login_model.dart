class LoginModelData {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? imageUrl;
  String? userType;
  String? token;

  LoginModelData({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.imageUrl,
    this.userType,
    this.token,
  });
  LoginModelData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    firstName = json['first_name']?.toString();
    lastName = json['last_name']?.toString();
    email = json['email']?.toString();
    imageUrl = json['image_url']?.toString();
    userType = json['user_type']?.toString();
    token = json['token']?.toString();
  }
}
