// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'place_service_order_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PlaceServiceOrderModelDataAdapter
    extends TypeAdapter<PlaceServiceOrderModelData> {
  @override
  final int typeId = 2;

  @override
  PlaceServiceOrderModelData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PlaceServiceOrderModelData(
      status: fields[0] as String?,
      state: fields[1] as String?,
      startedAt: fields[2] as String?,
      completedAt: fields[3] as String?,
      completedByVendor: fields[4] as bool?,
      completedByCustomer: fields[5] as bool?,
      id: fields[6] as int?,
      sourceLat: fields[7] as String?,
      destinationLat: fields[8] as String?,
      vehicleId: fields[9] as String?,
      capacity: fields[10] as String?,
      paymentMethod: fields[11] as String?,
      sourceLon: fields[12] as String?,
      destinationLon: fields[13] as String?,
      customerId: fields[14] as int?,
      customer: fields[15] as PlaceServiceOrderModelDataCustomer?,
    );
  }

  @override
  void write(BinaryWriter writer, PlaceServiceOrderModelData obj) {
    writer
      ..writeByte(16)
      ..writeByte(0)
      ..write(obj.status)
      ..writeByte(1)
      ..write(obj.state)
      ..writeByte(2)
      ..write(obj.startedAt)
      ..writeByte(3)
      ..write(obj.completedAt)
      ..writeByte(4)
      ..write(obj.completedByVendor)
      ..writeByte(5)
      ..write(obj.completedByCustomer)
      ..writeByte(6)
      ..write(obj.id)
      ..writeByte(7)
      ..write(obj.sourceLat)
      ..writeByte(8)
      ..write(obj.destinationLat)
      ..writeByte(9)
      ..write(obj.vehicleId)
      ..writeByte(10)
      ..write(obj.capacity)
      ..writeByte(11)
      ..write(obj.paymentMethod)
      ..writeByte(12)
      ..write(obj.sourceLon)
      ..writeByte(13)
      ..write(obj.destinationLon)
      ..writeByte(14)
      ..write(obj.customerId)
      ..writeByte(15)
      ..write(obj.customer);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PlaceServiceOrderModelDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
