// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'place_service_customer_data.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PlaceServiceOrderModelDataCustomerAdapter
    extends TypeAdapter<PlaceServiceOrderModelDataCustomer> {
  @override
  final int typeId = 3;

  @override
  PlaceServiceOrderModelDataCustomer read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PlaceServiceOrderModelDataCustomer(
      id: fields[0] as String?,
      firstName: fields[1] as String?,
      lastName: fields[2] as String?,
      phoneNo: fields[3] as String?,
      email: fields[4] as String?,
      address: fields[5] as String?,
      lat: fields[6] as String?,
      lon: fields[7] as String?,
      userType: fields[8] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, PlaceServiceOrderModelDataCustomer obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.firstName)
      ..writeByte(2)
      ..write(obj.lastName)
      ..writeByte(3)
      ..write(obj.phoneNo)
      ..writeByte(4)
      ..write(obj.email)
      ..writeByte(5)
      ..write(obj.address)
      ..writeByte(6)
      ..write(obj.lat)
      ..writeByte(7)
      ..write(obj.lon)
      ..writeByte(8)
      ..write(obj.userType);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PlaceServiceOrderModelDataCustomerAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
