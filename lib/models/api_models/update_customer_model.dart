class UpdateCustomerModelData {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? password;
  String? imageUrl;

  UpdateCustomerModelData({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.password,
    this.imageUrl,
  });
  UpdateCustomerModelData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    firstName = json['first_name']?.toString();
    lastName = json['last_name']?.toString();
    email = json['email']?.toString();
    password = json['password']?.toString();
    imageUrl = json['image_url']?.toString();
  }
}
