import 'package:hive/hive.dart';

part 'place_service_customer_data.g.dart';

@HiveType(typeId: 3)
class PlaceServiceOrderModelDataCustomer {
  @HiveField(0)
  String? id;
  @HiveField(1)
  String? firstName;
  @HiveField(2)
  String? lastName;
  @HiveField(3)
  String? phoneNo;
  @HiveField(4)
  String? email;
  @HiveField(5)
  String? address;
  @HiveField(6)
  String? lat;
  @HiveField(7)
  String? lon;
  @HiveField(8)
  String? userType;

  PlaceServiceOrderModelDataCustomer({
    this.id,
    this.firstName,
    this.lastName,
    this.phoneNo,
    this.email,
    this.address,
    this.lat,
    this.lon,
    this.userType,
  });

  PlaceServiceOrderModelDataCustomer.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toString();
    firstName = json['first_name']?.toString();
    lastName = json['last_name']?.toString();
    phoneNo = json['phone_no']?.toString();
    email = json['email']?.toString();
    address = json['address']?.toString();
    lat = json['lat']?.toString();
    lon = json['lon']?.toString();
    userType = json['user_type']?.toString();
  }
}
