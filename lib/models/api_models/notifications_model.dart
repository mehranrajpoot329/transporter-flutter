import 'package:transporter_customer/enums/order_status.dart';

class NotificationsModelData {
  int? id;
  String? message;
  String? userType;
  int? userId;
  String? createdAt;
  String? updatedAt;
  String? status;

  NotificationsModelData({
    this.id,
    this.message,
    this.userType,
    this.userId,
    this.createdAt,
    this.updatedAt,
    this.status,
  });
  NotificationsModelData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    message = json['message']?.toString();
    userType = json['user_type']?.toString();
    userId = json['user_id']?.toInt();
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    status = OrderStatus.cancelled;

    ///my Own Added Data
  }
}
