class AddReviewModel {
  String? data;

  AddReviewModel({
    this.data,
  });
  AddReviewModel.fromJson(Map<String, dynamic> json) {
    data = json['data']?.toString();
  }
}
