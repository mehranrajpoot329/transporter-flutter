class OrderByIdModelData {
  int? id;
  String? source;
  String? destination;
  int? vehicleId;
  String? capacity;
  String? note;
  String? rejectedBy;
  String? rejectedById;
  String? cancelledById;
  String? acceptedBy;
  String? status;
  String? state;
  String? startedAt;
  String? completedAt;
  String? price;
  String? tax;
  String? totalPrice;
  String? paymentMethod;
  bool? completedByVendor;
  bool? completedByCustomer;
  String? serviceId;
  int? customerId;
  String? vendorId;
  String? vehicle;

  OrderByIdModelData({
    this.id,
    this.source,
    this.destination,
    this.vehicleId,
    this.capacity,
    this.note,
    this.rejectedBy,
    this.rejectedById,
    this.cancelledById,
    this.acceptedBy,
    this.status,
    this.state,
    this.startedAt,
    this.completedAt,
    this.price,
    this.tax,
    this.totalPrice,
    this.paymentMethod,
    this.completedByVendor,
    this.completedByCustomer,
    this.serviceId,
    this.customerId,
    this.vendorId,
    this.vehicle,
  });
  OrderByIdModelData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    source = json['source']?.toString();
    destination = json['destination']?.toString();
    vehicleId = json['vehicle_id']?.toInt();
    capacity = json['capacity']?.toString();
    note = json['note']?.toString();
    rejectedBy = json['rejected_by']?.toString();
    rejectedById = json['rejected_by_id']?.toString();
    cancelledById = json['cancelled_by_id']?.toString();
    acceptedBy = json['accepted_by']?.toString();
    status = json['status']?.toString();
    state = json['state']?.toString();
    startedAt = json['started_at']?.toString();
    completedAt = json['completed_at']?.toString();
    price = json['price']?.toString();
    tax = json['tax']?.toString();
    totalPrice = json['total_price']?.toString();
    paymentMethod = json['payment_method']?.toString();
    completedByVendor = json['completed_by_vendor'];
    completedByCustomer = json['completed_by_customer'];
    serviceId = json['service_id']?.toString();
    customerId = json['customer_id']?.toInt();
    vendorId = json['vendor_id']?.toString();
    vehicle = json['vehicle']?.toString();
  }
}

class OrderByIdModel {
  bool? error;
  String? message;
  OrderByIdModelData? data;

  OrderByIdModel({
    this.error,
    this.message,
    this.data,
  });
  OrderByIdModel.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message']?.toString();
    data = (json['data'] != null)
        ? OrderByIdModelData.fromJson(json['data'])
        : null;
  }
}
