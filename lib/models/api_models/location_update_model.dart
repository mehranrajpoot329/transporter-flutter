class LocationUpdateModelDataLocationUpdateData {
  String? lat;
  String? lon;
  int? userId;

  LocationUpdateModelDataLocationUpdateData({
    this.lat,
    this.lon,
    this.userId,
  });

  LocationUpdateModelDataLocationUpdateData.fromJson(
      Map<String, dynamic> json) {
    lat = json['lat']?.toString();
    lon = json['lon']?.toString();
    userId = json['user_id']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['lat'] = lat;
    data['lon'] = lon;
    data['user_id'] = userId;
    return data;
  }
}

class LocationUpdateModelData {
  LocationUpdateModelDataLocationUpdateData? locationUpdate;

  LocationUpdateModelData({
    this.locationUpdate,
  });

  LocationUpdateModelData.fromJson(Map<String, dynamic> json) {
    locationUpdate = (json['LOCATION_UPDATE'] != null)
        ? LocationUpdateModelDataLocationUpdateData.fromJson(
            json['LOCATION_UPDATE'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (locationUpdate != null) {
      data['LOCATION_UPDATE'] = locationUpdate!.toJson();
    }
    return data;
  }
}

class LocationUpdateModel {
  LocationUpdateModelData? data;

  LocationUpdateModel({
    this.data,
  });

  LocationUpdateModel.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? LocationUpdateModelData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['data'] = this.data?.toJson();
    return data;
  }
}
