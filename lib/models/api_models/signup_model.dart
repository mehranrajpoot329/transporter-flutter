class SignupModelData {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? address;
  String? phoneNo;
  String? userType;
  String? token;

  SignupModelData({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.address,
    this.phoneNo,
    this.userType,
    this.token,
  });
  SignupModelData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    firstName = json['first_name']?.toString();
    lastName = json['last_name']?.toString();
    email = json['email']?.toString();
    address = json['address']?.toString();
    phoneNo = json['phone_no']?.toString();
    userType = json['user_type']?.toString();
    token = json['token']?.toString();
  }
}

class SignupModel {
  SignupModelData? data;

  SignupModel({
    this.data,
  });
  SignupModel.fromJson(Map<String, dynamic> json) {
    data =
        (json['data'] != null) ? SignupModelData.fromJson(json['data']) : null;
  }
}
