class VehicleListModelData {
  int? id;
  String? vehicleName;
  int? rate;
  String? imageUrl;
  String? createdAt;
  String? updatedAt;
  String? vehicleId;
  late bool isSelected;

  VehicleListModelData({
    this.id,
    this.vehicleName,
    this.rate,
    this.imageUrl,
    this.createdAt,
    this.updatedAt,
    this.vehicleId,
    this.isSelected = false,
  });
  VehicleListModelData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    vehicleName = json['vehicle_name']?.toString();
    rate = json['rate']?.toInt();
    imageUrl = json['image_url']?.toString();
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    vehicleId = json['vehicle_id']?.toString();
    isSelected = false;
  }
}
