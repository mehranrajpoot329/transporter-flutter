class ContactUsModel {
  String? data;

  ContactUsModel({
    this.data,
  });
  ContactUsModel.fromJson(Map<String, dynamic> json) {
    data = json['data']?.toString();
  }
}
