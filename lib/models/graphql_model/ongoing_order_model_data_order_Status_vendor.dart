import 'package:hive_flutter/hive_flutter.dart';
import 'package:transporter_customer/network/end_points.dart';

part 'ongoing_order_model_data_order_Status_vendor.g.dart';

@HiveType(typeId: 6)
class OnGoingOrderModelDataORDERSTATUSVendor {
  @HiveField(0)
  String? firstName;
  @HiveField(1)
  String? lastName;
  @HiveField(2)
  String? phoneNo;
  @HiveField(3)
  String? imageUrl;
  // EndPoints.baseURL + 'profileImage' + imageURl;
  String get getImageURL => (imageUrl?.isNotEmpty ?? false)
      ? '${EndPoints.baseURL}profileImage$imageUrl'
      : EndPoints.placeHolder;
  String get getFullName => '$firstName $lastName';
  String get firebaseKey => '$firstName$lastName'.toLowerCase();

  OnGoingOrderModelDataORDERSTATUSVendor({
    this.firstName,
    this.lastName,
    this.phoneNo,
    this.imageUrl,
  });
  OnGoingOrderModelDataORDERSTATUSVendor.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name']?.toString();
    lastName = json['last_name']?.toString();
    phoneNo = json['phone_no']?.toString();
    imageUrl = json['image_url']?.toString();
  }
}
