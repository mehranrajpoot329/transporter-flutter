// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ongoing_order_model_data_order_status.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class OnGoingOrderModelDataORDERSTATUSAdapter
    extends TypeAdapter<OnGoingOrderModelDataORDERSTATUS> {
  @override
  final int typeId = 5;

  @override
  OnGoingOrderModelDataORDERSTATUS read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return OnGoingOrderModelDataORDERSTATUS(
      orderId: fields[0] as int?,
      vendorId: fields[1] as int?,
      acceptedBy: fields[2] as int?,
      customerId: fields[3] as int?,
      reason: fields[4] as String?,
      price: fields[5] as String?,
      tax: fields[6] as String?,
      totalPrice: fields[7] as String?,
      orderStatus: fields[8] as String?,
      vendorStatus: fields[9] as String?,
      vendor: fields[10] as OnGoingOrderModelDataORDERSTATUSVendor?,
    );
  }

  @override
  void write(BinaryWriter writer, OnGoingOrderModelDataORDERSTATUS obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.orderId)
      ..writeByte(1)
      ..write(obj.vendorId)
      ..writeByte(2)
      ..write(obj.acceptedBy)
      ..writeByte(3)
      ..write(obj.customerId)
      ..writeByte(4)
      ..write(obj.reason)
      ..writeByte(5)
      ..write(obj.price)
      ..writeByte(6)
      ..write(obj.tax)
      ..writeByte(7)
      ..write(obj.totalPrice)
      ..writeByte(8)
      ..write(obj.orderStatus)
      ..writeByte(9)
      ..write(obj.vendorStatus)
      ..writeByte(10)
      ..write(obj.vendor);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OnGoingOrderModelDataORDERSTATUSAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
