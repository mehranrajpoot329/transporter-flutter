import 'package:hive_flutter/hive_flutter.dart';
import 'package:transporter_customer/models/graphql_model/ongoing_order_model_data_order_Status_vendor.dart';

part 'ongoing_order_model_data_order_status.g.dart';

@HiveType(typeId: 5)
class OnGoingOrderModelDataORDERSTATUS {
  @HiveField(0)
  int? orderId;
  @HiveField(1)
  int? vendorId;
  @HiveField(2)
  int? acceptedBy;
  @HiveField(3)
  int? customerId;
  @HiveField(4)
  String? reason;
  @HiveField(5)
  String? price;
  @HiveField(6)
  String? tax;
  @HiveField(7)
  String? totalPrice;
  @HiveField(8)
  String? orderStatus;
  @HiveField(9)
  String? vendorStatus;
  @HiveField(10)
  OnGoingOrderModelDataORDERSTATUSVendor? vendor;

  OnGoingOrderModelDataORDERSTATUS({
    this.orderId,
    this.vendorId,
    this.acceptedBy,
    this.customerId,
    this.reason,
    this.price,
    this.tax,
    this.totalPrice,
    this.orderStatus,
    this.vendorStatus,
    this.vendor,
  });
  OnGoingOrderModelDataORDERSTATUS.fromJson(Map<String, dynamic> json) {
    orderId = json['order_id']?.toInt();
    vendorId = json['vendor_id']?.toInt();
    acceptedBy = json['accepted_by']?.toInt();
    customerId = json['customer_id']?.toInt();
    reason = json['reason']?.toString();
    price = json['price']?.toString();
    tax = json['tax']?.toString();
    totalPrice = json['total_price']?.toString();
    orderStatus = json['order_status']?.toString();
    vendorStatus = json['vendor_status']?.toString();
    vendor = (json['vendor'] != null)
        ? OnGoingOrderModelDataORDERSTATUSVendor.fromJson(json['vendor'])
        : null;
  }
}
