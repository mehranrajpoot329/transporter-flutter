// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ongoing_order_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class OnGoingOrderModelDataAdapter extends TypeAdapter<OnGoingOrderModelData> {
  @override
  final int typeId = 4;

  @override
  OnGoingOrderModelData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return OnGoingOrderModelData(
      orderStatus: fields[0] as OnGoingOrderModelDataORDERSTATUS?,
    );
  }

  @override
  void write(BinaryWriter writer, OnGoingOrderModelData obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.orderStatus);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OnGoingOrderModelDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
