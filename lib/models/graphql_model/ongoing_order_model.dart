import 'package:hive_flutter/hive_flutter.dart';
import 'package:transporter_customer/models/graphql_model/ongoing_order_model_data_order_status.dart';

part 'ongoing_order_model.g.dart';

@HiveType(typeId: 4)
class OnGoingOrderModelData {
  @HiveField(0)
  OnGoingOrderModelDataORDERSTATUS? orderStatus;

  OnGoingOrderModelData({
    this.orderStatus,
  });

  OnGoingOrderModelData.fromJson(Map<String, dynamic> json) {
    orderStatus = (json['ORDER_STATUS'] != null)
        ? OnGoingOrderModelDataORDERSTATUS.fromJson(json['ORDER_STATUS'])
        : null;
  }
}
