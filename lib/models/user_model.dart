import 'package:hive/hive.dart';
import 'package:transporter_customer/extensions/string_extension.dart';

part 'user_model.g.dart';

@HiveType(typeId: 0)
class User extends HiveObject {
  @HiveField(0)
  int? id;
  @HiveField(1)
  String? firstName;
  @HiveField(2)
  String? lastName;
  @HiveField(3)
  String? email;
  @HiveField(4)
  String? phoneNo;
  @HiveField(5)
  String? imageUrl;
  @HiveField(6)
  String? lat;
  @HiveField(7)
  String? lon;
  @HiveField(8)
  String? fcmToken;
  @HiveField(9)
  String? age;
  @HiveField(10)
  String? gender;
  @HiveField(11)
  String? userName;
  @HiveField(12)
  String? etaNo;
  @HiveField(13)
  String? userType;
  @HiveField(14)
  String? token;
  @HiveField(15)
  String? jobOffered;
  @HiveField(16)
  String? cnicFront;
  @HiveField(17)
  String? cnicBack;
  @HiveField(18)
  String? licenceFront;
  @HiveField(19)
  String? licenceBack;

  User({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.phoneNo,
    this.imageUrl,
    this.lat,
    this.lon,
    this.fcmToken,
    this.age,
    this.gender,
    this.userName,
    this.etaNo,
    this.userType,
    this.token,
    this.jobOffered,
    this.cnicBack,
    this.cnicFront,
    this.licenceBack,
    this.licenceFront,
  });

  String get getUserName {
    return '$firstName $lastName'.toTitleCase();
  }

  String getFullName() {
    return '$firstName $lastName'.toTitleCase();
  }

  String get firebaseKey {
    String fKey = '$firstName$lastName'.toLowerCase();
    return fKey;
  }

  User.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    firstName = json['first_name']?.toString();
    lastName = json['last_name']?.toString();
    email = json['email']?.toString();
    phoneNo = json['phone_no']?.toString();
    imageUrl = json['image_url']?.toString();
    lat = json['lat']?.toString();
    lon = json['lon']?.toString();
    fcmToken = json['fcm_token']?.toString();
    age = json['age']?.toString();
    gender = json['gender']?.toString();
    userName = json['user_name']?.toString();
    etaNo = json['eta_no']?.toString();
    userType = json['user_type']?.toString();
    token = json['token']?.toString();
    jobOffered = json['job_offered']?.toString();
    cnicFront = json['cnic_front']?.toString();
    cnicBack = json['cnic_back']?.toString();
    licenceFront = json['licence_front']?.toString();
    licenceBack = json['licence_back']?.toString();
  }
}
