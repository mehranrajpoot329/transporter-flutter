class TruckModel {
  String title;
  String iconUrl;
  bool isSelected;

  TruckModel(this.title, this.iconUrl, this.isSelected);
}
