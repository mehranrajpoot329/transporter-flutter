import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/models/drawer_item_model.dart';
import 'package:transporter_customer/models/rates_model.dart';
import 'package:transporter_customer/routes/app_pages.dart';

List<DrawerItemModel> menuItems = [
  DrawerItemModel(
      title: AppStrings.home,
      iconUrl: AppImages.home,
      routeToGo: AppPages.home),
  DrawerItemModel(
      title: AppStrings.myOrders,
      iconUrl: AppImages.myOrder,
      routeToGo: AppPages.myOrder),
  DrawerItemModel(
      title: AppStrings.notification,
      iconUrl: AppImages.bell,
      routeToGo: AppPages.notifications),
  DrawerItemModel(
      title: AppStrings.viewRates,
      iconUrl: AppImages.viewRates,
      routeToGo: AppPages.viewRates),
  DrawerItemModel(
      title: AppStrings.termsAndConditions,
      iconUrl: AppImages.termAndCondition,
      routeToGo: AppPages.termsAndSConditions),
  DrawerItemModel(
      title: AppStrings.aboutUs,
      iconUrl: AppImages.aboutUS,
      routeToGo: AppPages.aboutUs),
  DrawerItemModel(
      title: AppStrings.contactUs,
      iconUrl: AppImages.contactUs,
      routeToGo: AppPages.contactUs),
  DrawerItemModel(
      title: AppStrings.shareOptions,
      iconUrl: AppImages.share,
      routeToGo: AppPages.shareOptions),
];

///Rates Model

List<RatesModel> ratesItems = [
  RatesModel('Open truck', '\$150'),
  RatesModel('Enclosed truck', '\$250'),
  RatesModel('Van or Kombi', '\$100'),
  RatesModel('Refrigerated truck', '\$400'),
  RatesModel('Tow truck', '\$300'),
  RatesModel('Tipper truck', '\$600'),
  RatesModel('Brick career truck', '\$700'),
];
