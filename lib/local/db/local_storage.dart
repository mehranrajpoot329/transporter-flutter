import 'dart:developer';

import 'package:hive_flutter/adapters.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/models/api_models/place_service_customer_data.dart';
import 'package:transporter_customer/models/api_models/place_service_order_model.dart';
import 'package:transporter_customer/models/graphql_model/ongoing_order_model.dart';
import 'package:transporter_customer/models/graphql_model/ongoing_order_model_data_order_Status_vendor.dart';
import 'package:transporter_customer/models/user_model.dart';

import '../../models/graphql_model/ongoing_order_model_data_order_status.dart';

class HiveHelper {
  Box get appBox => _appBox;

  static final HiveHelper _singleton = HiveHelper._internal();

  factory HiveHelper() {
    return _singleton;
  }

  HiveHelper._internal();

  static late Box _appBox;

  static initHiveHelper() async {
    await Hive.initFlutter();

    ///to register Hive adapter
    Hive
      ..registerAdapter(UserAdapter())
      ..registerAdapter(PlaceServiceOrderModelDataCustomerAdapter())
      ..registerAdapter(PlaceServiceOrderModelDataAdapter())
      ..registerAdapter(OnGoingOrderModelDataAdapter())
      ..registerAdapter(OnGoingOrderModelDataORDERSTATUSAdapter())
      ..registerAdapter(OnGoingOrderModelDataORDERSTATUSVendorAdapter());
    // Open the peopleBox
    _appBox = await Hive.openBox('TCustomer');
  }

  static saveFcmToken({String? fcmToken}) {
    if (fcmToken?.isNotEmpty ?? false) {
      _appBox.put(AppStrings.fcmToken, fcmToken ?? '');
    }
  }

  static String getFcmToken() {
    return _appBox.get(AppStrings.fcmToken) ?? '';
  }

  static setAuthToken(String authToken) {
    log('Login Token : $authToken');
    _appBox.put(
        AppStrings.authToken, authToken.isNotEmpty ? 'Bearer $authToken' : '');
  }

  static String getAuthToken() {
    return (_appBox.get(AppStrings.authToken) != null)
        ? _appBox.get(AppStrings.authToken)
        : 'Bearer ';
  }

  static isLoggedIn() {
    return _appBox.get(AppStrings.isLoggedIn) ?? false;
  }

  static isLoggedOut() {
    return _appBox.get(AppStrings.isLoggedOut) ?? false;
  }

  static saveUserLogout({required bool isLoggedOut}) async {
    await _appBox.put(AppStrings.isLoggedOut, isLoggedOut);
  }

  static isLoggedOutisUserSignedUp() {
    return _appBox.get(AppStrings.isUserSignedUp) ?? false;
  }

  static saveUserLogin({required bool isLoggedIn}) async {
    await _appBox.put(AppStrings.isLoggedIn, isLoggedIn);
  }

  static saveUserSignedUp({required bool isUserSignedUp}) async {
    await _appBox.put(AppStrings.isUserSignedUp, isUserSignedUp);
  }

  static isNew() {
    return _appBox.get(AppStrings.isNew) ?? true;
  }

  static saveIsNew({required bool isNew}) async {
    await _appBox.put(AppStrings.isNew, isNew);
  }

  static saveLoggIn({bool isLoggedIn = false}) async {
    await _appBox.put(AppStrings.isLoggedIn, isLoggedIn);
  }

  static saveUser(User user) async {
    await _appBox.put(AppStrings.user, user);
  }

  static User getUser() {
    return _appBox.get(AppStrings.user, defaultValue: null);
  }

  static Future<void> savePlacedOrder(
      PlaceServiceOrderModelData onGoingOrder) async {
    await _appBox.put(AppStrings.onGoingOrder, onGoingOrder);
  }

  static PlaceServiceOrderModelData? getPlacedOrder() {
    return _appBox.get(AppStrings.onGoingOrder, defaultValue: null);
  }

  static void removePlacedOrder() async {
    await _appBox.delete(AppStrings.onGoingOrder);
  }

  static Future<void> saveGQLOnGoingOrder(
      OnGoingOrderModelData onGoingOrder) async {
    await _appBox.put(AppStrings.onGQLGoingOrder, onGoingOrder);
  }

  static OnGoingOrderModelData? getGQLOnGoingOrder() {
    return _appBox.get(AppStrings.onGQLGoingOrder, defaultValue: null);
  }

  static void removeGQLOnGoingOrder() async {
    await _appBox.delete(AppStrings.onGQLGoingOrder);
  }

  static clearBox() {
    _appBox.clear();
  }

  static closeAllBoxes() {
    Hive.close();
  }
}
