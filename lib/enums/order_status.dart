class OrderStatus {
  static const String onGoing = 'ONGOING'; //when user will start order
  static const String completed = 'COMPLETED';

  static const String rejected = 'REJECTED'; //when vendor rejected order
  static const String accepted = 'ACCEPTED'; //will be count as Accepted
  static const String onTheWay = 'ON_THE_WAY'; //will be count as Accepted
  static const String arrived = 'ARRIVED'; //when vendor arrived hit api

  ///MY OWN STATUS-->
  static const String searching = 'SEARCHING';
  static const String notStartedYet = 'NOT_STARTED_YET';
  static const String cancelled =
      'CANCELLED'; //when vendor will cancel order =>NO manage
  ///-----<
}
