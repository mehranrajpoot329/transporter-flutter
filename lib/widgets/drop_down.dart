import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';

class CustomDropDown extends StatefulWidget {
  final List<String> items;
  final String hint;
  final Function(String?) selectedValue;
  const CustomDropDown(
      {Key? key,
      required this.items,
      required this.hint,
      required this.selectedValue})
      : super(key: key);

  @override
  State<CustomDropDown> createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {
  String? selectedValue;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 6.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.hint,
            textAlign: TextAlign.start,
            style: TextStyle(
                color: AppColors.textBlack.withOpacity(0.6),
                fontFamily: AppFonts.sarabunRegular,
                fontSize: 11.sp),
          ),
          Dimensions.y1,
          Container(
            height: 7.h,
            padding: EdgeInsets.symmetric(horizontal: 4.w),
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: AppColors.textBlack.withOpacity(0.1),
                  offset: const Offset(-3, 3),
                  blurRadius: 15,
                ),
              ],
            ),
            child: Center(
              child: DropdownButton<String>(
                value: selectedValue,
                borderRadius: BorderRadius.circular(10),
                icon: const Icon(Icons.arrow_drop_down),
                iconSize: 3.h,
                isExpanded: true,
                isDense: true,
                underline: const SizedBox(),
                style: TextStyle(
                    color: AppColors.textBlack,
                    fontFamily: AppFonts.sarabunRegular,
                    fontSize: 10.sp),
                hint: Text(
                  widget.hint,
                  style: TextStyle(
                      color: AppColors.textBlack.withOpacity(0.4),
                      fontFamily: AppFonts.sarabunRegular,
                      fontSize: 10.sp),
                  textAlign: TextAlign.center,
                ),
                onChanged: (newValue) {
                  selectedValue = newValue;
                  widget.selectedValue(newValue);
                  setState(() {});
                },
                items: widget.items.map<DropdownMenuItem<String>>(
                  (String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  },
                ).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
