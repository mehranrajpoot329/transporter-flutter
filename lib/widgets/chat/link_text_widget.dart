import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/utils/utils.dart';

class LinkTextWidget extends StatelessWidget {
  final String message;
  final bool isMe;
  const LinkTextWidget({Key? key, required this.message, required this.isMe})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: AppColors.blue,
      highlightColor: AppColors.blue,
      onTap: () {
        Utils.openURL(message);
      },
      child: Text(
        message,
        style: TextStyle(
          color: isMe ? AppColors.black : AppColors.white,
          fontSize: 12.0.sp,
          decoration: TextDecoration.underline,
        ),
      ),
    );
  }
}
