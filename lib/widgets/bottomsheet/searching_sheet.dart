import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/widgets/bottomsheet/app_bottom_sheet.dart';
import 'package:transporter_customer/widgets/buttons/timer_button.dart';

class SearchingSheet extends GetView<HomeController> {
  const SearchingSheet({Key? key}) : super(key: key);

  Future<bool> onBackPressed() async {
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => onBackPressed(),
      child: Obx(
        () => Align(
          alignment: Alignment.bottomCenter,
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: AppBottomSheet(
              height: 33.h,
              children: [
                Text(
                  'Searching...',
                  style: TextStyles.mediumStyle,
                ),
                Dimensions.y3,
                Flexible(
                  child: Text(
                    'Please wait...',
                    style: TextStyles.boldStyle
                        .copyWith(fontSize: 23.sp, fontWeight: FontWeight.bold),
                  ),
                ),
                Dimensions.y3,
                Text(
                  'Estimated Amount: \$ 600',
                  style: controller.isSearched.isFalse
                      ? TextStyles.regularStyle.copyWith(fontSize: 12.sp)
                      : TextStyles.mediumStyle.copyWith(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.bold,
                        ),
                ),
                Dimensions.y1,
                TimerButton(
                  title: AppStrings.cancle,
                  borderRadius: BorderRadius.circular(10.h),
                  buttonWidth: Get.width / 1.4,
                  fontSize: 12.sp,
                  titleColor: AppColors.yellow,
                  onEndTimer: (value) {
                    if (value) {}
                  },
                  onTap: () {
                    controller.cancelOrder();
                  },
                ),
                Dimensions.y1,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
