import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/global.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/modules/home/controllers/location_controller.dart';
import 'package:transporter_customer/modules/home/views/book_now_screen.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';

class LocationBottomSheet extends GetView<HomeController> {
  LocationBottomSheet({Key? key}) : super(key: key);

  Future<bool> onBackPressed() async {
    controller.isDes.value = true;
    controller.showBackIcon.value = false;
    controller.showCurrentLocation.value = true;
    controller.showDottedLines.value = true;
    controller.showMenuIcon.value = true;
    controller.showDestinationField(true);
    if (Get.isBottomSheetOpen ?? true) Get.back();
    return true;
  }

  final LocationController _locationController = Get.find<LocationController>();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => onBackPressed(),
      child: Container(
        height: 45.h,
        width: Get.width,
        padding: EdgeInsets.only(left: 4.w, right: 4.w, top: 4.h),
        decoration: const BoxDecoration(
          color: AppColors.yellow,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          ),
        ),
        child: Column(
          children: [
            GetBuilder<LocationController>(builder: (_) {
              return LocationTextWithIcon(
                iconUrl: AppImages.locationIcon,
                locationText:
                    _locationController.currentLocationController.value.text,
              );
            }),
            Transform.rotate(
              angle: 300,
              child: SvgPicture.asset(
                AppImages.back,
                color: AppColors.white,
              ),
            ),
            Dimensions.y2,
            GetBuilder<LocationController>(builder: (_) {
              return LocationTextWithIcon(
                iconUrl: AppImages.locationIcon,
                locationText: _locationController.desLocationController.value.text,
              );
            }),
            CustomeButton(
              title: AppStrings.nextbtnText,
              borderRadius: BorderRadius.circular(10.h),
              buttonWidth: Get.width / 1.5,
              fontSize: 10.sp,
              titleColor: AppColors.yellow,
              onTap: () {
                Get.back();
                openFullScreenDialog(
                  const BookNowDialogScreen(),
                );
                controller.isDes.value = true;
                controller.showBackIcon.value = false;
                controller.showCurrentLocation.value = true;
                controller.showDottedLines.value = true;
                controller.showMenuIcon.value = true;
                controller.showDestinationField(true);
              },
            ),
          ],
        ),
      ),
    );
  }
}

class LocationTextWithIcon extends GetView<HomeController> {
  final String? locationText;
  final String iconUrl;
  const LocationTextWithIcon({
    Key? key,
    this.locationText,
    required this.iconUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 2.h),
      child: Column(
        children: [
          Container(
            width: Get.width,
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 1.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SvgPicture.asset(
                    iconUrl,
                    height: 3.h,
                    fit: BoxFit.scaleDown,
                  ),
                  Dimensions.x6,
                  Flexible(
                    child: Text(
                      locationText!,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style:
                          const TextStyle(fontFamily: AppFonts.sarabunMedium),
                    ),
                  )
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              controller.isDes.value = true;
              controller.showBackIcon.value = false;
              controller.showCurrentLocation.value = true;
              controller.showMenuIcon.value = true;
              controller.showDottedLines.value = true;
              controller.showDestinationField(true);
              if (Get.isBottomSheetOpen ?? true) Get.back();
            },
            child: Padding(
              padding: EdgeInsets.only(right: 2.0.w, top: 0.2.h),
              child: const Align(
                alignment: Alignment.bottomRight,
                child: Text(
                  'Change',
                  style: TextStyle(
                      color: AppColors.black, fontFamily: AppFonts.sarabunBold),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
