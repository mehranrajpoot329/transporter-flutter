import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';

class AppBottomSheet extends StatelessWidget {
  final MainAxisAlignment mainAxisAlignment;
  final MainAxisSize mainAxisSize;
  final CrossAxisAlignment crossAxisAlignment;
  final List<Widget> children;
  final double? height;
  const AppBottomSheet(
      {Key? key,
      this.mainAxisAlignment = MainAxisAlignment.start,
      this.mainAxisSize = MainAxisSize.max,
      this.crossAxisAlignment = CrossAxisAlignment.center,
      this.height,
      required this.children})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height ?? 45.h,
      width: Get.width,
      padding: EdgeInsets.only(left: 4.w, right: 4.w, top: 4.h),
      decoration: const BoxDecoration(
        color: AppColors.yellow,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20),
          topLeft: Radius.circular(20),
        ),
      ),
      child: Column(
        children: children,
      ),
    );
  }
}
