import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/widgets/bottomsheet/app_bottom_sheet.dart';

class ContractorHasArrivedSheet extends GetWidget<HomeController> {
  const ContractorHasArrivedSheet({Key? key}) : super(key: key);

  Future<bool> onBackPressed() async {
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => onBackPressed(),
      child: GetBuilder<HomeController>(builder: (_) {
        return Align(
          alignment: Alignment.bottomCenter,
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: AppBottomSheet(
              height: 33.h,
              children: [
                Text(
                  AppStrings.contractorHasArrived,
                  style: TextStyles.mediumStyle,
                ),
                Dimensions.y10,
                Flexible(
                  child: Text(
                    AppStrings.jobIsInProgress,
                    style: TextStyles.boldStyle.copyWith(
                      fontSize: 23.sp,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Dimensions.y3,
              ],
            ),
          ),
        );
      }),
    );
  }
}
