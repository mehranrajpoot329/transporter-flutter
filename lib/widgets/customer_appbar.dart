import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';

class CustomeAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final String iconUrl;
  final VoidCallback onBack;
  final Color color;

  const CustomeAppBar(
      {Key? key,
      required this.iconUrl,
      required this.onBack,
      this.color = AppColors.white,
      required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: AppBar(
        backgroundColor: color,
        elevation: 0,
        centerTitle: false,
        leading: GestureDetector(
          onTap: onBack,
          child: Padding(
            padding: EdgeInsets.only(top: 0.5.h, left: 3.w),
            child: SvgPicture.asset(
              AppImages.back,
              fit: BoxFit.scaleDown,
            ),
          ),
        ),
        title: Text(
          title,
          style: TextStyle(
            fontFamily: AppFonts.sarabunMedium,
            fontSize: 16.sp,
            color: AppColors.textBlack,
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(10.h);
}




//  Row(
//           mainAxisAlignment: MainAxisAlignment.start,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
            
//             Dimensions.x4,
            
//           ],
//         ),