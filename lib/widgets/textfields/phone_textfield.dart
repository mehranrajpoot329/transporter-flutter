import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/widgets/vertical_divider.dart';

class PhoneTextFormField extends StatelessWidget {
  final void Function(CountryCode)? onChanged;
  final void Function(String)? onChangeTextFiled;
  final List<TextInputFormatter>? inputFormatters;
  const PhoneTextFormField(
      {Key? key, this.onChanged, this.onChangeTextFiled, this.inputFormatters})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 6.5.h,
      width: Get.width,
      margin: EdgeInsets.symmetric(horizontal: 3.w),
      decoration: BoxDecoration(
        color: AppColors.gray,
        borderRadius: BorderRadius.circular(7),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(
            child: Padding(
              padding: EdgeInsets.only(left: 2.0.w),
              child: CountryCodePicker(
                enabled: true,
                onChanged: onChanged,
                initialSelection: 'PK',
                favorite: const ['+92', 'PK'],
                showCountryOnly: false,
                showOnlyCountryWhenClosed: false,
                alignLeft: false,
                textStyle: TextStyle(
                  fontFamily: AppFonts.sarabunRegular,
                  fontSize: 12.sp,
                ),
                builder: (code) => Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    // Image.asset(code?.flagUri, package: 'country_code_picker', width: 30.0,),
                    Container(
                      height: 2.4.h,
                      width: 2.4.h,
                      margin: EdgeInsets.only(
                        right: 0.5.h,
                      ),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            code!.flagUri.toString(),
                            package: 'country_code_picker',
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 0.3.h),
                      child: Text(
                        code.dialCode.toString(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: AppColors.textBlack,
                          fontSize: 12.sp,
                          fontFamily: AppFonts.sarabunRegular,
                        ),
                      ),
                    ),
                    Icon(
                      Icons.arrow_drop_down,
                      size: 3.h,
                      color: AppColors.black,
                    ),
                  ],
                ),
                // showDropDownButton: true,
              ),
            ),
          ),
          VericalDividerWidget(
            thickness: 2,
            color: AppColors.black.withOpacity(0.5),
            padding: EdgeInsets.symmetric(vertical: 1.7.h),
          ),
          Expanded(
            flex: 2,
            child: TextFormField(
              onChanged: onChangeTextFiled,
              textAlign: TextAlign.start,
              textAlignVertical: TextAlignVertical.center,
              style: TextStyle(
                  color: AppColors.textBlack,
                  fontFamily: AppFonts.sarabunRegular,
                  fontSize: 12.sp),
              keyboardType: TextInputType.number,
              inputFormatters: inputFormatters,
              cursorColor: AppColors.black,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 1.h, bottom: 1.9.h),
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
