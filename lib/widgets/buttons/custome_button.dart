import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';

class CustomeButton extends StatelessWidget {
  final String title;
  final Color titleColor;
  final double fontSize;
  final double buttonWidth;
  final BorderRadius borderRadius;
  final VoidCallback? onTap;
  final Color? btnColor;

  const CustomeButton({
    Key? key,
    required this.title,
    required this.titleColor,
    required this.fontSize,
    required this.buttonWidth,
    required this.borderRadius,
    this.btnColor,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: buttonWidth,
      margin: EdgeInsets.symmetric(horizontal: 3.w),
      decoration: BoxDecoration(
        color: btnColor ?? AppColors.black,
        borderRadius: borderRadius,
      ),
      child: Center(
        child: TextButton(
          onPressed: onTap,
          child: Center(
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: AppFonts.sarabunMedium,
                color: titleColor,
                fontSize: fontSize,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
