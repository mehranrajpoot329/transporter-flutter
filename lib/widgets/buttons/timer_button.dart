import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';

class TimerButton extends GetWidget<HomeController> {
  final String title;
  final Color titleColor;
  final double fontSize;
  final double buttonWidth;
  final BorderRadius borderRadius;
  final VoidCallback onTap;
  final void Function(bool) onEndTimer;

  const TimerButton({
    Key? key,
    required this.title,
    required this.titleColor,
    required this.fontSize,
    required this.buttonWidth,
    required this.borderRadius,
    required this.onTap,
    required this.onEndTimer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller.startTimer(
      (isEnd) {
        onEndTimer(isEnd);
      },
    );
    return Container(
      width: buttonWidth,
      margin: EdgeInsets.symmetric(horizontal: 3.w),
      decoration: BoxDecoration(
        color: AppColors.black,
        borderRadius: borderRadius,
      ),
      child: Center(
        child: TextButton(
          onPressed: onTap,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                title,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: AppFonts.sarabunMedium,
                  color: titleColor,
                  fontSize: fontSize,
                ),
              ),
              Dimensions.getCustomeWidth(20),
              Obx(
                () => Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: 3.w),
                    child: Text(
                      controller.start.value.toString(),
                      style: TextStyles.regularStyle
                          .copyWith(color: AppColors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
