import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';

class RatingDialog extends GetView<HomeController> {
  const RatingDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white.withOpacity(0),
      body: WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: SingleChildScrollView(
          child: SizedBox(
            height: Get.height,
            width: Get.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Dimensions.getCustomeHeight(3),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
                    height: Get.height / 1.7,
                    width: Get.width,
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: AppColors.black.withOpacity(0.2),
                          blurRadius: 15,
                          offset: const Offset(3, 8),
                        ),
                      ],
                    ),
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 7.w, vertical: 1.h),
                      child: Column(
                        children: [
                          Dimensions.y2,
                          Text(
                            AppStrings.rateAndReview,
                            textAlign: TextAlign.center,
                            style:
                                TextStyles.boldStyle.copyWith(fontSize: 17.sp),
                          ),
                          Flexible(child: Dimensions.y9),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              AppStrings.howThisJob,
                              textAlign: TextAlign.start,
                              style: TextStyles.regularStyle
                                  .copyWith(fontSize: 13.sp),
                            ),
                          ),
                          Dimensions.y2,
                          RatingBar(
                            initialRating: 3,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            glow: false,
                            ratingWidget: RatingWidget(
                              full: const Icon(
                                Icons.star,
                                color: AppColors.yellow,
                              ),
                              half: const Icon(
                                Icons.star_half,
                                color: AppColors.yellow,
                              ),
                              empty: const Icon(Icons.star_outline_outlined),
                            ),
                            itemPadding:
                                const EdgeInsets.symmetric(horizontal: 4.0),
                            onRatingUpdate: (rating) {
                              controller.reviewStars?.value = rating;
                            },
                          ),
                          Flexible(child: Dimensions.y3),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              AppStrings.yourReview,
                              textAlign: TextAlign.start,
                              style: TextStyles.regularStyle.copyWith(
                                  fontSize: 15.sp,
                                  color: AppColors.textBlack.withOpacity(0.5)),
                            ),
                          ),
                          Dimensions.y2,
                          Container(
                            height: 15.h,
                            width: Get.width / 1.2,
                            margin: EdgeInsets.symmetric(vertical: 1.h),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                color: AppColors.textBlack.withOpacity(0.3),
                              ),
                            ),
                            child: FormBuilderTextField(
                              name: 'review',
                              keyboardType: TextInputType.multiline,
                              minLines: 6,
                              maxLines: null,
                              onChanged: (value) {
                                controller.reviewMessage?.value = value!;
                              },
                              cursorColor: AppColors.textBlack,
                              style: TextStyle(
                                  fontFamily: AppFonts.sarabunRegular,
                                  fontSize: 13.sp),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                    left: 5.w, right: 5.w, top: 1.h),
                                hintText: AppStrings.yourReview,
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    fontFamily: AppFonts.sarabunRegular,
                                    color: AppColors.black.withOpacity(0.5)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                CustomeButton(
                  title: AppStrings.submit,
                  borderRadius: BorderRadius.circular(10.h),
                  buttonWidth: Get.width / 1.4,
                  fontSize: 12.sp,
                  titleColor: AppColors.yellow,
                  onTap: () {
                    controller.addReview(context);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
