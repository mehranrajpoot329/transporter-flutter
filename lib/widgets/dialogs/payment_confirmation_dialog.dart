import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';

class PaymentConfirmationDialog extends GetView<HomeController> {
  const PaymentConfirmationDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white.withOpacity(0),
      body: WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: SingleChildScrollView(
          child: SizedBox(
            height: Get.height,
            width: Get.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Dimensions.getCustomeHeight(3),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
                    height: Get.height / 3.5,
                    width: Get.width,
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: AppColors.black.withOpacity(0.2),
                          blurRadius: 15,
                          offset: const Offset(3, 8),
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.0.w),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(AppImages.info),
                          Dimensions.y2,
                          Text(
                            'Please confirm (\$600) this amount has been payed by you?',
                            textAlign: TextAlign.center,
                            style: TextStyles.mediumStyle
                                .copyWith(fontSize: 19.sp),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                CustomeButton(
                  title: AppStrings.confirm,
                  borderRadius: BorderRadius.circular(10.h),
                  buttonWidth: Get.width / 1.4,
                  fontSize: 12.sp,
                  titleColor: AppColors.yellow,
                  onTap: () {
                    controller.endOrder(context);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
