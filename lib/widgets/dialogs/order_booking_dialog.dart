import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/widgets/drop_down.dart';

class OrderBookingDialog extends GetView<HomeController> {
  const OrderBookingDialog({Key? key}) : super(key: key);

  Future<bool> onBackPressed() async {
    controller.isDes.value = true;
    controller.showBackIcon.value = false;
    controller.showCurrentLocation.value = true;
    controller.showDottedLines.value = true;
    controller.showDestinationField(true);
    if (Get.isBottomSheetOpen ?? true) Get.back();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => onBackPressed(),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 3.w, vertical: 5.5.h),
        height: 30.h,
        width: Get.width,
        decoration: BoxDecoration(
          color: AppColors.gray,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Material(
          type: MaterialType.transparency,
          borderRadius: BorderRadius.circular(20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () => Get.back(),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                      padding: EdgeInsets.only(right: 5.w, top: 3.h),
                      child: SvgPicture.asset(
                        AppImages.close,
                        height: 2.h,
                      ),
                    ),
                  ),
                ),

                ///Details Text
                Text(
                  AppStrings.bookNowWhichYouWant,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: AppFonts.sarabunSemiBold,
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w700,
                    color: AppColors.textBlack,
                  ),
                ),
                Dimensions.y6,
                SizedBox(
                  width: Get.width,
                  height: 22.h,
                  child: Obx(
                    () => ListView.separated(
                      padding: EdgeInsets.only(left: 9.w),
                      physics: const BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () {
                          controller.vehiclesList[index].isSelected =
                              !controller.vehiclesList[index].isSelected;
                          controller.vehiclesList.refresh();
                        },
                        child: Column(
                          children: [
                            Container(
                              height: 14.h,
                              width: 35.w,
                              margin: EdgeInsets.only(bottom: 1.3.h),
                              decoration: BoxDecoration(
                                color: controller.vehiclesList[index].isSelected
                                    ? AppColors.yellow
                                    : AppColors.white,
                                borderRadius: BorderRadius.circular(19),
                                boxShadow: const [
                                  BoxShadow(
                                    color: AppColors.gray,
                                    offset: Offset(5, 5),
                                    blurRadius: 15,
                                  ),
                                ],
                              ),
                              child: Center(
                                child: SvgPicture.asset(
                                    controller.vehiclesList[index].imageUrl!),
                              ),
                            ),
                            Text(
                              controller.vehiclesList[index].vehicleName ?? '',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: AppColors.textBlack,
                                  fontFamily: AppFonts.sarabunMedium,
                                  fontSize: 12.sp),
                            )
                          ],
                        ),
                      ),
                      separatorBuilder: (context, index) => Dimensions.x4,
                      itemCount: controller.vehiclesList.length,
                    ),
                  ),
                ),
                CustomDropDown(
                  hint: AppStrings.capacity,
                  items: const <String>[
                    '200 Kg - 300 Kg',
                    '300 Kg - 500 Kg',
                    '500 Kg - 700 Kg',
                    '700 kg - 900 kg'
                  ],
                  selectedValue: (value) {
                    log(value.toString());
                  },
                ),
                Dimensions.y4,
                CustomDropDown(
                  hint: AppStrings.estimatedAmount,
                  items: const <String>[
                    '\$ 600',
                    '\$ 700',
                    '\$ 800',
                  ],
                  selectedValue: (value) {
                    log(value.toString());
                  },
                ),
                Dimensions.y4,
                CustomDropDown(
                  hint: AppStrings.paymentMethod,
                  items: const <String>[
                    'USD Cash',
                    'ZWL Cash',
                    'Eco Cash',
                  ],
                  selectedValue: (value) {
                    log(value.toString());
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
