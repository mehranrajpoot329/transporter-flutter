import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/orders/orders_controller.dart';
import 'package:transporter_customer/network/end_points.dart';
import 'package:transporter_customer/widgets/contractor_profile_order_widget.dart';
import 'package:transporter_customer/widgets/order_dialog_listtile.dart';

class OrderSummaryDialog extends GetView<MyOrdersController> {
  final int orderId;

  const OrderSummaryDialog({
    Key? key,
    required this.orderId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller.getOrderById(orderId: orderId);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 3.w, vertical: 4.h),
      height: Get.height,
      width: Get.width,
      decoration: BoxDecoration(
        color: AppColors.gray,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Material(
        borderRadius: BorderRadius.circular(20),
        child: GetBuilder<MyOrdersController>(
            id: 'singleOrder',
            builder: (_) {
              return controller.isLoading
                  ? Center(
                      child: Text(
                        'Loading...',
                        style: TextStyles.mediumStyle,
                      ),
                    )
                  : SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: () => Get.back(),
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Padding(
                                padding: EdgeInsets.only(right: 5.w, top: 3.h),
                                child: SvgPicture.asset(
                                  AppImages.close,
                                  height: 2.h,
                                ),
                              ),
                            ),
                          ),

                          ///Details Text
                          Text(
                            AppStrings.detail,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: AppFonts.sarabunSemiBold,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 0.5,
                              color: AppColors.textBlack,
                            ),
                          ),
                          OrderDialogListTile(
                            topMargin: Dimensions.y3,
                            title: AppStrings.yourLocaiton,
                            subWidget: Padding(
                              padding: EdgeInsets.only(left: 9.0.w),
                              child: Text(
                                '${controller.orderData.mSourceLocationAddress}',
                                textAlign: TextAlign.start,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontFamily: AppFonts.sarabunSemiBold,
                                  fontSize: 12.sp,
                                  overflow: TextOverflow.ellipsis,
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.textBlack,
                                ),
                              ),
                            ),
                          ),
                          OrderDialogListTile(
                            topMargin: Dimensions.y3,
                            title: AppStrings.destinationLocation,
                            subWidget: Padding(
                              padding: EdgeInsets.only(left: 9.0.w),
                              child: Text(
                                '${controller.orderData.mDestinationLocationAddress}',
                                textAlign: TextAlign.start,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontFamily: AppFonts.sarabunSemiBold,
                                  fontSize: 12.sp,
                                  overflow: TextOverflow.ellipsis,
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.textBlack,
                                ),
                              ),
                            ),
                          ),
                          OrderDialogListTile(
                            topMargin: Dimensions.y3,
                            title: AppStrings.selectedTruck,
                            subWidget: Padding(
                              padding: EdgeInsets.only(left: 9.0.w),
                              child: Text(
                                '${controller.orderData.vehicle}',
                                textAlign: TextAlign.start,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontFamily: AppFonts.sarabunSemiBold,
                                  fontSize: 12.sp,
                                  overflow: TextOverflow.ellipsis,
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.textBlack,
                                ),
                              ),
                            ),
                          ),
                          OrderDialogListTile(
                            topMargin: Dimensions.y3,
                            title: AppStrings.capacity,
                            subWidget: Padding(
                              padding: EdgeInsets.only(left: 9.0.w),
                              child: Text(
                                '${controller.orderData.capacity}',
                                textAlign: TextAlign.start,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontFamily: AppFonts.sarabunSemiBold,
                                  fontSize: 12.sp,
                                  overflow: TextOverflow.ellipsis,
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.textBlack,
                                ),
                              ),
                            ),
                          ),
                          OrderDialogListTile(
                            topMargin: Dimensions.y3,
                            title: AppStrings.payedAmount,
                            subWidget: Padding(
                              padding: EdgeInsets.only(left: 9.0.w),
                              child: Text(
                                ' ${controller.orderData.totalPrice}',
                                textAlign: TextAlign.start,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontFamily: AppFonts.sarabunSemiBold,
                                  fontSize: 12.sp,
                                  overflow: TextOverflow.ellipsis,
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.textBlack,
                                ),
                              ),
                            ),
                          ),
                          OrderDialogListTile(
                            topMargin: Dimensions.y3,
                            title: AppStrings.paymentMethod,
                            subWidget: Padding(
                              padding: EdgeInsets.only(left: 9.0.w),
                              child: Text(
                                '${controller.orderData.paymentMethod}',
                                textAlign: TextAlign.start,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontFamily: AppFonts.sarabunSemiBold,
                                  fontSize: 12.sp,
                                  overflow: TextOverflow.ellipsis,
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.textBlack,
                                ),
                              ),
                            ),
                          ),

                          OrderDialogListTile(
                            topMargin: Dimensions.y3,
                            title: 'Contractor Detail:',
                            subWidget: const ContractorProfileWidget(
                                imageURL: EndPoints.placeHolder,
                                fullName: 'Dummy',
                                phoneNumber: '457603475603456'),
                          ),
                          Dimensions.y2,
                        ],
                      ),
                    );
            }),
      ),
    );
  }
}
