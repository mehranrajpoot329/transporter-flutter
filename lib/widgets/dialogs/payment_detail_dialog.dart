import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/global.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/local/db/local_storage.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';
import 'package:transporter_customer/widgets/dialogs/payment_confirmation_dialog.dart';

class PaymentDetailDialog extends GetView<HomeController> {
  PaymentDetailDialog({Key? key}) : super(key: key);

  Future<bool> onBackPressed() async {
    return false;
  }

  TextEditingController bankAccountController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var onGoingOrderModelData = HiveHelper.getGQLOnGoingOrder();

    return Scaffold(
      backgroundColor: AppColors.white.withOpacity(0),
      body: WillPopScope(
        onWillPop: () => onBackPressed(),
        child: SingleChildScrollView(
          child: SizedBox(
            height: Get.height,
            width: Get.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Flexible(child: Dimensions.getCustomeHeight(3)),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
                    height: Get.height / 1.8,
                    width: Get.width,
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: AppColors.black.withOpacity(0.2),
                          blurRadius: 15,
                          offset: const Offset(3, 8),
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 2.w),
                      child: Column(
                        children: [
                          Flexible(child: Dimensions.y2),
                          Text(
                            AppStrings.paymentDetails,
                            style: TextStyles.boldStyle,
                          ),
                          Flexible(child: Dimensions.y2),
                          Container(
                            height: 10.h,
                            width: 15.h,
                            decoration: BoxDecoration(
                              color: AppColors.yellow,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Center(
                              child: SvgPicture.asset(AppImages.dollar),
                            ),
                          ),
                          Dimensions.getCustomeHeight(0.5),
                          Text(
                            AppStrings.usdCash,
                            style: TextStyles.mediumStyle
                                .copyWith(fontSize: 12.sp),
                          ),
                          Flexible(child: Dimensions.y4),
                          Text(
                            AppStrings.bankHolderName,
                            style: TextStyles.boldStyle.copyWith(
                              color: AppColors.black.withOpacity(0.5),
                            ),
                          ),
                          Dimensions.y2,
                          Text(
                            '${onGoingOrderModelData?.orderStatus?.vendor?.getFullName}',
                            style: TextStyles.boldStyle,
                          ),
                          Flexible(child: Dimensions.y4),
                          Text(
                            AppStrings.bankAccountNumber,
                            style: TextStyles.boldStyle.copyWith(
                              color: AppColors.black.withOpacity(0.5),
                            ),
                          ),
                          Flexible(child: Dimensions.y2),
                          DottedBorder(
                            radius: Radius.circular(10.h),
                            borderType: BorderType.RRect,
                            dashPattern: const [10, 5, 10, 5, 10, 5],
                            child: SizedBox(
                              width: Get.width / 1.2,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: FormBuilderTextField(
                                      name: 'account_number',
                                      controller: bankAccountController,
                                      keyboardType: TextInputType.number,
                                      cursorColor: AppColors.textBlack,
                                      style: TextStyle(
                                          fontFamily: AppFonts.sarabunRegular,
                                          fontSize: 13.sp),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                            left: 5.w, right: 5.w),
                                        hintText: AppStrings.bankAccountNumber,
                                        border: InputBorder.none,
                                        hintStyle: TextStyle(
                                          fontFamily: AppFonts.sarabunRegular,
                                          color:
                                              AppColors.black.withOpacity(0.5),
                                        ),
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      controller
                                          .copyText(bankAccountController.text);
                                    },
                                    child: Container(
                                      height: 4.4.h,
                                      width: 10.h,
                                      margin: EdgeInsets.only(right: 2.w),
                                      decoration: BoxDecoration(
                                        color: AppColors.black,
                                        borderRadius:
                                            BorderRadius.circular(3.h),
                                      ),
                                      child: Align(
                                        alignment: Alignment.center,
                                        child: Obx(
                                          () => controller.isCopied
                                              ? const Icon(
                                                  Icons.check,
                                                  color: AppColors.white,
                                                )
                                              : Text(
                                                  'Copy',
                                                  style: TextStyles.mediumStyle
                                                      .copyWith(
                                                          color:
                                                              AppColors.yellow,
                                                          fontSize: 11.sp),
                                                ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                CustomeButton(
                  title: AppStrings.paymentComplete,
                  borderRadius: BorderRadius.circular(10.h),
                  buttonWidth: Get.width / 1.4,
                  fontSize: 12.sp,
                  titleColor: AppColors.yellow,
                  onTap: () {
                    removeFullScreenDialog();
                    openFullScreenDialog(
                      const PaymentConfirmationDialog(),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
