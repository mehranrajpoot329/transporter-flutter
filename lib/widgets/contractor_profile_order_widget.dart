import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';

class ContractorProfileWidget extends StatelessWidget {
  final String imageURL;
  final String fullName;
  final String phoneNumber;
  const ContractorProfileWidget({
    Key? key,
    required this.imageURL,
    required this.fullName,
    required this.phoneNumber,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 8.h,
          height: 9.5.h,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: AppColors.black,
          ),
          child: const CircleAvatar(
            radius: 30.0,
            backgroundImage: NetworkImage(
              'https://via.placeholder.com/150',
            ),
            backgroundColor: Colors.transparent,
          ),
        ),
        Dimensions.x5,
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              fullName.toUpperCase(),
              textAlign: TextAlign.start,
              style: TextStyle(
                fontFamily: AppFonts.sarabunMedium,
                fontSize: 11.sp,
                color: AppColors.textBlack,
              ),
            ),
            Text(
              'Phone No: $phoneNumber',
              textAlign: TextAlign.start,
              style: TextStyle(
                fontFamily: AppFonts.sarabunMedium,
                fontSize: 10.sp,
                color: AppColors.textBlack.withOpacity(0.5),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
