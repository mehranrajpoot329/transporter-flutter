import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';

class OrderDialogListTile extends StatelessWidget {
  final String title;
  final double? titleOpacity;
  final Widget subWidget;
  final double leftPadding;
  final Widget topMargin;
  const OrderDialogListTile({
    Key? key,
    required this.title,
    required this.subWidget,
    required this.topMargin,
    this.titleOpacity,
    this.leftPadding = 8.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        topMargin,
        Padding(
          padding: EdgeInsets.only(left: leftPadding.w),
          child: Align(
            alignment: Alignment.topLeft,
            heightFactor: 1.5,
            child: Text(
              title,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontFamily: AppFonts.sarabunMedium,
                fontSize: 11.sp,
                color: AppColors.textBlack.withOpacity(titleOpacity ?? 1),
              ),
            ),
          ),
        ),
        SizedBox(
          width: Get.width / 1.5,
          child: subWidget,
        ),
      ],
    );
  }
}
