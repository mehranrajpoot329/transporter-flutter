import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:transporter_customer/constants/app_colors.dart';

class CustomNetworkCacheImage extends StatelessWidget {
  final String imageUrl;
  final double width;
  final double height;
  final Color errorWidgetColor;
  final BoxFit? fit;
  const CustomNetworkCacheImage(
      {Key? key,
      required this.imageUrl,
      required this.width,
      required this.height,
      this.errorWidgetColor = AppColors.black,
      this.fit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: imageProvider,
            fit: fit ?? BoxFit.cover,
          ),
        ),
      ),
      placeholder: (context, url) => const Center(
        child: CircularProgressIndicator(),
      ),
      errorWidget: (context, url, error) => Center(
        child: Icon(
          Icons.error,
          color: errorWidgetColor,
        ),
      ),
    );
  }
}
