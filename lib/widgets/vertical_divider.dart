import 'package:flutter/material.dart';

class VericalDividerWidget extends StatelessWidget {
  final EdgeInsets padding;
  final double thickness;
  final Color color;
  const VericalDividerWidget({
    Key? key,
    required this.padding,
    required this.thickness,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: VerticalDivider(
        thickness: thickness,
        color: color,
      ),
    );
  }
}
