class AppFonts {
  static const String sarabunBold = 'Sarabun-Bold';
  static const String sarabunRegular = 'Sarabun-Regular';
  static const String sarabunMedium = 'Sarabun-Medium';
  static const String sarabunSemiBold = 'Sarabun-SemiBold';
}
