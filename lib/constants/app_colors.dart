import 'package:flutter/material.dart';

class AppColors {
  static const Color white = Color(0xFFFFFFFF);
  static const Color lightGray = Color(0xFFEFEFEF);
  static const Color yellow = Color(0xFFFED053);
  static const Color darkGray = Color(0xFF00000029);
  static const Color gray = Color(0xFFEFEFEF);
  static const Color veryDarkGray = Color(0xFF0000003B);
  static const Color black = Color(0xFF1E2328);
  static const Color textBlack = Color(0xFF111820);
  static const Color transparent = Colors.transparent;
  static const Color blue = Color.fromARGB(255, 43, 6, 250);
}
