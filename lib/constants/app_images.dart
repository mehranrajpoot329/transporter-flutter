class AppImages {
  static const String aboutUS = 'assets/svgs/aboutUs.svg';
  static const String appIcon = 'assets/images/applogo.png';
  static const String back = 'assets/svgs/back.svg';
  static const String camera = 'assets/svgs/camera.svg';
  static const String close = 'assets/svgs/close.svg';
  static const String dumpTruck = 'assets/svgs/dumpTruck.svg';
  static const String home = 'assets/svgs/home.svg';
  static const String profile = 'assets/svgs/profile.svg';
  static const String signOut = 'assets/svgs/signOut.svg';
  static const String openTruck = 'assets/svgs/open_truck.svg';
  static const String enclosedTruck = 'assets/svgs/enclosed_truck.svg';
  static const String vanKombi = 'assets/svgs/van_kombi.svg';
  static const String refrigeratedTruck = 'assets/svgs/refrigerated_truck.svg';
  static const String towTruck = 'assets/svgs/tow_truck.svg';
  static const String tripperTruck = 'assets/svgs/tripper_truck.svg';
  static const String brickTruck = 'assets/svgs/brick_truck.svg';
  static const String viewRates = 'assets/svgs/view_rates.svg';
  static const String contactUs = 'assets/svgs/contact_us.svg';
  static const String myOrder = 'assets/svgs/order.svg';
  static const String termAndCondition = 'assets/svgs/terms_and_conditions.svg';
  static const String share = 'assets/svgs/share.svg';
  static const String bell = 'assets/svgs/bell.svg';
  static const String menu = 'assets/svgs/menu_icon.svg';
  static const String search = 'assets/svgs/search.svg';
  static const String locationIcon = 'assets/svgs/location_icon.svg';
  static const String dollar = 'assets/svgs/dollar.svg';
  static const String info = 'assets/svgs/info.svg';
  static const String unselectedStar = 'assets/svgs/unselected_star.svg';
  static const String backgroundCurve = 'assets/svgs/background_curve.svg';
  static const String moveToCurrentLocaitonIcon =
      'assets/svgs/move_to_current_location_icon.svg';

  ///images
  static const String splash = 'assets/images/splash.png';
  static const String chat = 'assets/svgs/chat.svg';
}
