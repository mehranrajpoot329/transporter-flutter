class AppStrings {
  ///Exception Message --->
  static String firebaseAuthE =
      'The sms verification code used to create the phone auth credential is invalid. Please resend the verification code sms and be sure use the verification code provided by the user.';
  static String firebaseExp =
      'The sms code has expired. Please re-send the verification code to try again.';

  ///<---

  static const String signIn = 'Sign in';
  static const String googleKey = 'AIzaSyCSgq_YA2E4ed1qaztDoLDzl0an8vn7tvA';
  static const String enterYourPhoneNumberToStart =
      'Enter your phone number to start.';
  static const String iAgreeToTheTermsAndConditions =
      'I agree to the terms & conditions';
  static const String requestCode = 'Request Code';
  static const String otp = 'OTP';
  static const String cancle = 'cancel';
  static const String payedAmount = 'Payed Amount:';
  static const String submit = 'Submit';

  static const String pleaseEnterVerificationCodeWeJustSendToYourPhoneNumber =
      'Please enter verification code. we just send to your phone number';
  static const String verificationCode = 'Verfication Code';
  static const String iDidNotReceiveCode = 'I didn\'t receiver code';
  static const String editPhoneNumber = 'Edit Phone Number';
  static const String clear = 'Clear';
  static const String verify = 'Verify';

  static const String profile = 'Profile';
  static const String firstName = 'First Name';
  static const String enterFirstName = ' Enter First Name';
  static const String lastName = 'Last Name';
  static const String enterLastName = 'Enter Last Name';
  static const String email = 'Email';
  static const String enterEmail = 'Enter Email';
  static const String address = 'Address';
  static const String enterYourAddress = 'Address';
  static const String optional = 'Optional';
  static const String create = 'Create';

  static const String userName = 'Jhon Doe';
  static const String home = 'Home';
  static const String myOrders = 'My Orders';
  static const String orderCompletion = 'Order Completion';
  static const String orderCancellation = 'Order Cancellation';
  static const String yourOrderhasBeenCompletedSuccessfully =
      'Your Order has been completed successfully.';
  static const String notification = 'Notifications';
  static const String viewRates = 'View Rates';
  static const String termsAndConditions = 'Terms & Conditions';
  static const String aboutUs = 'About Us';
  static const String contactUs = 'Contact Us';
  static const String shareOptions = 'Share Options';
  static const String signOut = 'Sign Out';

  static const String nextbtnText = 'Next';
  static const String currentLocation = 'Current Location';

  static const String destionationLocation = 'Destination Location';
  static const String confirm = 'Confirm';

  static const String change = 'Change';

  static const String bookNowWhichYouWant = 'Book now which you want';
  static const String capacity = 'Capacity';
  static const String selectCapacity = 'Select Capacity';
  static const String estimatedAmount = 'Estimaetd Amount';
  static const String language = 'Select Language';
  static const String paymentMethod = 'Payment Method :';

  static const String orderSummary = 'Order Summary';
  static const String yourLocaiton = 'Your Location :';
  static const String destinationLocation = 'Destination Location :';
  static const String selectedTruck = 'Selected Truck :';
  static const String anyNotes = 'Any Notes :';
  static const String ifAny = 'If any...';

  static const String searching = 'Searching...';
  static const String pleaseWait = 'Please Wait';

  static const String weHaveFound = 'We Have Found';
  static const String contractorName = 'Contractor Name';
  static const String anyDescriptionAboutHim = 'Any description about him';
  static const String arrivalTime = 'Arrival Time';
  static const String wantToChat = 'Want To Chat?';

  static const contractorHasArrived = 'Contractor has arrived.';
  static const jobIsInProgress = 'Job is in Progress...';

  static const String paymentDetails = 'Payment Details';
  static const String usdCash = 'USD Cash';
  static const String bankHolderName = 'Bank Holder Name';
  static const String bankAccountNumber = 'Bank Account Number';
  static const String copy = 'Copy';
  static const String paymentComplete = 'Payment Complete';

  static const String confirmPayment =
      'Please Confirm (\$600) this amount has been payed by you';

  static const String rateAndReview = 'Rate & Review';
  static const String howThisJob = 'How\'s this jon?';
  static const String yourReview = 'Your Review';

  static const String chat = 'Chat';
  static const String typeAMessage = 'Type a message';

  static const String detail = 'Details';

  static const String driverIsOnHisWal = 'Driver is on his way...';

  static const String searchLocationFromMap = 'Search location from map';
  static const String searchResult = 'Search Results';
  static const String termsAndConsitionsLongText =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at lacinia eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean eget sem scelerisque, faucibus mi ut, tempor arcu. Aliquam et elementum mi. Maecenas sed vehicula risus. Duis in posuere mauris. Aliquam neque sapien, pharetra id egestas eu, accumsan sit amet tortor. Ut eget placerat nisl. Fusce imperdiet tincidunt laoreet. In hac habitasse platea dictumst. Aliquam erat volutpat. Mauris tempus nibh non vestibulum cursus. Proin quis arcu eget mauris euismod cursus nec non massa. Ut varius elit ac ligula sollicitudin, eget venenatis libero finibus. Fusce dignissim in mi quis rhoncus. Phasellus tristique viverra nisi, aliquet eleifend lectus ultrices eu. Pellentesque in vulputate sapien. Donec lacinia pellentesque enim, in vestibulum sem luctus at. Integer aliquam nibh a ullamcorper tempor. Proin non quam porttitor, eleifend eros ut, viverra tellus. Maecenas id elit rutrum, venenatis dolor ut, efficitur nisl. Fusce ullamcorper porttitor suscipit. Etiam pulvinar sapien nec erat malesuada, ut posuere libero congue. Aliquam imperdiet lacus sit amet iaculis ornare. Maecenas bibendum laoreet eleifend. Curabitur eget malesuada ligula. Duis et leo eget sapien pulvinar dignissim. Pellentesque sed arcu vel nunc efficitur cursus. Aenean eu velit mauris. Phasellus faucibus lacus sit amet nibh porttitor, et condimentum sapien suscipit.';

  static const String aboutUsLongText =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at lacinia eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean eget sem scelerisque, faucibus mi ut, tempor arcu. Aliquam et elementum mi. Maecenas sed vehicula risus. Duis in posuere mauris. Aliquam neque sapien, pharetra id egestas eu, accumsan sit amet tortor. Ut eget placerat nisl. Fusce imperdiet tincidunt laoreet. In hac habitasse platea dictumst. Aliquam erat volutpat. Mauris tempus nibh non vestibulum cursus. Proin quis arcu eget mauris euismod cursus nec non massa. Ut varius elit ac ligula sollicitudin, eget venenatis libero finibus. Fusce dignissim in mi quis rhoncus. Phasellus tristique viverra nisi, aliquet eleifend lectus ultrices eu. Pellentesque in vulputate sapien. Donec lacinia pellentesque enim, in vestibulum sem luctus at. Integer aliquam nibh a ullamcorper tempor. Proin non quam porttitor, eleifend eros ut, viverra tellus. Maecenas id elit rutrum, venenatis dolor ut, efficitur nisl. Fusce ullamcorper porttitor suscipit. Etiam pulvinar sapien nec erat malesuada, ut posuere libero congue. Aliquam imperdiet lacus sit amet iaculis ornare. Maecenas bibendum laoreet eleifend. Curabitur eget malesuada ligula. Duis et leo eget sapien pulvinar dignissim. Pellentesque sed arcu vel nunc efficitur cursus. Aenean eu velit mauris. Phasellus faucibus lacus sit amet nibh porttitor, et condimentum sapien suscipit.';

  static const String save = 'Save';
  static const String editProfile = 'Edit';

  ///local storage strings
  static const String isLoggedIn = 'isLoggedIn';
  static const String isLoggedOut = 'isLoggedOut';
  static const String isUserSignedUp = 'isUserSignedUp';
  static const String authToken = 'authToken';
  static const String fcmToken = 'fcmToken';
  static const String isNew = 'isNew';
  static const String user = 'user';
  static const String firebaseCredentails = 'firebaseCredentails';
  static const String onGQLGoingOrder = 'onGQLGoingOrder';
  static const String onGoingOrder = 'onGoingOrder';

  //
  static const String notificationId = 'high_importance_channel';
  static const String notificationTitle = 'High Importance Notifications';
  static const String notificationDesc =
      'This channel is used for important notifications';
}
