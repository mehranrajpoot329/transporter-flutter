import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_theme.dart';
import 'package:transporter_customer/config/map_icon_controller.dart';
import 'package:transporter_customer/config/push_notification_config.dart';
import 'package:transporter_customer/graphql/graphql_config.dart';
import 'package:transporter_customer/routes/app_routes.dart';

import 'local/db/local_storage.dart';

void init() {
  log('........Initializing.....');
  Get.put(MapIconController());
  log('........Initialized.....');
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await HiveHelper.initHiveHelper();
  FirebaseMessaging.onBackgroundMessage(
      PushNotificationConfig.handleBackgroundPushNotifications);
  PushNotificationConfig.initNotifications();
  init();
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle());
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return GraphQLProvider(
          client: GraphQLConfigV2.client(),
          child: GetMaterialApp(
            theme: AppTheme.appTheme(),
            debugShowCheckedModeBanner: (kDebugMode) ? true : false,
            builder: (context, child) {
              return MediaQuery(
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                child: child!,
              );
            },
            initialRoute: AppRoutes.initialRoute,
            getPages: AppRoutes.routes,
            title: 'Transport Customer',
          ),
        );
      },
    );
  }
}
