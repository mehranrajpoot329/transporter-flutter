import 'package:flutter/rendering.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';

class TextStyles {
  static TextStyle regularStyle = TextStyle(
      fontFamily: AppFonts.sarabunRegular,
      fontSize: 9.sp,
      color: AppColors.textBlack);
  static TextStyle semiBoldStyle = TextStyle(
      fontFamily: AppFonts.sarabunSemiBold,
      fontSize: 14.sp,
      color: AppColors.textBlack);
  static TextStyle boldStyle = TextStyle(
      fontFamily: AppFonts.sarabunBold,
      fontSize: 14.sp,
      color: AppColors.textBlack);
  static TextStyle mediumStyle = TextStyle(
      fontFamily: AppFonts.sarabunMedium,
      fontSize: 14.sp,
      color: AppColors.textBlack);
}
