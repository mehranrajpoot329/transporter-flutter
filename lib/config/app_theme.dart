import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';

class AppTheme {
  static ThemeData appTheme() {
    return ThemeData(
      iconTheme: const IconThemeData(
        color: AppColors.textBlack,
      ),
      fontFamily: AppFonts.sarabunSemiBold,
      shadowColor: AppColors.lightGray,
      scaffoldBackgroundColor: AppColors.white,
      progressIndicatorTheme: const ProgressIndicatorThemeData(
        color: AppColors.textBlack,
      ),
      appBarTheme: const AppBarTheme(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: AppColors.black,
          statusBarIconBrightness: Brightness.light,
          systemNavigationBarDividerColor: AppColors.transparent,
        ),
      ),

      /// Elevated Button Theme
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          primary: AppColors.textBlack,
        ),
      ),

      /// Text Button Theme
      textButtonTheme: TextButtonThemeData(
        style: TextButton.styleFrom(
          primary: AppColors.textBlack,
          textStyle: TextStyle(
              fontSize: 13.0.sp, fontFamily: AppFonts.sarabunSemiBold),
        ),
      ),
      // inputDecorationTheme: InputDecorationTheme(
      //   focusedBorder: const OutlineInputBorder(
      //     borderRadius: BorderRadius.all(
      //       Radius.circular(7),
      //     ),
      //   ),
      //   enabledBorder: const OutlineInputBorder(
      //     borderRadius: BorderRadius.all(
      //       Radius.circular(7),
      //     ),
      //   ),
      //   border: const OutlineInputBorder(
      //     borderRadius: BorderRadius.all(
      //       Radius.circular(7),
      //     ),
      //   ),
      //   labelStyle: TextStyle(
      //     overflow: TextOverflow.ellipsis,
      //     color: AppColors.textBlack,
      //     fontSize: 11.5.sp,
      //     fontFamily: AppFonts.sarabunSemiBold,
      //   ),
      //   hintStyle: TextStyle(
      //     color: AppColors.veryDarkGray,
      //     fontSize: 12.5.sp,
      //   ),
      //   focusColor: AppColors.lightGray,
      //   filled: true,
      //   fillColor: AppColors.gray,
      // ),

      textTheme: TextTheme(
        headline1: TextStyle(
          fontSize: 23.0.sp,
          letterSpacing: -0.5,
          fontFamily: AppFonts.sarabunBold,
          color: AppColors.black,
        ),
        headline4: TextStyle(
            fontSize: 13.0.sp,
            fontFamily: AppFonts.sarabunBold,
            color: AppColors.textBlack),
        headline5: TextStyle(
            fontSize: 13.5.sp,
            fontFamily: AppFonts.sarabunBold,
            color: AppColors.textBlack),

        /// This style automatically applies on all the [Text]
        bodyText2: TextStyle(
            fontSize: 12.5.sp,
            fontWeight: FontWeight.w300,
            fontFamily: AppFonts.sarabunSemiBold,
            color: AppColors.textBlack),

        /// Using this style for radio button text and other place with 16.0.sp and #0d1111 color
        bodyText1: TextStyle(
            fontSize: 13.0.sp,
            fontFamily: AppFonts.sarabunSemiBold,
            color: AppColors.textBlack),

        /// This style automatically applies on the text of [ElevatedButton, OutlinedButton]
        /// also we can define different text style for these button in there respective
        /// theme data above
        button: TextStyle(
            fontSize: 13.0.sp,
            fontFamily: AppFonts.sarabunSemiBold,
            color: AppColors.yellow),

        //text size semiBold black
        headline6: TextStyle(
            fontSize: 13.sp,
            fontFamily: AppFonts.sarabunBold,
            color: AppColors.textBlack),
      ),
    );
  }
}
