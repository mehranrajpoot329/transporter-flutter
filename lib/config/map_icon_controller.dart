import 'dart:ui' as ui;
import 'dart:ui';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:transporter_customer/constants/app_images.dart';

class MapIconController extends GetxController {
  ///custom markers
  late BitmapDescriptor locationIcon;

  @override
  void onInit() {
    bitmapDescriptorFromSvgAsset(AppImages.locationIcon, 0);
    super.onInit();
  }

  Future<void> bitmapDescriptorFromSvgAsset(
      String assetName, int iconType) async {
    String svgString = await rootBundle.loadString(assetName);
    DrawableRoot svgDrawableRoot =
        await svg.fromSvgString(svgString, 'LocationIcon');
    ui.Picture picture = svgDrawableRoot.toPicture(size: const Size(50, 80));
    ui.Image image = await picture.toImage(50, 80);
    ByteData? bytes = await image.toByteData(format: ui.ImageByteFormat.png);
    locationIcon = BitmapDescriptor.fromBytes(bytes!.buffer.asUint8List());
    update();
  }
}
