import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transporter_customer/network/color_logges.dart';
import 'package:transporter_customer/routes/app_pages.dart';
import 'package:transporter_customer/utils/utils.dart';

class FirebaseApi {
  static final FirebaseApi _singleton = FirebaseApi._internal();

  factory FirebaseApi() {
    return _singleton;
  }

  FirebaseApi._internal();

  static final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  static RxBool isLoading = false.obs;
  static late String codeSentVid;
  static late String timeOutVid;
  static bool isTimeOut = false;
  static bool isResending = false;
  static int? forceToken;
  static late String mPhoneNumber;
  static RxBool isDisableResendButton = false.obs;
  static RxBool isNavigationRequired = false.obs;

  static Future<void> phoneSignIn(
      {required String phoneNumber, required bool isNavigationReq}) async {
    mPhoneNumber = phoneNumber;
    print('Phone Number is $mPhoneNumber');
    isNavigationRequired.value = isNavigationReq;
    Utils.showProgressBar();
    try {
      await _firebaseAuth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        verificationCompleted: _onVerificationCompleted,
        verificationFailed: _onVerificationFailed,
        codeSent: _onCodeSent,
        timeout: 100.seconds,
        codeAutoRetrievalTimeout: _onCodeTimeout,
      );
    } catch (e) {
      Utils.dismissProgressBar();
      Get.showSnackbar(Utils.errorSnackBar(message: e.toString()));
    }
  }

  static Future<User?> mVerifyPhoneNumber({required String smsCode}) async {
    PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: isTimeOut ? timeOutVid : codeSentVid, smsCode: smsCode);
    final User? user =
        (await _firebaseAuth.signInWithCredential(credential)).user;
    if (user != null) {
      isTimeOut = false;
      isResending = false;
    }
    return user;
  }

  static _onVerificationCompleted(PhoneAuthCredential authCredential) async {
    /// Don't User this below line it will verify your otp mean's utilize it and when you try to verify
    /// otp manually firebase will give you exception that otp is expired try sending new otp
    /// verify otp by using [verifyPhoneNumber]
    // UserCredential userCredential =
    //     await _firebaseAuth.signInWithCredential(authCredential);
    if (isDisableResendButton.isFalse) {
      disableResendOTPButton();
    }
    isLoading.value = false;
    Utils.dismissProgressBar();
    // if (authCredential.smsCode?.isNotEmpty ?? false) {
    //   moveToCodeScreen(verificationId: userVId.value);
    // }
  }

  static _onVerificationFailed(FirebaseAuthException exception) {
    Utils.dismissProgressBar();
    print('Phone Number is $mPhoneNumber');
    if (isDisableResendButton.isFalse) {
      disableResendOTPButton();
    }
    String message = exception.message.toString().contains('format')
        ? 'Invalid Phone Number'
        : exception.message.toString();
    Get.showSnackbar(Utils.errorSnackBar(message: message));
  }

  static _onCodeSent(String verificationId, int? fToken) async {
    codeSentVid = verificationId;
    forceToken = fToken ?? -1;

    debugPrint("OTP send Successfully : $verificationId");
    Utils.dismissProgressBar();
    if (isDisableResendButton.isFalse) {
      disableResendOTPButton();
    }
    if (isNavigationRequired.isTrue) {
      moveToCodeScreen(verificationId: codeSentVid, fToken: forceToken);
    }
  }

  static _onCodeTimeout(String timeOutVID) {
    Utils.dismissProgressBar();
    if (isDisableResendButton.isFalse) {
      disableResendOTPButton();
    }
    isTimeOut = true;
    timeOutVid = timeOutVID;
    return null;
  }

  static moveToCodeScreen({
    required String verificationId,
    int? fToken,
  }) {
    var data = [
      {'phone': mPhoneNumber},
      {'verificationId': verificationId},
      {'fToken': fToken ?? forceToken},
    ];
    Get.toNamed(
      AppPages.otpCode,
      arguments: data,
    );
  }

  ///logout firebase user
  static Future<void> logout() async {
    try {
      await FirebaseAuth.instance.signOut();
    } catch (e) {
      LoggerMessage.logError(msg: e.toString());
    }
  }

  static User? getFirebaseCurrentUser() {
    User? user = FirebaseAuth.instance.currentUser;
    return user;
  }

  static void showPhoneAuthExcMessage(FirebaseAuthException e) {
    Utils.dismissProgressBar();
    if (e.code == 'invalid-verification-code') {
      Get.showSnackbar(
          Utils.errorSnackBar(message: 'Invalid Verification Code'));
    } else {
      Get.showSnackbar(Utils.errorSnackBar(message: e.message.toString()));
    }
  }

  static void resendOTP(
      {required String pNumber, required bool isNavReq}) async {
    isResending = true;
    await phoneSignIn(phoneNumber: pNumber, isNavigationReq: isNavReq);
  }

  static void disableResendOTPButton() {
    isDisableResendButton.value = true;
    Timer(30.seconds, () {
      isDisableResendButton.value = false;
    });
  }
}
