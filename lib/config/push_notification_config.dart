import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../constants/app_strings.dart';

class PushNotificationConfig {
  static FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  static const AndroidNotificationChannel channel = AndroidNotificationChannel(
      AppStrings.notificationId, // id
      AppStrings.notificationTitle,
      description: AppStrings.notificationDesc,
      importance: Importance.high,
      playSound: true);

  static initNotifications() async {
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
    handleForeGroundPushNotifications();
  }

  static handleForeGroundPushNotifications() {
    FirebaseMessaging.onMessage.listen(
      (RemoteMessage message) async {
        log(message.toString());
        RemoteNotification? notification = message.notification;
        AndroidNotification? android = message.notification?.android;
        if (notification != null && android != null) {
          flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channelDescription: channel.description,
                color: Colors.blue,
                playSound: true,
                icon: '@mipmap/ic_launcher',
              ),
              iOS: const IOSNotificationDetails(
                  presentSound: true, presentBadge: true, presentAlert: true),
            ),
          );
        }
      },
    );
  }

  static Future<void> handleBackgroundPushNotifications(
      RemoteMessage message) async {
    await Firebase.initializeApp();
    debugPrint('A bg message just showed up :  ${message.messageId}');
    RemoteNotification? notification = message.notification;
    debugPrint('Title: ${notification!.title!}');
    debugPrint('Body: ${notification.body!}');
  }
}
