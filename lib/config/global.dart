import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:transporter_customer/constants/app_colors.dart';

final mySystemTheme = SystemUiOverlayStyle.dark.copyWith(
    systemNavigationBarColor: AppColors.black,
    statusBarBrightness: Brightness.dark,
    statusBarIconBrightness: Brightness.light,
    statusBarColor: AppColors.black);

void setSystemNavBarConfig({
  Color? systemNavigationBarColor,
  required Color statusBarColor,
  required Brightness iosStatusBarBrightness,
  required Brightness androidStatusBarIconBrightness,
}) {
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: statusBarColor,
      statusBarBrightness: iosStatusBarBrightness,

      ///this one is used for changing ios status bar color
      statusBarIconBrightness: androidStatusBarIconBrightness,

      systemNavigationBarColor: systemNavigationBarColor,
      systemNavigationBarIconBrightness: Brightness.dark,
    ),
  );
}

void openFullScreenDialog(Widget widget) {
  if (Get.isDialogOpen ?? false) Get.back();
  if (Get.isSnackbarOpen) Get.closeAllSnackbars();
  Get.dialog(
    widget,
    barrierColor: AppColors.transparent,
    barrierDismissible: false,
    useSafeArea: false,
    transitionCurve: Curves.easeIn,
    transitionDuration: 500.milliseconds,
  );
}

void removeFullScreenDialog() {
  if (Get.isDialogOpen ?? false) {
    Get.back();
  }
}
