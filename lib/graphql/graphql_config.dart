import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../network/end_points.dart';

class GraphQLConfig {
  static final HttpLink httpLink = HttpLink(EndPoints.graphQLURL);

  static final WebSocketLink webSocketLink = WebSocketLink(
    EndPoints.graphQLWebSocketURL,
    config: const SocketClientConfig(
      autoReconnect: true,
      inactivityTimeout: Duration(seconds: 30),
    ),
  );
  static final Link link = httpLink.concat(webSocketLink);

  static ValueNotifier<GraphQLClient> initClient() {
    ValueNotifier<GraphQLClient> client = ValueNotifier(
      GraphQLClient(
        cache: GraphQLCache(),
        link: httpLink.concat(webSocketLink),
      ),
    );
    return client;
  }
}

class GraphQLConfigV2 {
  static final HttpLink _httpLink = HttpLink(EndPoints.graphQLURL);

  static WebSocketLink _wsLink() => WebSocketLink(
        EndPoints.graphQLWebSocketURL,
        config: SocketClientConfig(
          inactivityTimeout: const Duration(hours: 1),
          autoReconnect: true,
          connect: (url, protocols) {
            ///Added webSockets to make sure it works even the application is in backGround
            var channel = WebSocketChannel.connect(url, protocols: protocols);
            channel = channel.forGraphQL();
            channel.stream.listen(
              (dynamic message) {
                ///Web Socket will keep sending message to keep the connection ALive
                // debugPrint('message $message');
              },
              onDone: () {
                debugPrint('ws channel closed');
              },
              onError: (error) {
                debugPrint('ws error $error');
              },
            );
            return channel;
          },
          delayBetweenReconnectionAttempts: const Duration(minutes: 5),
        ),
      );

  static Link _splitLink() => Link.split(
        (request) => request.isSubscription,
        _wsLink(),
        _httpLink,
      );

  static ValueNotifier<GraphQLClient> client() {
    ValueNotifier<GraphQLClient> client = ValueNotifier(
      GraphQLClient(
        cache: GraphQLCache(),
        link: _splitLink(),
      ),
    );
    return client;
  }
}
