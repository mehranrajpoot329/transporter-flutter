import 'package:graphql_flutter/graphql_flutter.dart';

class GQueries {
  static const String orderOperation = 'WatchOrderStatus';
  static const String locationOperation = 'WatchVendorLocation';

  static final orderStatusSubscription = gql(r'''subscription WatchOrderStatus{
   ORDER_STATUS{
    order_id
    vendor_id
    accepted_by
    customer_id
    reason
    price
    tax
    total_price
    order_status
    vendor_status
    vendor{
      first_name
      last_name
      phone_no
      image_url
    }
   }
  }''');

  static final locationSubscription = gql(r'''subscription WatchVendorLocation{
   LOCATION_UPDATE{
    lat
    lon
    user_id
    }
   }''');
}
