import 'package:dio/dio.dart';
import 'package:get/get.dart' hide Response, FormData;
import 'package:transporter_customer/network/base_response.dart';
import 'package:transporter_customer/network/end_points.dart';
import 'package:transporter_customer/network/logging.dart';
import 'package:transporter_customer/utils/utils.dart';

class DioClient {
  static const String _baseURL = EndPoints.baseURL;
  static final DioClient _singleton = DioClient._internal();

  factory DioClient() {
    return _singleton;
  }

  DioClient._internal();

  final Dio _dio = Dio(
    BaseOptions(
      baseUrl: _baseURL,
      connectTimeout: 11000,
      receiveTimeout: 10000,
    ),
  )..interceptors.addAll([Logging()]);

  Future<BaseResponse> getRequest({
    required String endPoint,
    Map<String, dynamic>? queryParams,
    Map<String, dynamic>? headers,
  }) async {
    try {
      Utils.showProgressBar();
      Response response = await _dio.get(
        endPoint,
        queryParameters: queryParams,
        options: Options(headers: headers),
      );
      BaseResponse baseResponse = parseResponse(response);
      return baseResponse;
    } on DioError catch (e) {
      return handleException(e);
    }
  }

  Future<BaseResponse> postRequest({
    required String endPoint,
    FormData? body,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
  }) async {
    try {
      Utils.showProgressBar();
      Response response = await _dio.post(
        endPoint,
        data: body,
        queryParameters: queryParameters,
        options: Options(headers: headers),
      );
      BaseResponse baseResponse = parseResponse(response);
      return baseResponse;
    } on DioError catch (e) {
      return handleException(e);
    }
  }

  Future<BaseResponse> putRequest({
    required String endPoint,
    FormData? body,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
  }) async {
    try {
      Utils.showProgressBar();
      Response response = await _dio.put(
        endPoint,
        data: body,
        queryParameters: queryParameters,
        options: Options(headers: headers),
      );
      BaseResponse? baseResponse = parseResponse(response);
      return baseResponse;
    } on DioError catch (e) {
      return handleException(e);
    }
  }

  Future<BaseResponse> deleteRequest({
    required String endPoint,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? body,
  }) async {
    try {
      Utils.showProgressBar();
      Response response = await _dio.delete(
        endPoint,
        queryParameters: queryParameters,
        options: Options(headers: headers),
      );
      BaseResponse? baseResponse = parseResponse(response);
      return baseResponse;
    } on DioError catch (e) {
      return handleException(e);
    }
  }

  BaseResponse parseResponse(Response response) {
    Utils.dismissProgressBar();
    if (response.statusCode == 200 && !response.data['error']) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      return baseResponse;
    } else {
      showMessage(message: response.data['message'], isError: true);
      return BaseResponse(
        error: true,
        message: response.data['message'],
      );
    }
  }

  void showMessage({required String message, required bool isError}) {
    if (message.toLowerCase().contains('DioError')) {
      message = 'Something went Wrong';
    }
    if (isError) {
      Get.showSnackbar(Utils.errorSnackBar(message: message));
    } else {
      Get.showSnackbar(Utils.successSnackBar(message: message));
    }
  }

  BaseResponse handleException(DioError e) {
    Utils.dismissProgressBar();
    showMessage(message: e.response?.data['message'] ?? e.error, isError: true);
    return BaseResponse(
      error: true,
      message: e.response?.statusMessage ?? e.error,
      statuCode: e.response?.statusCode,
    );
  }
}
