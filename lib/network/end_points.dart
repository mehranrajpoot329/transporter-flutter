class EndPoints {
  static const String baseURL = 'http://51.68.167.212:3002/user/customer/';
  static const String graphQLURL = 'http://51.68.167.212:3002/graphql';
  static const String graphQLWebSocketURL = 'ws://51.68.167.212:3002/graphql';
  static const String placeHolder = 'https://via.placeholder.com/150';
  static const String customerLogin = 'login';
  static const String customerSignup = 'signup';
  static const String notifications = 'notifications';
  static const String orders = 'orders';
  static const String contactUs = 'contactUs';
  static const String vehices = 'vehicles';
  static const String placeOrder = 'order/place';
  static const String ordersById = 'order';
  static const String endOrder = 'order/end';
  static const String cancelServiceOrder = 'order/cancel';
  static const String customerAddReview = 'review';
  static const String placeServiceOrder = 'order/place';
  static const String updatePassword = 'changePassword';
  static const String updateCustomer = 'update';
}
