class BaseResponse {
  bool? error;
  String? message;
  int? statuCode;
  dynamic data;

  BaseResponse({
    this.message,
    this.error = true,
    this.statuCode,
  });

  BaseResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    error = json['error'];
    data = json['data'] != null ? data = json['data'] : null;
  }
}
