import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/text_styles.dart';

class ShareOptionsScreen extends StatelessWidget {
  const ShareOptionsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          'Native Share Option Bottom Sheet Will Appear',
          style: TextStyles.boldStyle.copyWith(fontSize: 10.sp),
        ),
      ),
    );
  }
}
