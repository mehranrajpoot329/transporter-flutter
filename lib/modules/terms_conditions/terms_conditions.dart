import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/notifications/notifications_controller.dart';
import 'package:transporter_customer/modules/notifications/notification_item_widget.dart';
import 'package:transporter_customer/widgets/customer_appbar.dart';

class TermsAndConditionsScreen extends GetView<NotificationsController> {
  const TermsAndConditionsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomeAppBar(
        iconUrl: AppImages.back,
        title: AppStrings.termsAndConditions,
        onBack: () => Get.back(),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(left: 8.w, right: 8.w, top: 2.h),
          child: Text(
            AppStrings.termsAndConsitionsLongText,
            textAlign: TextAlign.justify,
            style: TextStyle(
              fontFamily: AppFonts.sarabunRegular,
              fontSize: 10.sp,
              color: AppColors.textBlack,
            ),
          ),
        ),
      ),
    );
  }
}
