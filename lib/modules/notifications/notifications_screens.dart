import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/notifications/notification_item_widget.dart';
import 'package:transporter_customer/modules/notifications/notifications_controller.dart';
import 'package:transporter_customer/widgets/customer_appbar.dart';

class NotificationsScreen extends GetView<NotificationsController> {
  const NotificationsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomeAppBar(
        iconUrl: AppImages.back,
        title: AppStrings.notification,
        onBack: () => Get.back(),
      ),
      body: SafeArea(
        child: GetBuilder<NotificationsController>(
          builder: (_) {
            return (controller.notificationsList.isEmpty)
                ? const Center(
                    child: Text('No Notifications'),
                  )
                : ListView.builder(
                    itemCount: controller.notificationsList.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return NotificationItemWidget(
                        notificationsModelData:
                            controller.notificationsList[index],
                      );
                    },
                  );
          },
        ),
      ),
    );
  }
}
