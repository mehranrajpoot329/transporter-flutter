import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/enums/order_status.dart';
import 'package:transporter_customer/models/api_models/notifications_model.dart';

class NotificationItemWidget extends StatelessWidget {
  final NotificationsModelData notificationsModelData;

  const NotificationItemWidget({
    Key? key,
    required this.notificationsModelData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      margin: EdgeInsets.symmetric(horizontal: 4.w, vertical: 1.h),
      padding: EdgeInsets.symmetric(vertical: 2.5.h),
      decoration: BoxDecoration(
        color: AppColors.gray,
        borderRadius: BorderRadius.circular(6),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: SizedBox(
              height: 7.4.h,
              child: CircleAvatar(
                backgroundColor: AppColors.yellow,
                child: SvgPicture.asset(
                  AppImages.bell,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  notificationsModelData.status == OrderStatus.completed
                      ? '${notificationsModelData.createdAt}'
                      : '${notificationsModelData.updatedAt}',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: AppColors.textBlack.withOpacity(0.5),
                    fontSize: 8.sp,
                    fontFamily: AppFonts.sarabunRegular,
                  ),
                ),
                Text(
                  notificationsModelData.status == OrderStatus.cancelled
                      ? AppStrings.orderCancellation
                      : AppStrings.orderCompletion,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: AppColors.textBlack,
                    fontSize: 16.sp,
                    fontFamily: AppFonts.sarabunMedium,
                  ),
                ),
                Text(
                  '${notificationsModelData.message}',
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: AppColors.textBlack.withOpacity(0.5),
                    fontSize: 8.sp,
                    fontFamily: AppFonts.sarabunMedium,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
