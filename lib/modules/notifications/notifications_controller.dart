import 'package:get/get.dart';
import 'package:transporter_customer/enums/order_status.dart';
import 'package:transporter_customer/models/api_models/notifications_model.dart';
import 'package:transporter_customer/network/base_response.dart';
import 'package:transporter_customer/network/end_points.dart';
import 'package:transporter_customer/network/requests.dart';
import 'package:transporter_customer/utils/utils.dart';

class NotificationsController extends GetxController {
  List<NotificationsModelData> notificationsList = [];
  String cancellationMessage = 'Your job has been cancelled';
  String completionMessage = 'Job ACCEPTED';
  @override
  onInit() {
    super.onInit();
    Future.delayed(500.milliseconds, () {
      getAllNotification();
    });
  }

  Future<void> getAllNotification() async {
    BaseResponse response = await DioClient().getRequest(
      endPoint: EndPoints.notifications,
    );
    if (response.error == false) {
      if (response.data != null) {
        for (var item in response.data) {
          var parsedItem = NotificationsModelData.fromJson(item);
          parsedItem.status = parsedItem.message == cancellationMessage
              ? OrderStatus.cancelled
              : OrderStatus.completed;
          parsedItem.createdAt = Utils.formatDateYMMMMD(parsedItem.createdAt!);
          parsedItem.updatedAt = Utils.formatDateYMMMMD(parsedItem.updatedAt!);
          notificationsList.add(parsedItem);
        }
        update();
      }
    }
  }
}
