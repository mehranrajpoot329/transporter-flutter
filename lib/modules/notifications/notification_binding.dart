import 'package:get/instance_manager.dart';
import 'package:transporter_customer/modules/notifications/notifications_controller.dart';

class NotificationsBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(NotificationsController());
  }
}
