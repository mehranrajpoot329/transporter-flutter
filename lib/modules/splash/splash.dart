import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/local/db/local_storage.dart';
import 'package:transporter_customer/modules/login/login_controller.dart';
import 'package:transporter_customer/routes/app_pages.dart';

class SplashScreen extends GetView<LoginController> {
  const SplashScreen({Key? key}) : super(key: key);

  void moveToNext() {
    bool isNew = HiveHelper.isNew();
    bool isAlreadyLoggedIn = HiveHelper.isLoggedIn();
    if (isNew) {
      Future.delayed(3.seconds, () => Get.offAllNamed(AppPages.phoneLogin));
      return;
    } else if (isAlreadyLoggedIn) {
      Future.delayed(3.seconds, () => Get.offAllNamed(AppPages.home));
      return;
    } else {
      if (HiveHelper.isLoggedOut() || !isAlreadyLoggedIn) {
        Future.delayed(3.seconds, () => Get.offAllNamed(AppPages.phoneLogin));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.dark,
    ));
    moveToNext();
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            AppImages.splash,
            height: Get.height,
            width: Get.width,
            fit: BoxFit.cover,
          ),
          Positioned(
            top: 7.h,
            left: 0,
            right: 0,
            child: Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: 10.h,
                alignment: Alignment.topCenter,
                child: Image.asset(
                  AppImages.appIcon,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
