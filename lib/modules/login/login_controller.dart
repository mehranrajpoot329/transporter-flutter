import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transporter_customer/config/firebase_api.dart';
import 'package:transporter_customer/utils/utils.dart';

class LoginController extends GetxController {
  RxBool isChecked = false.obs;
  late ScrollController scrollController;
  RxString pickedCountryCode = '+92'.obs;
  RxString phoneNumber = ''.obs;

  @override
  void onInit() {
    scrollController = ScrollController();
    super.onInit();
  }

  @override
  void onClose() {
    scrollController.dispose();
    super.onClose();
  }

  Future<void> isAgree() async {
    if (isChecked.isFalse) {
      Get.showSnackbar(
        Utils.errorSnackBar(message: 'Accept Terms & Conditions'),
      );
    } else if (phoneNumber.value.isEmpty) {
      Get.showSnackbar(
        Utils.errorSnackBar(message: 'Please Enter Phone Number'),
      );
    } else {
      await FirebaseApi.phoneSignIn(
          phoneNumber: '$pickedCountryCode$phoneNumber', isNavigationReq: true);
    }
  }
}
