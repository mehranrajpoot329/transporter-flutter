import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_images.dart';

class ParentScreen extends StatelessWidget {
  final Positioned topWidget;
  final List<Widget> children;
  final EdgeInsets padding;
  final bool resizeToAvoidBottomInset;

  const ParentScreen({
    Key? key,
    required this.topWidget,
    required this.children,
    required this.padding,
    required this.resizeToAvoidBottomInset,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      body: Stack(
        children: [
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: SvgPicture.asset(
              AppImages.backgroundCurve,
              fit: BoxFit.fill,
            ),
          ),
          topWidget,
          Positioned(
            bottom: 4.h,
            right: 9.w,
            left: 9.w,
            child: Container(
              padding: padding,
              width: Get.width,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: AppColors.black.withOpacity(0.2),
                    offset: const Offset(-2, 3),
                    blurRadius: 8,
                    spreadRadius: 3,
                  )
                ],
              ),
              child: Column(
                children: children,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
