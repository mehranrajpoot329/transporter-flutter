import 'package:get/instance_manager.dart';
import 'package:transporter_customer/modules/login/login_controller.dart';

class LoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(LoginController());
  }
}
