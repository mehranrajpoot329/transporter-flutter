import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/login/login_controller.dart';
import 'package:transporter_customer/modules/login/parent_screen.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';
import 'package:transporter_customer/widgets/check_box_listtile.dart';
import 'package:transporter_customer/widgets/textfields/phone_textfield.dart';

class PhoneLoginScreen extends StatelessWidget {
  PhoneLoginScreen({Key? key}) : super(key: key);

  var controller = Get.isRegistered<LoginController>()
      ? Get.find<LoginController>()
      : Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return ParentScreen(
      padding: EdgeInsets.only(top: 3.h, left: 4.w, right: 4.w, bottom: 0.h),
      resizeToAvoidBottomInset: true,

      ///TODO:Changed Here
      topWidget: Positioned(
        top: 18.h,
        left: 0,
        right: 0,
        child: Container(
          height: 10.h,
          alignment: Alignment.topCenter,
          child: Image.asset(
            AppImages.appIcon,
            fit: BoxFit.cover,
          ),
        ),
      ),
      children: [
        Text(
          AppStrings.signIn,
          style: TextStyles.boldStyle,
        ),
        Dimensions.y2,
        Text(
          AppStrings.enterYourPhoneNumberToStart,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: AppFonts.sarabunMedium,
            fontSize: 10.sp,
            color: AppColors.textBlack.withOpacity(0.6),
          ),
        ),
        Dimensions.y8,
        Dimensions.y5,
        PhoneTextFormField(
          onChanged: (value) {
            controller.pickedCountryCode.value = value.dialCode.toString();
          },
          onChangeTextFiled: (value) {
            controller.phoneNumber.value = value;
          },
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.allow(
              RegExp(r'[0-9]'),
            ),
            FilteringTextInputFormatter.deny(
              RegExp(r'^0+'), //users can't type 0 at 1st position
            ),
          ],
        ),
        Dimensions.getCustomeHeight(0.2),
        Obx(
          () => LabeledCheckbox(
            contentPadding: EdgeInsets.zero,
            activeColor: AppColors.black,
            fontSize: 10.sp,
            label: AppStrings.iAgreeToTheTermsAndConditions,
            onTap: (value) {
              controller.isChecked.value = value ?? !controller.isChecked.value;
            },
            value: controller.isChecked.value,
            bold: false,
            gap: 0,
          ),
        ),
        Dimensions.y5,

        ///Requeset button
        CustomeButton(
          title: AppStrings.requestCode,
          borderRadius: BorderRadius.circular(10.h),
          buttonWidth: Get.width,
          fontSize: 12.sp,
          titleColor: AppColors.yellow,
          onTap: () {
            controller.isAgree();
          },
        ),
        Dimensions.y3,
      ],
    );
  }
}
