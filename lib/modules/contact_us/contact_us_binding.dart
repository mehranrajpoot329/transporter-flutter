import 'package:get/instance_manager.dart';
import 'package:transporter_customer/modules/contact_us/contact_us_controller.dart';

class ContactUsBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ContactUsController());
  }
}
