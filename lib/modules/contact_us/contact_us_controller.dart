import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart' hide FormData;

import 'package:transporter_customer/network/base_response.dart';
import 'package:transporter_customer/network/color_logges.dart';
import 'package:transporter_customer/network/end_points.dart';
import 'package:transporter_customer/network/requests.dart';
import 'package:transporter_customer/utils/utils.dart';

class ContactUsController extends GetxController {
  final formKey = GlobalKey<FormBuilderState>();

  Future<void> contactUs() async {
    if (formKey.currentState!.saveAndValidate()) {
      FormData formData = FormData.fromMap({
        'first_name': formKey.currentState!.value['first_name'],
        'last_name': formKey.currentState!.value['last_name'],
        'email': formKey.currentState!.value['email'],
        'message': formKey.currentState!.value['message'],
      });
      BaseResponse response = await DioClient().postRequest(
        endPoint: EndPoints.contactUs,
        body: formData,
      );
      if (response.error == false) {
        LoggerMessage.logSuccess(msg: response.message.toString());
        Get.showSnackbar(Utils.successSnackBar(message: response.data));
      }
    }
  }
}
