import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/contact_us/contact_us_controller.dart';
import 'package:transporter_customer/modules/profile/textfield_with_label.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';
import 'package:transporter_customer/widgets/customer_appbar.dart';

class ContactUsScreen extends GetView<ContactUsController> {
  ContactUsScreen({Key? key}) : super(key: key);

  final _firstNameField = GlobalKey<FormBuilderFieldState>();
  final _lastNameField = GlobalKey<FormBuilderFieldState>();
  final _emailField = GlobalKey<FormBuilderFieldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: CustomeAppBar(
        iconUrl: AppImages.back,
        title: AppStrings.contactUs,
        onBack: () => Get.back(),
      ),
      body: FormBuilder(
        key: controller.formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.w),
          child: Column(
            children: [
              TextFormFieldWithLabel(
                hint: 'Enter First Name',
                keyboardType: TextInputType.text,
                label: 'First Name',
                name: 'first_name',
                onSaved: (value) {},
                validator: (value) {
                  if (value?.trim().isEmpty ?? true) {
                    return 'First Name is Required\n';
                  }
                  return null;
                },
                fieldKey: _firstNameField,
                isOptional: false,
                fontSize: 11,
                space: 1.2,
              ),
              Dimensions.y2,
              TextFormFieldWithLabel(
                hint: 'Enter Last Name',
                keyboardType: TextInputType.text,
                label: 'Enter Last Name',
                name: 'last_name',
                onSaved: (value) {},
                validator: (value) {
                  if (value?.trim().isEmpty ?? true) {
                    return 'Last Name is Required\n';
                  }
                  return null;
                },
                fieldKey: _lastNameField,
                isOptional: false,
                fontSize: 11,
                space: 1.2,
              ),
              Dimensions.y2,
              TextFormFieldWithLabel(
                hint: 'Enter Email',
                keyboardType: TextInputType.text,
                label: 'Enter Email',
                name: 'email',
                onSaved: (value) {},
                validator: (value) {
                  if (value?.trim().isEmpty ?? true) {
                    return 'Email is Required\n';
                  } else if (!value!.contains('@')) {
                    return 'Please Enter Valid Email\n';
                  }
                  return null;
                },
                fieldKey: _emailField,
                isOptional: false,
                fontSize: 11,
                space: 1.2,
              ),
              Flexible(child: Dimensions.y2),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(left: 0.8.w),
                  child: const Text(
                    'Message',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontFamily: AppFonts.sarabunRegular,
                      color: AppColors.textBlack,
                    ),
                  ),
                ),
              ),
              Dimensions.y1,
              _messageBox(),
              const Spacer(),
              CustomeButton(
                title: AppStrings.submit,
                fontSize: 15.sp,
                titleColor: AppColors.yellow,
                buttonWidth: Get.width / 1.3,
                borderRadius: BorderRadius.circular(10.h),
                onTap: () {
                  controller.contactUs();
                },
              ),
              Dimensions.getCustomeHeight(0.5)
            ],
          ),
        ),
      ),
    );
  }

  Container _messageBox() {
    return Container(
      height: 23.h,
      width: Get.width / 1.2,
      decoration: BoxDecoration(
        color: AppColors.gray,
        borderRadius: BorderRadius.circular(1.h),
      ),
      child: FormBuilderTextField(
        maxLines: null,
        minLines: 1,
        name: 'message',
        keyboardType: TextInputType.multiline,
        style: const TextStyle(
          fontFamily: AppFonts.sarabunRegular,
          color: AppColors.textBlack,
        ),
        validator: (value) {
          if (value?.isEmpty ?? true) {
            return 'Message is Required';
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: 'Write Message',
          hintStyle: TextStyle(
            fontFamily: AppFonts.sarabunRegular,
            color: AppColors.textBlack.withOpacity(0.5),
          ),
          border: InputBorder.none,
          contentPadding: EdgeInsets.symmetric(
            vertical: 2.h,
            horizontal: 4.w,
          ),
        ),
      ),
    );
  }
}
