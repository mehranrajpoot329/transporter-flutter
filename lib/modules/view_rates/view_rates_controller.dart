import 'package:get/get.dart';
import 'package:transporter_customer/models/api_models/vehicles_list_model.dart';
import 'package:transporter_customer/network/base_response.dart';
import 'package:transporter_customer/network/end_points.dart';
import 'package:transporter_customer/network/requests.dart';

class ViewRatesController extends GetxController {
  @override
  onInit() {
    super.onInit();
    Future.delayed(20.milliseconds, () {
      getAllVehicles();
    });
  }

  RxList<VehicleListModelData> vehiclesList = <VehicleListModelData>[].obs;

  Future<void> getAllVehicles() async {
    BaseResponse response = await DioClient().getRequest(
      endPoint: EndPoints.vehices,
    );
    if (response.error == false) {
      for (var item in response.data) {
        vehiclesList.add(VehicleListModelData.fromJson(item));
      }
    }
  }
}
