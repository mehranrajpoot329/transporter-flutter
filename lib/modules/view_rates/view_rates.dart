import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/local/dummy/dummy.dart';
import 'package:transporter_customer/modules/view_rates/view_rates_controller.dart';
import 'package:transporter_customer/widgets/customer_appbar.dart';

class ViewRatesScreen extends GetView<ViewRatesController> {
  const ViewRatesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomeAppBar(
        iconUrl: AppImages.back,
        onBack: () => Get.back(),
        title: 'Rate List',
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5.0.w),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Minimum Rates of trucks',
                textAlign: TextAlign.start,
                style: TextStyles.mediumStyle.copyWith(
                  fontSize: 12.sp,
                  color: AppColors.textBlack.withOpacity(0.5),
                ),
              ),
              Dimensions.y2,
              Obx(
                () => Column(
                  children: List.generate(
                    controller.vehiclesList.length,
                    (index) => Container(
                      decoration: index.floor().isEven
                          ? BoxDecoration(
                              color: AppColors.gray,
                              borderRadius: BorderRadius.circular(5),
                            )
                          : const BoxDecoration(
                              color: AppColors.white,
                            ),
                      height: 5.h,
                      margin: EdgeInsets.symmetric(vertical: 0.5.h),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 2.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              controller.vehiclesList[index].vehicleName ?? '',
                              textAlign: TextAlign.start,
                              style: TextStyles.mediumStyle
                                  .copyWith(fontSize: 11.sp),
                            ),
                            Text(
                              '\$${controller.vehiclesList[index].rate}',
                              style: TextStyles.mediumStyle
                                  .copyWith(fontSize: 11.sp),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
