import 'package:get/get.dart';
import 'package:transporter_customer/modules/view_rates/view_rates_controller.dart';

class ViewRatesBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ViewRatesController>(() => ViewRatesController());
  }
}
