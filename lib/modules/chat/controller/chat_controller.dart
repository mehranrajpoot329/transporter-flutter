import 'dart:async';

import 'package:get/get.dart';
import 'package:messenger/controllers/threads_controller.dart';
import 'package:messenger/model/Message.dart';
import 'package:messenger/model/MessageMetaValue.dart';
import 'package:messenger/model/MessageType.dart';
import 'package:messenger/model/Thread.dart';
import 'package:transporter_customer/utils/utils.dart';

class ChatController extends GetxController {
  void sendMessage({
    required int messageType,
    required String? dataToSend,
    required String fcm,
    required String currentUserId,
    required Thread thread,
  }) {
    List<String> receiverList = <String>[];
    thread.users?.forEach((element) {
      if (element.entityID != currentUserId) {
        receiverList.add(element.entityID!);
      }
    });

    MessageMetaValue meta =
        MessageMetaValue(text: dataToSend, type: messageType);

    Message msg = Message(
      '',
      0.0,
      MessageType.ChatMessageHandler,
      0,
      0.0,
      0.0,
      0.0,
      null,
      null,
      null,
      null,
      currentUserId,
      currentUserId,
      meta,
    );
    msg.threadEntityId = thread.entityId!;
    msg.to = receiverList;
    ThreadsController.sendMessage(msg).then((value) {
      if (value.isError) {
        Get.showSnackbar(Utils.errorSnackBar(
            message: '${value.message}: Message is not delivered'));
        return;
      }
      return;
    }).onError((error, stackTrace) {
      Utils.log(message: error.toString());
    });
  }
}
