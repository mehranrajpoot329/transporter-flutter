import 'package:get/instance_manager.dart';
import 'package:transporter_customer/modules/chat/controller/chat_controller.dart';

class ChatBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ChatController>(() => ChatController());
  }
}
