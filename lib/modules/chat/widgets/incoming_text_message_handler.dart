import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:messenger/model/Message.dart';
import 'package:messenger/utils/time_utils.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/widgets/chat/link_text_widget.dart';

class IncomingTextMessageHandler extends StatelessWidget {
  final Message message;

  const IncomingTextMessageHandler({required this.message, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        ///Start margin
        SizedBox(
          width: 1.5.h,
        ),

        ///Main Content
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              IncomingMessageTextChildWidget(
                message: message,
              ),
              SizedBox(
                height: 0.5.h,
              ),

              ///Time view
              Container(
                margin: EdgeInsets.only(right: 9.0.h),
                child: Text(
                  TimeUtils.getTimeInddMMMyyHHMM(message.date ?? 0),
                  style: TextStyle(
                    color: AppColors.textBlack,
                    fontSize: 9.0.sp,
                  ),
                ),
              ),

              SizedBox(
                height: 2.h,
              ),
            ],
          ),
        ),

        ///end margin
        SizedBox(
          width: 12.0.w,
        ),
      ],
    );
  }
}

class IncomingMessageTextChildWidget extends StatelessWidget {
  final Message message;
  final double? width;
  const IncomingMessageTextChildWidget(
      {Key? key, required this.message, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      decoration: BoxDecoration(
        color: AppColors.textBlack,
        borderRadius: BorderRadius.circular(2.0.w),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 1.0.h, horizontal: 1.0.h),
        child: GetUtils.isURL(message.meta?.text ?? '')
            ? LinkTextWidget(
                message: message.meta?.text ?? '',
                isMe: false,
              )
            : SelectableText(
                message.meta?.text ?? '',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12.0.sp,
                ),
              ),
      ),
    );
  }
}
