import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:messenger/model/Message.dart';
import 'package:messenger/utils/time_utils.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/widgets/chat/link_text_widget.dart';

class OutgoingTextMessageHandler extends StatelessWidget {
  final Message message;
  const OutgoingTextMessageHandler({
    required this.message,
    Key? key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        ///start margin
        SizedBox(
          width: 12.w,
        ),

        ///Main content

        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              OutgoingMessageTextChildWidget(
                message: message,
                width: null,
              ),
              SizedBox(
                height: 0.5.h,
              ),

              ///Time view
              Container(
                margin: EdgeInsets.only(left: 9.0.h),
                child: Text(
                  TimeUtils.getTimeInddMMMyyHHMM(message.date ?? 0),
                  style: TextStyle(
                    color: AppColors.black,
                    fontSize: 9.0.sp,
                  ),
                ),
              ),

              ///bottom margin
              SizedBox(
                height: 2.h,
              ),
            ],
          ),
        ),

        ///end margin
        SizedBox(
          width: 1.5.h,
        ),
      ],
    );
  }
}

class OutgoingMessageTextChildWidget extends StatelessWidget {
  final Message message;
  final double? width;
  const OutgoingMessageTextChildWidget(
      {Key? key, required this.message, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      decoration: BoxDecoration(
        color: AppColors.lightGray,
        borderRadius: BorderRadius.circular(2.0.w),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 1.0.h, horizontal: 1.0.h),
        child: GetUtils.isURL(message.meta?.text ?? '')
            ? LinkTextWidget(
                message: message.meta?.text ?? '',
                isMe: true,
              )
            : SelectableText(
                message.meta?.text ?? '',
                style: TextStyle(
                  color: AppColors.black,
                  fontSize: 12.0.sp,
                ),
              ),
      ),
    );
  }
}
