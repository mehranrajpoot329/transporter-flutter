import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:messenger/controllers/threads_controller.dart';
import 'package:messenger/model/Message.dart';
import 'package:messenger/model/MessageMetaValue.dart';
import 'package:messenger/model/MessageType.dart';
import 'package:messenger/model/Thread.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/local/db/local_storage.dart';
import 'package:transporter_customer/utils/utils.dart';

class MessageInputLayout extends StatefulWidget {
  final int? position;
  final Thread? thread;
  final String? currentUserId;
  final TextEditingController messageController;
  final void Function(bool isMessageSent) onMessageSent;

  const MessageInputLayout({
    this.position,
    this.thread,
    required this.messageController,
    this.currentUserId,
    required this.onMessageSent,
    Key? key,
  }) : super(key: key);

  @override
  State<MessageInputLayout> createState() => _MessageInputLayoutState();
}

class _MessageInputLayoutState extends State<MessageInputLayout> {
  /// This variable will hold info of receiver if he is active in this thread or not
  List<String> receiverList = <String>[];
  String typingIndication = '';
  late final StreamSubscription receiverTypingStatusSubscription;

  @override
  void initState() {
    widget.thread?.users?.forEach((element) {
      if (element.entityID != widget.currentUserId) {
        receiverList.add(element.entityID!);
      }
    });
    super.initState();

    ThreadsController.observerTypingStatus(
        widget.thread!.entityId!, widget.thread!.receiverUserId!, (s) {
      typingIndication = s;
    }).then((StreamSubscription s) => receiverTypingStatusSubscription = s);
  }

  @override
  void dispose() {
    receiverTypingStatusSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: 9.h,
      decoration: BoxDecoration(
        color: AppColors.gray,
        boxShadow: [
          BoxShadow(
            color: AppColors.textBlack.withOpacity(0.1),
            blurRadius: 15,
            offset: const Offset(3, 4),
          ),
        ],
      ),
      child: Center(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: 7.w,
            ),
            Expanded(
              flex: 4,
              child: TextField(
                controller: widget.messageController,
                maxLines: 2,
                autofocus: false,
                minLines: 1,
                enableSuggestions: true,
                keyboardType: TextInputType.multiline,
                textInputAction: TextInputAction.send,
                onSubmitted: (value) {
                  if (value.trim().isEmpty) return;
                  _sendMessage(
                      messageType: MessageType.Text,
                      dataToSend: value,
                      fcm: HiveHelper.getFcmToken(),
                      currentUserId: widget.currentUserId!,
                      thread: widget.thread!);
                },
                decoration: InputDecoration(
                  hintText: 'Type a message',
                  fillColor: Colors.transparent,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 2.0.w, vertical: 1.0.h),
                  hintStyle: TextStyle(
                    fontFamily: AppFonts.sarabunRegular,
                    fontSize: 12.sp,
                  ),
                ),
                style: TextStyle(
                  fontFamily: AppFonts.sarabunRegular,
                  fontSize: 12.sp,
                ),
              ),
            ),
            Flexible(
              child: GestureDetector(
                onTap: () {
                  if (widget.messageController.text.trim().isEmpty) return;
                  _sendMessage(
                      messageType: MessageType.Text,
                      dataToSend: widget.messageController.text,
                      fcm: HiveHelper.getFcmToken(),
                      currentUserId: widget.currentUserId!,
                      thread: widget.thread!);
                },
                child: Container(
                  padding: EdgeInsets.all(0.5.h),
                  height: 5.h,
                  decoration: const BoxDecoration(
                    color: AppColors.black,
                    shape: BoxShape.circle,
                  ),
                  child: Transform.rotate(
                    angle: 200,
                    child: const Center(
                      child: Icon(
                        Icons.send_outlined,
                        color: AppColors.yellow,
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _sendMessage({
    required int messageType,
    required String? dataToSend,
    required String fcm,
    required String currentUserId,
    required Thread thread,
  }) {
    MessageMetaValue meta =
        MessageMetaValue(text: dataToSend, type: messageType);

    Message msg = Message(
      '',
      0.0,
      MessageType.ChatMessageHandler,
      0,
      0.0,
      0.0,
      0.0,
      null,
      null,
      null,
      null,
      currentUserId,
      currentUserId,
      meta,
    );
    msg.threadEntityId = thread.entityId!;
    msg.to = receiverList;
    ThreadsController.sendMessage(msg).then((value) {
      if (value.isError) {
        Get.showSnackbar(Utils.errorSnackBar(
            message: '${value.message}: Message is not delivered'));
        return;
      }
      widget.onMessageSent.call(true);
      return;
    }).onError((error, stackTrace) {
      widget.onMessageSent.call(false);
      Utils.log(message: error.toString());
    });
  }
}
