import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:messenger/config/Glob.dart';
import 'package:messenger/controllers/threads_controller.dart';
import 'package:messenger/model/Message.dart';
import 'package:messenger/model/MessageType.dart';
import 'package:messenger/model/Thread.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/global.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/local/db/local_storage.dart';
import 'package:transporter_customer/models/user_model.dart';
import 'package:transporter_customer/modules/chat/message_input_layout.dart';
import 'package:transporter_customer/modules/chat/scroll_behaviour.dart';
import 'package:transporter_customer/modules/chat/widgets/incoming_text_message_handler.dart';
import 'package:transporter_customer/modules/chat/widgets/outgoing_text_messagre_handler.dart';
import 'package:transporter_customer/widgets/customer_appbar.dart';

class ChatScreen extends StatefulWidget {
  final int? position;
  final Thread? thread;
  final String? currentUserId;

  const ChatScreen({
    this.position,
    this.thread,
    this.currentUserId,
    Key? key,
  }) : super(key: key);

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  List<Message> messages = <Message>[];
  final _scrollController = ScrollController();
  late final StreamSubscription _messagesSubscription;
  late User user;
  double deletedTimeStamp = -1;
  late TextEditingController messageController;

  @override
  void initState() {
    super.initState();
    user = HiveHelper.getUser();
    Glob().setThreadItemListener(false);
    ThreadsController.updateTypingState(
        widget.thread?.entityId ?? '', widget.currentUserId!, 'active');
    widget.thread?.addListener(() {
      setState(() {});
    });
    widget.thread?.users?.forEach((element) {
      if (element.entityID == widget.currentUserId) {
        deletedTimeStamp = element.deleted ?? -1;
      }
    });
    if (mounted) {
      animateToEndV2();
    }
    ThreadsController.listenChatBySingleMessage(widget.thread!.entityId!,
            widget.currentUserId!, _newMessageAdded, deletedTimeStamp)
        .then(
      (StreamSubscription s) {
        _messagesSubscription = s;
      },
    );
  }

  void animateToEndV2() async {
    await Future.delayed(const Duration(milliseconds: 300));
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _scrollController.animateTo(_scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 400),
          curve: Curves.fastOutSlowIn);
    });
  }

  void _newMessageAdded(Message message) {
    messages.add(message);
    messages.sort((a, b) => a.date!.compareTo(b.date!));
    setState(() {});
  }

  @override
  void dispose() {
    if (_messagesSubscription != null) {
      _messagesSubscription.cancel();
      widget.thread!.removeListener(() {});
      widget.thread!.unSubscribe();
    }
    super.dispose();
  }

  void animateToEnd() {
    if (_scrollController.hasClients) {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 400),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  void deactivate() {
    widget.thread!.removeListener(() {});
    Glob().setThreadItemListener(true);
    ThreadsController.updateTypingState(
        widget.thread!.entityId!, widget.currentUserId!, 'inactive');
    super.deactivate();
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    setSystemNavBarConfig(
      systemNavigationBarColor: AppColors.gray,
      statusBarColor: AppColors.black,
      iosStatusBarBrightness: Brightness.light,
      androidStatusBarIconBrightness: Brightness.light,
    );
    return Scaffold(
      appBar: CustomeAppBar(
        iconUrl: AppImages.back,
        onBack: () {
          Get.back();
        },
        title: 'Chat',
      ),
      body: Column(
        children: [
          Expanded(
            child: ScrollConfiguration(
              behavior: MyScrollBehavior(),
              child: ListView.builder(
                shrinkWrap: false,
                controller: _scrollController,
                itemCount: messages.length,
                itemBuilder: (context, position) {
                  return _messageHandler(messages[position]);
                },
              ),
            ),
          ),
          SizedBox(
            height: 1.0.h,
          ),
          MessageInputLayout(
            currentUserId: widget.currentUserId,
            position: widget.position,
            messageController: TextEditingController(),
            thread: widget.thread,
            onMessageSent: (isSent) {
              if (isSent) {
                animateToEnd();
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _messageHandler(Message message) {
    bool isOutgoing = message.from == user.firebaseKey;
    switch (message.meta?.type) {
      case MessageType.Text:
        return isOutgoing
            ? OutgoingTextMessageHandler(
                message: message,
              )
            : IncomingTextMessageHandler(
                message: message,
              );
      default:
        return Container(
          color: Colors.white,
          child: Text(
            'Unknown message type',
            style: TextStyle(
              color: Colors.black,
              fontSize: 14.0.sp,
            ),
          ),
        );
    }
  }
}
