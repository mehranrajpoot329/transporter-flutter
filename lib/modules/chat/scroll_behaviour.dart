import 'package:flutter/material.dart';

class MyScrollBehavior extends ScrollBehavior {
  ///This behaviour removes the scroll glow

  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}
