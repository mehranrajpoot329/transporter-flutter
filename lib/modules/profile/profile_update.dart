import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/local/db/local_storage.dart';
import 'package:transporter_customer/models/user_model.dart';
import 'package:transporter_customer/modules/profile/profile_controller.dart';
import 'package:transporter_customer/modules/profile/textfield_with_label.dart';
import 'package:transporter_customer/network/end_points.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';
import 'package:transporter_customer/widgets/customer_appbar.dart';

class ProfileUpdateScreen extends GetView<ProfileController> {
  const ProfileUpdateScreen({Key? key}) : super(key: key);

  Future<ImageSource?> pickImage() async {
    ImageSource? selectedSource = await Get.defaultDialog(
      title: 'Select From',
      content: Column(
        children: [
          GestureDetector(
            onTap: () {
              Get.back(result: ImageSource.gallery);
            },
            child: const ListTile(
              leading: Icon(Icons.image),
              title: Text(
                'Gallery',
                style: TextStyle(
                    color: AppColors.textBlack,
                    fontFamily: AppFonts.sarabunMedium),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Get.back(result: ImageSource.camera);
            },
            child: const ListTile(
              leading: Icon(Icons.photo_camera),
              title: Text(
                'Camera',
                style: TextStyle(
                  color: AppColors.textBlack,
                  fontFamily: AppFonts.sarabunMedium,
                ),
              ),
            ),
          ),
        ],
      ),
    );
    return selectedSource;
  }

  @override
  Widget build(BuildContext context) {
    User user = HiveHelper.getUser();
    String? imageURL = user.imageUrl;
    return Scaffold(
      appBar: CustomeAppBar(
        iconUrl: AppImages.back,
        title: AppStrings.profile,
        onBack: () => Get.back(),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0.w),
          child: SizedBox(
            height: Get.height,
            width: Get.width,
            child: FormBuilder(
              key: controller.formKey,
              child: Obx(
                () => Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Stack(
                      children: [
                        CacheNetworkProfileImage(
                          height: 13.h,
                          width: 13.h,
                          imageURl: imageURL,
                        ),
                        Obx(
                          () => controller.isEditAble.isFalse
                              ? const SizedBox()
                              : Positioned(
                                  bottom: 1.h,
                                  right: -9.w,
                                  left: 10.w,
                                  child: GestureDetector(
                                    onTap: () async {
                                      ImageSource? imageSource =
                                          await pickImage();
                                      if (imageSource == null) return;
                                      controller.selectImage(imageSource);
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(1.h),
                                      decoration: const BoxDecoration(
                                        color: AppColors.black,
                                        shape: BoxShape.circle,
                                      ),
                                      child: Center(
                                        child: SvgPicture.asset(
                                          AppImages.camera,
                                          fit: BoxFit.contain,
                                          color: AppColors.yellow,
                                          height: 2.h,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                        )
                      ],
                    ),
                    Dimensions.getCustomeHeight(1.5),
                    TextFormFieldWithLabel(
                      label: 'First Name',
                      hint: '',
                      space: 1,
                      initValue: user.firstName,
                      name: 'first_name',
                      editAble: controller.isEditAble.value,
                      textStyle: TextStyles.boldStyle.copyWith(fontSize: 11.sp),
                      lableOpaity: 0.4,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Enter First Name';
                        } else {
                          return null;
                        }
                      },
                      onSaved: (value) {
                        log(value.toString());
                      },
                      keyboardType: TextInputType.text,
                      isOptional: false,
                    ),
                    Dimensions.getCustomeHeight(1.5),
                    TextFormFieldWithLabel(
                      label: 'Last Name',
                      hint: '',
                      initValue: user.lastName,
                      textStyle: TextStyles.boldStyle.copyWith(fontSize: 11.sp),
                      space: 1,
                      name: 'last_name',
                      editAble: controller.isEditAble.value,
                      lableOpaity: 0.4,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Enter Last Name';
                        } else {
                          return null;
                        }
                      },
                      onSaved: (value) {
                        log(value.toString());
                      },
                      keyboardType: TextInputType.text,
                      isOptional: false,
                    ),
                    Dimensions.getCustomeHeight(1.5),
                    TextFormFieldWithLabel(
                      label: 'Email',
                      hint: '',
                      textStyle: TextStyles.boldStyle.copyWith(fontSize: 11.sp),
                      initValue: user.email,
                      space: 1,
                      name: 'email',
                      editAble: controller.isEditAble.value,
                      lableOpaity: 0.4,
                      hintOpacity: 0.5,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Enter Email';
                        } else {
                          return null;
                        }
                      },
                      onSaved: (value) {
                        log(value.toString());
                      },
                      keyboardType: TextInputType.text,
                      isOptional: false,
                    ),
                    Dimensions.getCustomeHeight(1.5),
                    TextFormFieldWithLabel(
                      hint: AppStrings.address,
                      name: 'address',
                      textStyle: TextStyles.boldStyle.copyWith(fontSize: 11.sp),
                      label: AppStrings.enterYourAddress,
                      keyboardType: TextInputType.streetAddress,
                      isOptional: true,
                      onSaved: (String? value) {},
                      validator: (String? value) {
                        return null;
                      },
                    ),
                    Dimensions.getCustomeHeight(4.5),
                    CustomeButton(
                      title: controller.isEditAble.value
                          ? AppStrings.save
                          : AppStrings.editProfile,
                      borderRadius: BorderRadius.circular(10.h),
                      buttonWidth: Get.width,
                      fontSize: 13.4.sp,
                      titleColor: AppColors.yellow,
                      onTap: () {
                        controller.allowUpdateProfile();
                      },
                    ),
                    // Dimensions.getCustomeHeight(3),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CacheNetworkProfileImage extends StatelessWidget {
  final double? height;
  final double? width;
  final String? imageURl;

  const CacheNetworkProfileImage({
    Key? key,
    this.height,
    this.width,
    this.imageURl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ProfileController controller = Get.find<ProfileController>();
    final String image = (imageURl?.isEmpty ?? true)
        ? "http://via.placeholder.com/200x150"
        : EndPoints.baseURL + 'profileImage' + imageURl!;
    return GetBuilder<ProfileController>(
      builder: (_) {
        return ClipOval(
          child: controller.profileImage != null
              ? Image.file(
                  controller.profileImage!,
                  height: height ?? 10.h,
                  width: width ?? 10.h,
                )
              : CachedNetworkImage(
                  height: height ?? 10.h,
                  width: width ?? 10.h,
                  imageUrl: image,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => const Center(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => const Center(
                    child: Icon(Icons.error),
                  ),
                ),
        );
      },
    );
  }
}
