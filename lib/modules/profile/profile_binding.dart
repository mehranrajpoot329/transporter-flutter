import 'package:get/instance_manager.dart';
import 'package:transporter_customer/modules/profile/profile_controller.dart';

class ProfileBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(ProfileController());
  }
}
