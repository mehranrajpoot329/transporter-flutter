import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart' hide FormData, MultipartFile;
import 'package:image_picker/image_picker.dart';
import 'package:messenger/config/Glob.dart';
import 'package:messenger/controllers/user_controller.dart';
import 'package:messenger/model/Availability.dart';
import 'package:messenger/model/UserWrapper.dart';
import 'package:transporter_customer/local/db/local_storage.dart';
import 'package:transporter_customer/models/user_model.dart';
import 'package:transporter_customer/modules/code/otp_code_controller.dart';
import 'package:transporter_customer/network/base_response.dart';
import 'package:transporter_customer/network/end_points.dart';
import 'package:transporter_customer/network/requests.dart';
import 'package:transporter_customer/routes/app_pages.dart';
import 'package:transporter_customer/utils/utils.dart';

class ProfileController extends GetxController {
  File? profileImage;
  RxBool isEditAble = false.obs;
  final formKey = GlobalKey<FormBuilderState>();

  Future<void> selectImage(ImageSource selectedSource) async {
    profileImage = await Utils.getImage(source: selectedSource);
    update();
  }

  Future<void> signUp(String phoneNumber) async {
    if (profileImage == null) {
      Get.showSnackbar(
          Utils.errorSnackBar(message: 'Please Select Profile Image'));
      return;
    } else {
      String fileName = profileImage!.path.split('/').last;
      if (formKey.currentState!.saveAndValidate()) {
        FormData formData = FormData.fromMap({
          'first_name': formKey.currentState!.value['first_name'],
          'last_name': formKey.currentState!.value['last_name'],
          'email': formKey.currentState!.value['email'],
          'address': formKey.currentState!.value['address'],
          'phone_no': phoneNumber,
          'fcm_token': await getFCMToken(),
          'profile_image': await MultipartFile.fromFile(profileImage!.path,
              filename: fileName)
        });
        BaseResponse response = await DioClient().postRequest(
          endPoint: EndPoints.customerSignup,
          body: formData,
        );
        if (response.error == false) {
          User user = User.fromJson(response.data);
          await HiveHelper.setAuthToken(user.token!);
          await HiveHelper.saveIsNew(isNew: false);
          await HiveHelper.saveUser(user);
          var login = Get.find<OTPController>();
          BaseResponse loginModel = await login.login(phoneNumber: phoneNumber);
          if (loginModel.error == false) {
            await authenticateUserWithMessenger(user);
            HiveHelper.saveLoggIn(isLoggedIn: true);
            Get.offAllNamed(AppPages.home);
          }
        }
      }
    }
  }

  Future<String?> getFCMToken() async {
    String? token = await FirebaseMessaging.instance.getToken();
    return token;
  }

  /// password is not required because the user is already login with phone number so dont needs to
  /// crate user with email and in customer dont required to login with
  /// and also don't needs to login user because its already login wtih phone number
  authenticateUserWithMessenger(User user) async {
    Utils.log(message: '@@@@ ---> User is authenticated with firebase');
    final userWrapper = UserWrapper.toServerObject(
      name: user.getFullName(),
      phone: user.phoneNo,
      userType: user.userType,
      pictureURL: user.imageUrl,
      email: user.email,
      availability: Availability.Available,
    );

    /// [user.firebaseKey] this will be the user name in lower-case
    userWrapper.entityId = user.firebaseKey;
    userWrapper.fcm = HiveHelper.getFcmToken();
    Glob().currentUserKey = user.firebaseKey;

    ///this will create user on firebase
    await UserController.authenticateUser(userWrapper);
  }

  Future<void> updateCustomer() async {
    if (formKey.currentState!.saveAndValidate()) {
      FormData formData = FormData.fromMap({
        'first_name': formKey.currentState!.value['first_name'],
        'last_name': formKey.currentState!.value['last_name'],
        'address': formKey.currentState!.value['address'],
        'email': formKey.currentState!.value['email'],
        'profile_image': (profileImage != null)
            ? await MultipartFile.fromFile(
                profileImage!.path,
                filename: profileImage!.path.split('/').last,
              )
            : ''
      });
      BaseResponse response = await DioClient().putRequest(
        endPoint: EndPoints.updateCustomer,
        body: formData,
      );
      if (response.error == false) {
        isEditAble.value = false;
        User user = User.fromJson(response.data);
        HiveHelper.saveUser(user);

        Get.showSnackbar(Utils.successSnackBar(
            message: 'Profile Data Updated Successfully'));
      }
    }
  }

  void allowUpdateProfile() {
    if (isEditAble.isTrue) {
      updateCustomer();
    } else {
      isEditAble.value = true;
    }
  }
}
