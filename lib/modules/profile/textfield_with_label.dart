import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';

class TextFormFieldWithLabel extends StatelessWidget {
  final String label;
  final String hint;
  final String name;
  final String? initValue;
  final bool? isOptional;
  final FocusNode? focusNode;
  final String? Function(String?)? validator;
  final void Function(String?)? onSaved;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final double? space;
  final TextStyle? textStyle;
  final Key? fieldKey;
  final double? fontSize;
  final double? lableOpaity;
  final void Function()? onTap;
  final double? hintOpacity;
  final bool editAble;
  final double? height;
  const TextFormFieldWithLabel(
      {Key? key,
      required this.label,
      required this.hint,
      this.inputFormatters,
      this.height,
      this.onTap,
      this.focusNode,
      this.textStyle,
      this.initValue,
      required this.name,
      this.lableOpaity,
      this.hintOpacity,
      this.fieldKey,
      this.editAble = true,
      this.fontSize,
      this.space,
      this.isOptional,
      required this.validator,
      required this.onSaved,
      required this.keyboardType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left: 0.8.w),
            child: Text.rich(
              TextSpan(
                text: label,
                style: TextStyle(
                  fontFamily: AppFonts.sarabunMedium,
                  fontSize: fontSize?.sp ?? 9.sp,
                  color: AppColors.textBlack.withOpacity(lableOpaity ?? 1),
                ),
                children: [
                  if (isOptional!)
                    TextSpan(
                      text: '',
                      style: TextStyle(
                          fontFamily: AppFonts.sarabunRegular,
                          fontSize: 8.sp,
                          color: AppColors.textBlack.withOpacity(0.4)),
                    ),
                ],
              ),
            ),
          ),
        ),
        Dimensions.getCustomeHeight(space ?? 0.5),
        Container(
          width: Get.width,
          height: height ?? 6.h,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: AppColors.gray,
            borderRadius: BorderRadius.circular(7),
          ),
          child: Center(
            child: FormBuilderTextField(
              name: name,
              initialValue: initValue,
              inputFormatters: inputFormatters,
              enabled: editAble,
              onSubmitted: (value) {
                focusNode?.unfocus();
                FocusScope.of(context).unfocus();
              },
              validator: validator,
              focusNode: focusNode,
              onEditingComplete: () {
                focusNode?.unfocus();
                FocusScopeNode().unfocus();
              },
              key: fieldKey,
              autofocus: false,
              onSaved: (value) {
                onSaved!(value);
                focusNode?.unfocus();
              },
              keyboardType: keyboardType,
              cursorColor: AppColors.textBlack,
              style: textStyle ??
                  TextStyle(
                      fontFamily: AppFonts.sarabunRegular, fontSize: 10.sp),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 3.w),
                hintText: hint,
                errorStyle: TextStyle(color: Colors.red, fontSize: 6.sp),
                border: InputBorder.none,
                hintStyle: TextStyle(
                    fontFamily: AppFonts.sarabunRegular,
                    fontSize: 10.sp,
                    color: AppColors.textBlack.withOpacity(hintOpacity ?? 0.8)),
              ),
            ),
          ),
        )
      ],
    );
  }
}
