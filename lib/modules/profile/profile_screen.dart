import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/login/parent_screen.dart';
import 'package:transporter_customer/modules/profile/profile_controller.dart';
import 'package:transporter_customer/modules/profile/textfield_with_label.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({Key? key}) : super(key: key);

  Future<ImageSource?> pickImage() async {
    ImageSource? selectedSource = await Get.defaultDialog(
      title: 'Select From',
      content: Column(
        children: [
          GestureDetector(
            onTap: () {
              Get.back(result: ImageSource.gallery);
            },
            child: const ListTile(
              leading: Icon(Icons.image),
              title: Text(
                'Gallery',
                style: TextStyle(
                    color: AppColors.textBlack,
                    fontFamily: AppFonts.sarabunMedium),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Get.back(result: ImageSource.camera);
            },
            child: const ListTile(
              leading: Icon(Icons.photo_camera),
              title: Text(
                'Camera',
                style: TextStyle(
                  color: AppColors.textBlack,
                  fontFamily: AppFonts.sarabunMedium,
                ),
              ),
            ),
          ),
        ],
      ),
    );
    return selectedSource;
  }

  ProfileController controller = Get.put(ProfileController());
  String phoneNumber = Get.arguments[0]['phone'].toString();

  @override
  Widget build(BuildContext context) {
    return ParentScreen(
      padding: EdgeInsets.only(
        top: 3.h,
        left: 5.w,
        right: 5.w,
        bottom: 3.h,
      ),
      resizeToAvoidBottomInset: true,
      topWidget: Positioned(
        top: 10.h,
        left: 0,
        right: 0,
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            ///Profile Image
            GetBuilder<ProfileController>(
              builder: (_) {
                return Container(
                  height: 13.h,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey.withOpacity(0.6),
                  ),
                  alignment: Alignment.topCenter,
                  child: controller.profileImage == null
                      ? Center(
                          child: SvgPicture.asset(
                            AppImages.profile,
                            fit: BoxFit.scaleDown,
                            height: 10.h,
                          ),
                        )
                      : ClipRRect(
                          borderRadius: BorderRadius.circular(10.h),
                          child: Image.file(
                            controller.profileImage!,
                            fit: BoxFit.cover,
                          ),
                        ),
                );
              },
            ),

            ///camera icon
            Positioned(
              bottom: 1.h,
              right: -9.w,
              left: 10.w,
              child: GestureDetector(
                onTap: () async {
                  ImageSource? imageSource = await pickImage();
                  if (imageSource == null) return;
                  controller.selectImage(imageSource);
                },
                child: Container(
                  padding: EdgeInsets.all(1.h),
                  decoration: const BoxDecoration(
                    color: AppColors.black,
                    shape: BoxShape.circle,
                  ),
                  child: Center(
                    child: SvgPicture.asset(
                      AppImages.camera,
                      fit: BoxFit.contain,
                      height: 2.h,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      children: [
        Text(
          AppStrings.profile,
          style: TextStyles.boldStyle.copyWith(fontSize: 14.sp),
        ),
        Dimensions.y3,
        FormBuilder(
          key: controller.formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            children: [
              TextFormFieldWithLabel(
                hint: AppStrings.enterFirstName,
                name: 'first_name',
                label: AppStrings.firstName,
                isOptional: false,
                keyboardType: TextInputType.name,
                onSaved: (String? value) {},
                validator: (String? value) {
                  return (value?.isEmpty ?? true)
                      ? 'First Name is Required'
                      : null;
                },
              ),
              Dimensions.getCustomeHeight(1),
              TextFormFieldWithLabel(
                hint: AppStrings.enterLastName,
                name: 'last_name',
                label: AppStrings.lastName,
                keyboardType: TextInputType.name,
                isOptional: false,
                onSaved: (String? value) {},
                validator: (String? value) {
                  return (value?.isEmpty ?? true)
                      ? 'Last Name is Required'
                      : null;
                },
              ),
              Dimensions.getCustomeHeight(1),
              TextFormFieldWithLabel(
                hint: AppStrings.enterEmail,
                name: 'email',
                label: AppStrings.email,
                keyboardType: TextInputType.emailAddress,
                isOptional: false,
                onSaved: (String? value) {},
                validator: (String? value) {
                  return (value?.isEmpty ?? true) ? 'Email is Required' : null;
                },
              ),
              Dimensions.getCustomeHeight(1),
              TextFormFieldWithLabel(
                hint: AppStrings.address,
                name: 'address',
                label: AppStrings.enterYourAddress,
                keyboardType: TextInputType.streetAddress,
                isOptional: true,
                onSaved: (String? value) {},
                validator: (String? value) {
                  return null;
                },
              ),
            ],
          ),
        ),
        Dimensions.y3,

        /// Create Profile Button
        CustomeButton(
          title: AppStrings.create,
          borderRadius: BorderRadius.circular(10.h),
          buttonWidth: Get.width,
          fontSize: 12.sp,
          titleColor: AppColors.yellow,
          onTap: () {
            // Get.toNamed(AppPages.home);
            controller.signUp(phoneNumber);
          },
        ),
      ],
    );
  }
}
