import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/modules/home/controllers/drawer_controller.dart';

class MenuIcon extends GetWidget<DrawerHandlerController> {
  const MenuIcon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (controller.isDrawerOpen) {
          controller.closeDrwer();
        } else {
          controller.openDrawer();
        }
      },
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.yellow,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
              color: AppColors.black.withOpacity(0.2),
              offset: const Offset(-3, 3),
              blurRadius: 15,
            )
          ],
        ),
        child: Padding(
          padding: EdgeInsets.all(1.8.h),
          child: SvgPicture.asset(
            AppImages.menu,
            height: 2.4.h,
            fit: BoxFit.scaleDown,
          ),
        ),
      ),
    );
  }
}
