import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/modules/home/controllers/location_controller.dart';

class MoveToCurrentLocationWidget extends GetWidget<LocationController> {
  const MoveToCurrentLocationWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 7.w,
      bottom: 15.h,
      child: InkWell(
        onTap: () => controller.animateCameraToCurrentLocation(),
        child: Container(
          height: 7.h,
          width: 7.h,
          alignment: Alignment.bottomRight,
          decoration: BoxDecoration(
            color: AppColors.white,
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(
                color: AppColors.black.withOpacity(0.2),
                blurRadius: 15,
                offset: const Offset(3, 5),
              ),
            ],
          ),
          child: Center(
            child: SvgPicture.asset(
              AppImages.moveToCurrentLocaitonIcon,
            ),
          ),
        ),
      ),
    );
  }
}
