import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';

class DashedLineVerticalPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    double dashHeight = 0.8.h, dashSpace = 1.h, startY = 0;
    final paint = Paint()
      ..color = AppColors.yellow
      ..strokeWidth = 0.8.w;
    while (startY < size.height) {
      canvas.drawLine(Offset(0, startY), Offset(0, startY + dashHeight), paint);
      startY += dashHeight + dashSpace;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
