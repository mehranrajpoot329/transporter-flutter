import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/local/db/local_storage.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/utils/utils.dart';
import 'package:transporter_customer/widgets/bottomsheet/app_bottom_sheet.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';
import 'package:transporter_customer/widgets/contractor_profile_order_widget.dart';

class UserFoundedSheet extends GetWidget<HomeController> {
  const UserFoundedSheet({Key? key}) : super(key: key);

  Future<bool> onBackPressed() async {
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => onBackPressed(),
      child: Obx(
        () => Align(
          alignment: Alignment.bottomCenter,
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: AppBottomSheet(
              height: 33.h,
              children: [
                Text(
                  'We have Found',
                  style: TextStyles.mediumStyle,
                ),
                Dimensions.y2,
                Container(
                  width: Get.width / 1.5,
                  margin: EdgeInsets.symmetric(horizontal: 6.w),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: ContractorProfileWidget(
                      phoneNumber:
                          '${controller.onGoingOrderModelData.orderStatus?.vendor?.phoneNo}',
                      fullName:
                          '${controller.onGoingOrderModelData.orderStatus?.vendor?.getFullName}',
                      imageURL:
                          '${controller.onGoingOrderModelData.orderStatus?.vendor?.getImageURL}'),
                ),
                Dimensions.y1,
                Text(
                  'Arrival Time : 30 min',
                  style: controller.isSearched.isFalse
                      ? TextStyles.regularStyle.copyWith(fontSize: 12.sp)
                      : TextStyles.mediumStyle.copyWith(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.bold,
                        ),
                ),
                Dimensions.y1,
                CustomeButton(
                  title: AppStrings.wantToChat,
                  borderRadius: BorderRadius.circular(10.h),
                  buttonWidth: Get.width / 1.4,
                  fontSize: 13.sp,
                  titleColor: AppColors.yellow,
                  onTap: () => Utils.open1to1Chat(
                    HiveHelper.getUser().firebaseKey,
                    controller
                        .onGoingOrderModelData.orderStatus!.vendor!.firebaseKey,
                    context,
                  ),
                ),
                Dimensions.y1,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
