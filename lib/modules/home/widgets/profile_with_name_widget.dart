import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/network/end_points.dart';
import 'package:transporter_customer/widgets/customer_cache_network_image.dart';

class ProfileWidget extends StatelessWidget {
  final String? imageUrl;
  final String? userName;

  const ProfileWidget({
    Key? key,
    required this.imageUrl,
    required this.userName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String finalImageURL = (imageUrl?.isEmpty ?? true)
        ? "http://via.placeholder.com/200x150"
        : EndPoints.baseURL + 'profileImage' + imageUrl!;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 4.w),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ClipOval(
            child: Container(
              decoration: const BoxDecoration(color: AppColors.black),
              height: 20.0.w,
              width: 20.0.w,
              child: CustomNetworkCacheImage(
                height: 20.0.w,
                width: 20.0.w,
                imageUrl: finalImageURL,
                errorWidgetColor: AppColors.white,
              ),
            ),
          ),
          Dimensions.x5,
          Center(
            child: Text(
              userName ?? '',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: AppFonts.sarabunSemiBold,
                fontSize: 14.sp,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                color: AppColors.textBlack,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
