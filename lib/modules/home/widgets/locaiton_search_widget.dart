import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';

class LocationSearchWidget extends StatelessWidget {
  final String hint;
  final Function(String value) onChange;
  final TextEditingController textController;
  final void Function()? onTap;
  final void Function()? onSufficClick;
  final bool isShowCrossIcon;
  const LocationSearchWidget({
    Key? key,
    required this.onChange,
    required this.hint,
    required this.textController,
    required this.onTap,
    required this.onSufficClick,
    required this.isShowCrossIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Padding(
        padding: EdgeInsets.only(right: 2.0.h, left: 1.5.h, bottom: 4.h),
        child: Container(
          width: Get.width / 1.4,
          decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: BorderRadius.circular(30),
            boxShadow: [
              BoxShadow(
                color: AppColors.black.withOpacity(0.2),
                blurRadius: 15,
                offset: const Offset(3, 4),
              )
            ],
          ),
          child: TextFormField(
            cursorColor: AppColors.black,
            textAlignVertical: TextAlignVertical.center,
            onTap: onTap,
            style: TextStyle(
              fontSize: 11.sp,
              fontFamily: AppFonts.sarabunRegular,
              overflow: TextOverflow.ellipsis,
              color: AppColors.textBlack,
            ),
            controller: textController,
            decoration: InputDecoration(
              hintText: hint,
              contentPadding: EdgeInsets.only(bottom: 0.2.h, right: 3.0.w),
              prefixIcon: SvgPicture.asset(
                AppImages.search,
                fit: BoxFit.scaleDown,
              ),
              suffixIcon: isShowCrossIcon
                  ? IconButton(
                      onPressed: onSufficClick,
                      icon: const Icon(Icons.close),
                      color: AppColors.black,
                    )
                  : const SizedBox.shrink(),
              hintStyle: TextStyle(
                fontSize: 11.sp,
                fontFamily: AppFonts.sarabunRegular,
                color: AppColors.textBlack.withOpacity(0.3),
              ),
              fillColor: AppColors.textBlack,
              border: InputBorder.none,
            ),
            onChanged: onChange,
          ),
        ),
      ),
    );
  }
}
