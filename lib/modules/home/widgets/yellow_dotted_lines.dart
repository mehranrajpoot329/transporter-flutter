import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/modules/home/widgets/dashed_line_vertical.dart';

class DottedLinesWidget extends GetWidget<HomeController> {
  const DottedLinesWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Obx(
        () => Visibility(
          visible: controller.showDottedLines.isTrue,
          child: Container(
            margin: EdgeInsets.only(top: 10.h, left: Get.width / 6),
            height: 8.h,
            width: Get.width / 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomPaint(
                  size: Size(1, 20.h),
                  painter: DashedLineVerticalPainter(),
                ),
                CustomPaint(
                  size: Size(1, 20.h),
                  painter: DashedLineVerticalPainter(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
