import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:transporter_customer/modules/home/controllers/drawer_controller.dart';
import 'package:transporter_customer/modules/home/views/home_screen.dart';
import 'package:transporter_customer/modules/home/views/menu_screen.dart';

class DrawerHandlerScreen extends GetView<DrawerHandlerController> {
  const DrawerHandlerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: const [
         MenuScreen(),
        HomeScreen(),
      ],
    );
  }
}
