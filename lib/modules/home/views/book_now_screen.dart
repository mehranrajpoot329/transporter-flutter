import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/global.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';
import 'package:transporter_customer/widgets/customer_appbar.dart';
import 'package:transporter_customer/widgets/customer_cache_network_image.dart';
import 'package:transporter_customer/widgets/drop_down.dart';

class BookNowDialogScreen extends GetView<HomeController> {
  const BookNowDialogScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    setSystemNavBarConfig(
      systemNavigationBarColor: AppColors.gray,
      statusBarColor: AppColors.black,
      androidStatusBarIconBrightness: Brightness.light,
      iosStatusBarBrightness: Brightness.light,
    );
    return Scaffold(
      backgroundColor: AppColors.gray,
      appBar: CustomeAppBar(
        iconUrl: AppImages.back,
        onBack: () => Get.back(),
        title: '',
        color: AppColors.gray,
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: 4.w,
              ),
              height: Get.height / 1.35,
              width: Get.width,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Dimensions.y2,

                      ///Details Text
                      Text(
                        AppStrings.bookNowWhichYouWant,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: AppFonts.sarabunSemiBold,
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w500,
                          color: AppColors.textBlack,
                        ),
                      ),
                      Dimensions.y3,

                      ///Truck ListView
                      SizedBox(
                        width: Get.width,
                        height: 20.h,
                        child: Obx(
                          () => ListView.builder(
                            padding: EdgeInsets.only(left: 9.w),
                            physics: const BouncingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            itemExtent: 33.w,
                            itemBuilder: (context, index) => GestureDetector(
                              onTap: () {
                                for (var item in controller.vehiclesList) {
                                  item.isSelected = false;
                                }
                                controller.vehiclesList[index].isSelected =
                                    !controller.vehiclesList[index].isSelected;
                                controller.truckRate.value =
                                    controller.vehiclesList[index].rate!;
                                controller.vehiclesList.refresh();
                              },
                              child: Padding(
                                padding: EdgeInsets.only(left: 2.0.w, top: 1.h),
                                child: Column(
                                  children: [
                                    Container(
                                      height: 13.h,
                                      width: 33.w,
                                      padding: EdgeInsets.all(4.h),
                                      margin: EdgeInsets.only(bottom: 1.3.h),
                                      decoration: BoxDecoration(
                                        color: controller
                                                .vehiclesList[index].isSelected
                                            ? AppColors.yellow
                                            : AppColors.white,
                                        borderRadius: BorderRadius.circular(19),
                                        boxShadow: const [
                                          BoxShadow(
                                            color: AppColors.gray,
                                            offset: Offset(5, 5),
                                            blurRadius: 15,
                                          ),
                                        ],
                                      ),
                                      child: Center(
                                        ///still needs to test
                                        child: CustomNetworkCacheImage(
                                          imageUrl: controller
                                                  .vehiclesList[index]
                                                  .imageUrl ??
                                              'http://via.placeholder.com/200x150',
                                          width: 5.w,
                                          height: 5.h,
                                        ),
                                      ),
                                    ),
                                    FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                        controller.vehiclesList[index]
                                                .vehicleName ??
                                            '',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: AppColors.textBlack,
                                          fontFamily: AppFonts.sarabunMedium,
                                          fontSize: 12.sp,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            itemCount: controller.vehiclesList.length,
                          ),
                        ),
                      ),
                      CustomDropDown(
                        hint: AppStrings.capacity,
                        items: const <String>[
                          '200 Kg - 300 Kg',
                          '300 Kg - 500 Kg',
                          '500 Kg - 700 Kg',
                          '700 kg - 900 kg'
                        ],
                        selectedValue: (value) {
                          controller.vehicleCapacity.value = value!;
                          log(value.toString());
                        },
                      ),
                      Dimensions.y4,
                      CustomDropDown(
                        hint: AppStrings.language,
                        items: const <String>[
                          'English',
                          'Chinese',
                          'Japanese',
                        ],
                        selectedValue: (value) {
                          controller.selectedLanguage.value = value!;
                          log(value.toString());
                        },
                      ),
                      Dimensions.y4,
                      CustomDropDown(
                        hint: AppStrings.paymentMethod,
                        items: const <String>[
                          'USD Cash',
                          'ZWL Cash',
                          'Eco Cash',
                        ],
                        selectedValue: (value) {
                          controller.paymentMethod.value = value!;
                          log(value.toString());
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Dimensions.y3,
            CustomeButton(
              title: AppStrings.nextbtnText,
              borderRadius: BorderRadius.circular(10.h),
              buttonWidth: Get.width / 2,
              fontSize: 12.sp,
              titleColor: AppColors.yellow,
              onTap: () {
                controller.checkBookNowDataAdded();
              },
            ),
          ],
        ),
      ),
    );
  }
}
