import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/global.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/modules/home/controllers/location_controller.dart';
import 'package:transporter_customer/modules/home/views/book_now_screen.dart';
import 'package:transporter_customer/utils/utils.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';
import 'package:transporter_customer/widgets/customer_appbar.dart';
import 'package:transporter_customer/widgets/order_dialog_listtile.dart';

class OrderSummaryDialogScreen extends GetView<HomeController> {
  OrderSummaryDialogScreen({Key? key}) : super(key: key);

  Future<bool> onBackPressed(BuildContext context) async {
    _onBackClick(context);
    return true;
  }

  _onBackClick(BuildContext context) {
    removeFullScreenDialog();
    openFullScreenDialog(
      const BookNowDialogScreen(),
    );
    controller.isDes.value = true;
    controller.showBackIcon.value = false;
    controller.showCurrentLocation.value = true;
    controller.showMenuIcon.value = true;
    controller.showDottedLines.value = true;
    controller.showDestinationField(true);
  }

  final LocationController _locationController = Get.find<LocationController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColors.white.withOpacity(0),
      appBar: CustomeAppBar(
        iconUrl: AppImages.back,
        onBack: () {
          _onBackClick(context);
          // Get.back();
          // Future.delayed(
          //   10.milliseconds,
          //   () => openFullScreenDialog(
          //     context,
          //     const BookNowDialogScreen(),
          //   ),
          // );
          // // Get.back();
          // controller.isDes.value = true;
          // controller.showBackIcon.value = false;
          // controller.showMenuIconAndCureentLocation.value = true;
          // controller.showDottedLines.value = true;
          // controller.showDestinationField(true);
        },
        title: '',
        color: AppColors.white.withOpacity(0),
      ),
      body: WillPopScope(
        onWillPop: () => onBackPressed(context),
        child: SizedBox(
          height: Get.height,
          width: Get.width,
          child: Column(
            children: [
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 5.w, vertical: 2.h),
                  height: Get.height / 1.35,
                  width: Get.width,
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.black.withOpacity(0.2),
                        blurRadius: 15,
                        offset: const Offset(3, 8),
                      ),
                    ],
                  ),
                  child: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        Dimensions.y3,
                        Text(
                          AppStrings.orderSummary,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: AppFonts.sarabunSemiBold,
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 0.5,
                            color: AppColors.textBlack,
                          ),
                        ),
                        Dimensions.y1,
                        OrderDialogListTile(
                          topMargin: Dimensions.getCustomeHeight(1.3),
                          title: AppStrings.yourLocaiton,
                          leftPadding: 6.5,
                          titleOpacity: 0.5,
                          subWidget: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              SvgPicture.asset(
                                AppImages.locationIcon,
                                fit: BoxFit.scaleDown,
                                height: 2.5.h,
                              ),
                              Dimensions.x5,
                              Flexible(
                                flex: 50,
                                child: Text(
                                  _locationController
                                      .currentLocationController.value.text,
                                  textAlign: TextAlign.start,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontFamily: AppFonts.sarabunSemiBold,
                                    fontSize: 11.sp,
                                    overflow: TextOverflow.ellipsis,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.textBlack,
                                  ),
                                ),
                              ),
                              const Spacer(),
                            ],
                          ),
                        ),
                        OrderDialogListTile(
                          topMargin: Dimensions.getCustomeHeight(1.3),
                          title: AppStrings.destinationLocation,
                          leftPadding: 6.5,
                          titleOpacity: 0.5,
                          subWidget: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              SvgPicture.asset(
                                AppImages.locationIcon,
                                fit: BoxFit.scaleDown,
                                height: 2.5.h,
                              ),
                              Dimensions.x5,
                              Flexible(
                                flex: 50,
                                child: Text(
                                  _locationController
                                      .desLocationController.value.text,
                                  textAlign: TextAlign.start,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontFamily: AppFonts.sarabunSemiBold,
                                    fontSize: 11.sp,
                                    overflow: TextOverflow.ellipsis,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.textBlack,
                                  ),
                                ),
                              ),
                              const Spacer(),
                            ],
                          ),
                        ),
                        OrderDialogListTile(
                          topMargin: Dimensions.getCustomeHeight(1.3),
                          title: AppStrings.selectedTruck,
                          leftPadding: 6.5,
                          titleOpacity: 0.5,
                          subWidget: Row(
                            children: [
                              Flexible(child: Dimensions.x9),
                              Flexible(
                                flex: 1,
                                child: Text(
                                  '${controller.getSelectedTruck()}',
                                  textAlign: TextAlign.start,
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontFamily: AppFonts.sarabunSemiBold,
                                    fontSize: 11.sp,
                                    overflow: TextOverflow.ellipsis,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.textBlack,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Obx(
                          () => OrderDialogListTile(
                            topMargin: Dimensions.getCustomeHeight(1.3),
                            title: AppStrings.capacity,
                            leftPadding: 6.5,
                            titleOpacity: 0.5,
                            subWidget: Row(
                              children: [
                                Flexible(child: Dimensions.x9),
                                Flexible(
                                  flex: 1,
                                  child: Text(
                                    controller.vehicleCapacity.value,
                                    textAlign: TextAlign.start,
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontFamily: AppFonts.sarabunSemiBold,
                                      fontSize: 11.sp,
                                      overflow: TextOverflow.ellipsis,
                                      fontWeight: FontWeight.bold,
                                      color: AppColors.textBlack,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        OrderDialogListTile(
                          topMargin: Dimensions.getCustomeHeight(1.3),
                          title: AppStrings.payedAmount,
                          leftPadding: 6.5,
                          titleOpacity: 0.5,
                          subWidget: Row(
                            children: [
                              Flexible(child: Dimensions.x9),
                              Flexible(
                                flex: 1,
                                child: Text(
                                  '\$${controller.truckRate.value}',
                                  textAlign: TextAlign.start,
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontFamily: AppFonts.sarabunSemiBold,
                                    fontSize: 11.sp,
                                    overflow: TextOverflow.ellipsis,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.textBlack,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        OrderDialogListTile(
                          topMargin: Dimensions.getCustomeHeight(1.3),
                          title: AppStrings.paymentMethod,
                          leftPadding: 6.5,
                          titleOpacity: 0.5,
                          subWidget: Row(
                            children: [
                              Flexible(child: Dimensions.x9),
                              Flexible(
                                  flex: 1,
                                  child: Obx(
                                    () => Text(
                                      '${controller.paymentMethod.value} \n54657845234576',
                                      textAlign: TextAlign.start,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontFamily: AppFonts.sarabunSemiBold,
                                        fontSize: 11.sp,
                                        overflow: TextOverflow.ellipsis,
                                        fontWeight: FontWeight.bold,
                                        color: AppColors.textBlack,
                                      ),
                                    ),
                                  )),
                            ],
                          ),
                        ),
                        OrderDialogListTile(
                          topMargin: Dimensions.getCustomeHeight(1.3),
                          title: AppStrings.anyNotes,
                          leftPadding: 6.5,
                          titleOpacity: 0.5,
                          subWidget: Row(
                            children: [
                              Expanded(
                                child: SizedBox(
                                  height: 16.5.h,
                                  width: Get.width,
                                  child: FormBuilderTextField(
                                    name: 'note',
                                    style: TextStyle(
                                      fontFamily: AppFonts.sarabunRegular,
                                      fontSize: 10.sp,
                                      color: AppColors.textBlack,
                                    ),
                                    minLines:
                                        6, // any number you need (It works as the rows for the textarea)
                                    keyboardType: TextInputType.multiline,
                                    textInputAction: TextInputAction.done,
                                    decoration: InputDecoration(
                                      isDense: true,
                                      focusColor:
                                          AppColors.gray.withOpacity(0.6),
                                      hintText: AppStrings.ifAny,
                                      hintStyle: TextStyle(
                                        fontFamily: AppFonts.sarabunRegular,
                                        fontSize: 10.sp,
                                        color: AppColors.textBlack,
                                      ),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(6),
                                      ),
                                      focusedBorder: const OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(7),
                                        ),
                                      ),
                                      enabledBorder: const OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(7),
                                        ),
                                      ),
                                    ),
                                    maxLines: null,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Dimensions.y3,
                        Padding(
                          padding: EdgeInsets.only(right: 10.w),
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: CustomeButton(
                              title: AppStrings.cancle,
                              borderRadius: BorderRadius.circular(10.h),
                              buttonWidth: Get.width / 2.4,
                              fontSize: 12.sp,
                              titleColor: AppColors.yellow,
                              onTap: () {
                                Get.back();
                                controller.isDes.value = true;
                                controller.showBackIcon.value = false;
                                controller.showCurrentLocation.value = true;
                                controller.showMenuIcon.value = true;
                                controller.showDottedLines.value = true;
                                controller.showDestinationField(true);
                              },
                            ),
                          ),
                        ),
                        Dimensions.y3,
                      ],
                    ),
                  ),
                ),
              ),

              ///space between
              const Spacer(),

              ///Confirm Button in Order Summary
              CustomeButton(
                title: AppStrings.confirm,
                borderRadius: BorderRadius.circular(10.h),
                buttonWidth: Get.width / 1.4,
                fontSize: 12.sp,
                titleColor: AppColors.yellow,
                onTap: () {
                  Utils.hideKeyboard(context);
                  controller.placeOrder();
                },
              ),
              Dimensions.y2,
            ],
          ),
        ),
      ),
    );
  }
}
