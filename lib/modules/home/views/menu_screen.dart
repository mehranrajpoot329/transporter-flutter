import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/local/db/local_storage.dart';
import 'package:transporter_customer/local/dummy/dummy.dart';
import 'package:transporter_customer/models/user_model.dart';
import 'package:transporter_customer/modules/home/controllers/drawer_controller.dart';
import 'package:transporter_customer/modules/home/widgets/app_icon_widget.dart';
import 'package:transporter_customer/modules/home/widgets/profile_with_name_widget.dart';

class MenuScreen extends GetView<DrawerHandlerController> {
  const MenuScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    User user = HiveHelper.getUser();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColors.yellow,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 3.0.w),
              child: Column(
                children: [
                  ///cross Icon for closing drwer
                  GestureDetector(
                    onTap: () => controller.closeDrwer(),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: EdgeInsets.only(right: 9.w, top: 3.h),
                        child: SvgPicture.asset(
                          AppImages.close,
                          height: 3.h,
                        ),
                      ),
                    ),
                  ),

                  ///profile Widget
                  GestureDetector(
                    onTap: () => controller.goTOProfileUpdate(),
                    child: ProfileWidget(
                      imageUrl: user.imageUrl,
                      userName: user.getUserName,
                    ),
                  ),
                  Dimensions.y4,

                  ///Drawer items
                  Column(
                    children: List.generate(
                      menuItems.length,
                      (index) => InkWell(
                        splashColor: AppColors.black,
                        highlightColor: AppColors.black,
                        onTap: () =>
                            controller.goToRoute(menuItems[index].routeToGo),
                        child: ListTile(
                          /// for removing defalut space horizontaily
                          visualDensity:
                              const VisualDensity(horizontal: 0, vertical: -4),

                          leading: SvgPicture.asset(
                            menuItems[index].iconUrl,
                            height: 6.2.w,
                          ),
                          title: Text(
                            menuItems[index].title,
                            style: TextStyle(
                              fontFamily: AppFonts.sarabunRegular,
                              fontSize: 9.sp,
                              color: AppColors.textBlack,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Dimensions.y9,
                  GestureDetector(
                    onTap: () => controller.logoutUser(),
                    child: ListTile(
                      leading: SvgPicture.asset(AppImages.signOut),
                      title: Text(
                        AppStrings.signOut,
                        style: TextStyle(
                          fontFamily: AppFonts.sarabunRegular,
                          fontSize: 11.sp,
                          color: AppColors.textBlack,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const AppIconWidget(),
          ],
        ),
      ),
    );
  }
}
