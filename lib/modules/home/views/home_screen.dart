import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/enums/order_status.dart';
import 'package:transporter_customer/modules/home/controllers/drawer_controller.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/modules/home/controllers/location_controller.dart';
import 'package:transporter_customer/modules/home/widgets/locaiton_search_widget.dart';
import 'package:transporter_customer/modules/home/widgets/menu_icon.dart';
import 'package:transporter_customer/modules/home/widgets/move_to_location.dart';
import 'package:transporter_customer/modules/home/widgets/order_detail_widget.dart';
import 'package:transporter_customer/modules/home/widgets/yellow_dotted_lines.dart';
import 'package:transporter_customer/widgets/bottomsheet/contractor_has_arrived_sheet.dart';
import 'package:transporter_customer/widgets/bottomsheet/searching_sheet.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  final LocationController locationController = Get.put(LocationController());

  final DrawerHandlerController drawerHandlerController =
      Get.isRegistered<DrawerHandlerController>()
          ? Get.find<DrawerHandlerController>()
          : Get.put(DrawerHandlerController());

  final HomeController homeController = Get.find<HomeController>();

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      locationController.mapController.setMapStyle("[]");
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DrawerHandlerController>(
      builder: (_) {
        return AnnotatedRegion<SystemUiOverlayStyle>(
          value: const SystemUiOverlayStyle(
              statusBarBrightness: Brightness.light,

              ///for ios
              statusBarColor: AppColors.black,
              statusBarIconBrightness: Brightness.light),
          child: AnimatedContainer(
            height: Get.height,
            width: Get.width,
            transform: Matrix4.translationValues(
                drawerHandlerController.xOffset,
                drawerHandlerController.yOffset,
                0)
              ..scale(drawerHandlerController.scaleFector),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: AppColors.black.withOpacity(0.4),
                  offset: const Offset(5, 5),
                  blurRadius: 15,
                )
              ],
              color: AppColors.white,
              borderRadius: BorderRadius.circular(
                  drawerHandlerController.drawerBorderRadius),
            ),
            duration: const Duration(milliseconds: 300),
            child: Material(
              borderRadius: BorderRadius.circular(
                  drawerHandlerController.drawerBorderRadius),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(
                  drawerHandlerController.drawerBorderRadius,
                ),
                child: Stack(
                  children: [
                    /// Google Map
                    GetBuilder<LocationController>(
                      id: 'googleMap',
                      builder: (_) {
                        return SizedBox(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          child: GoogleMap(
                            polylines: locationController.polyLines,
                            mapType: MapType.normal,
                            initialCameraPosition:
                                locationController.kGooglePlex,
                            markers: Set<Marker>.of(locationController.markers),
                            onMapCreated: (GoogleMapController controller) {
                              locationController.determinePosition();
                              if (!locationController
                                  .completerController.isCompleted) {
                                locationController.completerController
                                    .complete(controller);
                                locationController.mapController = controller;
                              }
                            },
                            compassEnabled: false,
                            trafficEnabled: true,
                            buildingsEnabled: true,
                            indoorViewEnabled: true,
                            myLocationEnabled: true,
                            myLocationButtonEnabled: false,
                            zoomGesturesEnabled: true,
                            zoomControlsEnabled: false,
                            scrollGesturesEnabled: true,
                          ),
                        );
                      },
                    ),
                    const DottedLinesWidget(),

                    ///Back Icon
                    Obx(
                      () => Positioned(
                        top: 7.h,
                        left: 3.w,
                        child: Visibility(
                          visible: homeController.showBackIcon.isTrue,
                          child: GestureDetector(
                            onTap: () {
                              homeController.showLocationSheet.value = false;
                            },
                            child: SvgPicture.asset(AppImages.back),
                          ),
                        ),
                      ),
                    ),

                    ///Menu Icon  And Current Location Search Field
                    Positioned(
                      top: 7.h,
                      left: 3.w,
                      child: Obx(
                        () => Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Visibility(
                              visible: homeController.showMenuIcon.isTrue,
                              child: const MenuIcon(),
                            ),
                            Visibility(
                              visible:
                                  homeController.showCurrentLocation.isTrue,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Obx(
                                    () => LocationSearchWidget(
                                      hint: 'Source Location',
                                      isShowCrossIcon: locationController
                                          .isCurrentLocationNotEmpty.value,
                                      onSufficClick: () {
                                        locationController
                                            .currentLocationController.value
                                            .clear();
                                      },
                                      textController: locationController
                                          .currentLocationController.value,
                                      onTap: () {
                                        locationController.searchPlaces(
                                            context, true);
                                      },
                                      onChange: (value) {
                                        log(value);
                                      },
                                    ),
                                  ),
                                  Obx(
                                    () => Visibility(
                                      visible: homeController.isDes.value,
                                      child: LocationSearchWidget(
                                        isShowCrossIcon: locationController
                                            .isDestinationLocationNotEmpty
                                            .value,
                                        hint: AppStrings.destinationLocation,
                                        onTap: () {
                                          locationController.searchPlaces(
                                            context,
                                            false,
                                          );
                                        },
                                        onSufficClick: () {
                                          locationController
                                              .desLocationController.value
                                              .clear();
                                        },
                                        textController: locationController
                                            .desLocationController.value,
                                        onChange: (value) {},
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                    ///next button and Confirm Button
                    Obx(
                      () => Positioned(
                        bottom: 3.h,
                        right: 0.w,
                        left: 0.w,
                        child: Column(
                          children: [
                            if (homeController.isDes.value)
                              CustomeButton(
                                title: AppStrings.confirm,
                                borderRadius: BorderRadius.circular(10.h),
                                buttonWidth: Get.width / 1.5,
                                fontSize: 10.sp,
                                btnColor: AppColors.black,
                                titleColor: AppColors.yellow,
                                onTap: () => homeController.confirmBtnClick(),
                              ),
                            if (homeController.showNextButton.isTrue)
                              CustomeButton(
                                title: AppStrings.nextbtnText,
                                borderRadius: BorderRadius.circular(10.h),
                                buttonWidth: Get.width / 1.5,
                                btnColor: AppColors.black,
                                fontSize: 10.sp,
                                titleColor: AppColors.yellow,
                                onTap: () => homeController.onNextBtnClick(),
                              ),
                          ],
                        ),
                      ),
                    ),
                    const MoveToCurrentLocationWidget(),

                    Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,

                      ///only 1 item will be visible at once
                      child: Column(
                        children: [
                          ///searching Vendor
                          GetBuilder<HomeController>(
                            builder: (_) {
                              return Visibility(
                                visible: homeController.mOrderStatus ==
                                    OrderStatus.searching,
                                child: const SearchingSheet(),
                              );
                            },
                          ),

                          /// Founded User
                          GetBuilder<HomeController>(
                            builder: (_) {
                              return Visibility(
                                visible: homeController.mOrderStatus ==
                                        OrderStatus.accepted ||
                                    homeController.mOrderStatus ==
                                        OrderStatus.onTheWay ||
                                    homeController.mOrderStatus ==
                                        OrderStatus.onGoing,
                                child: const UserFoundedSheet(),
                              );
                            },
                          ),

                          /// contractor has arrived state
                          GetBuilder<HomeController>(
                            builder: (_) {
                              return Visibility(
                                visible: homeController.mOrderStatus ==
                                    OrderStatus.arrived,
                                child: const ContractorHasArrivedSheet(),
                              );
                            },
                          ),

                          ///when the driver on his way
                          // GetBuilder<HomeController>(
                          //   builder: (_) {
                          //     return Visibility(
                          //       visible: homeController.mOrderStatus ==
                          //           OrderStatus.onTheWay,
                          //       child: const DriverOnWaySheet(),
                          //     );
                          //   },
                          // ),
                        ],
                      ),
                    ),

                    ///PaymentDetailDialog
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
