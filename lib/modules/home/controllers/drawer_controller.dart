import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/local/db/local_storage.dart';
import 'package:transporter_customer/routes/app_pages.dart';

class DrawerHandlerController extends GetxController {
  bool isDrawerOpen = false;
  double xOffset = 0;
  double scaleFector = 1;
  double yOffset = 0;
  double drawerBorderRadius = 0;

  void openDrawer() {
    xOffset = 230;
    yOffset = 150;
    scaleFector = 0.6;
    isDrawerOpen = true;
    drawerBorderRadius = 4.h;
    update();
  }

  void closeDrwer() {
    xOffset = 0;
    yOffset = 0;
    scaleFector = 1;
    isDrawerOpen = false;
    drawerBorderRadius = 0;
    update();
  }

  void goToRoute(String routeToGo) {
    if (isDrawerOpen) {
      closeDrwer();
      Get.toNamed(routeToGo);
    }
  }

  void logoutUser() {
    HiveHelper.saveUserLogout(isLoggedOut: true);
    HiveHelper.saveUserLogin(isLoggedIn: false);
    if (isDrawerOpen) {
      closeDrwer();
      Get.offAllNamed(AppPages.phoneLogin);
    }
  }

  void goTOProfileUpdate() {
    if (isDrawerOpen) {
      closeDrwer();
      Future.delayed(500.milliseconds, () {
        Get.toNamed(AppPages.profileUpdate);
      });
    }
  }
}
