import 'dart:async';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide FormData;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:transporter_customer/config/global.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/enums/order_status.dart';
import 'package:transporter_customer/graphql/queries.dart';
import 'package:transporter_customer/local/db/local_storage.dart';
import 'package:transporter_customer/models/api_models/location_update_model.dart';
import 'package:transporter_customer/models/api_models/place_service_order_model.dart';
import 'package:transporter_customer/models/api_models/vehicles_list_model.dart';
import 'package:transporter_customer/models/graphql_model/ongoing_order_model.dart';
import 'package:transporter_customer/modules/home/controllers/location_controller.dart';
import 'package:transporter_customer/modules/home/views/order_summary_screen.dart';
import 'package:transporter_customer/network/base_response.dart';
import 'package:transporter_customer/network/end_points.dart';
import 'package:transporter_customer/network/requests.dart';
import 'package:transporter_customer/utils/utils.dart';
import 'package:transporter_customer/widgets/bottomsheet/location_detial_botom_sheet.dart';
import 'package:transporter_customer/widgets/dialogs/payment_detail_dialog.dart';
import 'package:transporter_customer/widgets/dialogs/rating_dialog.dart';

class HomeController extends GetxController {
  late PlaceServiceOrderModelData placeServiceOrderModelData;
  RxString? reviewMessage = ''.obs;
  RxDouble? reviewStars = 0.0.obs;
  RxList<VehicleListModelData> vehiclesList = <VehicleListModelData>[].obs;
  RxString vehicleCapacity = ''.obs;
  RxString paymentMethod = ''.obs;
  RxString selectedLanguage = ''.obs;
  RxInt truckRate = 0.obs;
  OnGoingOrderModelData onGoingOrderModelData = OnGoingOrderModelData();
  final RxBool _isCoped = false.obs;
  late Stream<QueryResult> locationStream;
  late Stream<QueryResult> orderStream;
  StreamSubscription<QueryResult>? locationSubscription;
  StreamSubscription<QueryResult>? orderStatusSubscription;

  bool get isCopied => _isCoped.value;

  HomeController get getHomeController => Get.find<HomeController>();
  String mOrderStatus = OrderStatus.notStartedYet;

  /// handle Map UI Back Button
  /// searching fields
  /// Menu Icon
  RxBool isDes = false.obs;
  RxBool showLocationSheet = false.obs;
  RxBool isShowOrderDialog = false.obs;
  RxBool isConfirmOrderSummary = false.obs;
  RxBool isSearched = false.obs;
  RxBool showBackIcon = false.obs;
  RxBool showDottedLines = false.obs;
  RxBool showCurrentLocation = true.obs;
  RxBool showMenuIcon = true.obs;
  RxBool showNextButton = true.obs;

  ///Controller
  var locationController = Get.find<LocationController>();

  @override
  onInit() {
    Future.delayed(10.milliseconds, () {
      getAllVehicles();
      showOnGoingOrder();
    });
    super.onInit();
  }

  Future<void> getAllVehicles() async {
    BaseResponse response = await DioClient().getRequest(
      endPoint: EndPoints.vehices,
    );
    if (response.error == false) {
      for (var item in response.data) {
        vehiclesList.add(VehicleListModelData.fromJson(item));
      }
    }
  }

  void showOnGoingOrder() {
    OnGoingOrderModelData? onGoingOrder = HiveHelper.getGQLOnGoingOrder();
    if (onGoingOrder != null) {
      onGoingOrderModelData = onGoingOrder;
      placeServiceOrderModelData = HiveHelper.getPlacedOrder()!;
      mOrderStatus = onGoingOrderModelData.orderStatus!.orderStatus!;
      showCurrentLocation.value = false;
      update();
    }
  }

  Future<void> placeOrder() async {
    var locationController = Get.find<LocationController>();
    int? vId = _getSelectedVehicleId();
    if (vId == null) {
      showMessage(message: 'Pleas Select Vehicle');
      return;
    }
    var fC = _getValidCapacity();
    if (fC == null) {
      showMessage(message: 'Pleas Select Capacity');
      return;
    }
    var paymentM = _getValidPaymentMethod();
    if (paymentM == null) {
      showMessage(message: 'Pleas Select Payment Method');
      return;
    }

    Map<String, dynamic> body = {};
    body['source_lat'] = locationController.cLatLng.latitude;
    body['source_lon'] = locationController.cLatLng.longitude;
    body['destination_lat'] = locationController.dLatLng.latitude;
    body['destination_lon'] = locationController.dLatLng.longitude;
    body['payment_method'] = paymentM;
    body['vehicle_id'] = vId;
    body['capacity'] = fC;

    BaseResponse response = await DioClient().postRequest(
      endPoint: EndPoints.placeOrder,
      body: FormData.fromMap(body),
    );
    if (response.error == false) {
      removeFullScreenDialog();
      clearAllFilledData();
      locationController.clearPolyLines();
      placeServiceOrderModelData =
          PlaceServiceOrderModelData.fromJson(response.data);
      log('Order Id this one: ${placeServiceOrderModelData.id}');
      listenToOrderStatus();
      await HiveHelper.savePlacedOrder(placeServiceOrderModelData);
      hideAll();
      Future.delayed(
        200.milliseconds,
        () {
          mOrderStatus = OrderStatus.searching;
          showMenuIcon.value = true;
          showCurrentLocation.value = false;
          update();
        },
      );
      cancelOrderAfter40Seconds();
    }
  }

  Future<void> addReview(BuildContext context) async {
    BaseResponse response = await DioClient().postRequest(
      endPoint: EndPoints.customerAddReview,
      body: FormData.fromMap({
        'review': reviewMessage!.value,
        'order_id': placeServiceOrderModelData.id,
        'stars': reviewStars!.value,
      }),
    );
    if (response.error == false) {
      removeFullScreenDialog();
      mOrderStatus = OrderStatus.completed;
      Utils.hideKeyboard(context);
      HiveHelper.removePlacedOrder();
      HiveHelper.removeGQLOnGoingOrder();
      _isCoped.value = false;
      showMenuCurentNextWidgtes();
      Get.showSnackbar(
        Utils.successSnackBar(message: response.data ?? ''),
      );
      showMenuIcon.value = true;
      update();
    }
  }

  /// order id will be taken from [_placeServiceOrderModelDataData]
  Future<void> cancelOrder({String? messageToShow}) async {
    stopTimer();
    BaseResponse response = await DioClient().postRequest(
      endPoint: EndPoints.cancelServiceOrder,
      body: FormData.fromMap({
        'order_id': placeServiceOrderModelData.id,
        'reason': 'No Reason',
      }),
    );
    if (response.error == false) {
      log('Manually Cancel Order : ${response.message}');
      Future.delayed(2.seconds, () {
        Utils.dismissProgressBar();
        if (Get.isDialogOpen ?? false) {
          Get.back();
        }
        onOrderCancel(response, message: messageToShow);
      });
    }
  }

  ///complete order => order id will be taken from [_placeServiceOrderModelDataData]
  Future<void> endOrder(BuildContext context) async {
    BaseResponse response = await DioClient().postRequest(
      endPoint: EndPoints.endOrder,
      body: FormData.fromMap({
        "order_id": placeServiceOrderModelData.id,
      }),
    );
    if (response.error == false) {
      Utils.dismissProgressBar();
      _cancelStreams();
      isLocationListenStarted = false;
      var lc = Get.find<LocationController>();
      Future.delayed(500.milliseconds, () {
        removeFullScreenDialog();
        lc.clearPolyLines();
        openFullScreenDialog(
          const RatingDialog(),
        );
      });
    }
  }

  /// this can be called  before the order accepted when customer will be waiting
  /// for the vendor
  void onOrderCancel(BaseResponse? response, {String? message}) {
    Utils.dismissProgressBar();
    _cancelStreams();
    mOrderStatus = OrderStatus.cancelled;
    locationController.removeMarker(markerId: 'sourceLocation');
    locationController.removeMarker(markerId: 'destinationLocation');
    HiveHelper.removeGQLOnGoingOrder();
    HiveHelper.removePlacedOrder();
    isDes.value = true;
    showBackIcon.value = false;
    showCurrentLocation.value = true;
    showDottedLines.value = true;
    showDestinationField(true);
    update();
    if (message?.isNotEmpty ?? false) {
      Get.showSnackbar(
        Utils.successSnackBar(
          message: message!,
          snackPosition: SnackPosition.TOP,
        ),
      );
    }
  }

  ///GraphQL Helpers---->
  void listenToOrderStatus() {
    try {
      GraphQLClient client = GraphQLProvider.of(Get.context!).value;
      orderStream = client.subscribe(
        SubscriptionOptions(
          document: GQueries.orderStatusSubscription,
          operationName: GQueries.orderOperation,
        ),
      );
      orderStatusSubscription = orderStream.listen((result) {
        if (result.hasException) {
          log("GraphQL Exception : ${result.exception.toString()}");
          return;
        }
        if (result.isLoading) {
          log('Waiting for Result');
          return;
        }
        if (result.data?['ORDER_STATUS'] != null) {
          log('New Data : ${result.data?['ORDER_STATUS']}');
          onGoingOrderModelData = OnGoingOrderModelData.fromJson(result.data!);
          mOrderStatus = onGoingOrderModelData.orderStatus!.orderStatus!;
          handleListeningToOrderStatus(onGoingOrderModelData);
          update();
        }
      });
    } catch (e) {
      log('Exception in Connection : ${e.toString()}');
    }
  }

  /// this will listen to Location update by the vendor
  void listenToLocationUpdate() {
    try {
      GraphQLClient client = GraphQLProvider.of(Get.context!).value;
      locationStream = client.subscribe(
        SubscriptionOptions(
          document: GQueries.locationSubscription,
          operationName: GQueries.locationOperation,
        ),
      );
      locationSubscription = locationStream.listen((result) {
        if (result.hasException) {
          log("GraphQL Exception : ${result.exception.toString()}");
          return;
        }
        if (result.isLoading) {
          log('Waiting for Result');
          return;
        }
        if (result.data?['LOCATION_UPDATE'] != null) {
          log('New Location Data : ${result.data?['LOCATION_UPDATE']}');
          if (result.data != null) {
            var locationData = LocationUpdateModel.fromJson(result.data!);
            double lat =
                double.tryParse(locationData.data!.locationUpdate!.lat!)!;
            double long =
                double.tryParse(locationData.data!.locationUpdate!.lon!)!;
            locationController.moveMarkerOnMap(
                location: LatLng(lat, long), markerId: 'vendorLocationTrack');
          }
        }
      });
    } catch (e) {
      log('Exception in Connection : ${e.toString()}');
    }
  }

  void onOrderCompleted() {
    showMenuIcon.value = false;
    showNextButton.value = false;
    isDes.value = false;
    HiveHelper.removeGQLOnGoingOrder();
    HiveHelper.removePlacedOrder();
    _cancelStreams();
    locationController.removeMarker(markerId: 'vendorLocationTrack');
    update();
    Get.dialog(
      PaymentDetailDialog(),
      barrierDismissible: false,
      useSafeArea: false,
      barrierColor: AppColors.transparent,
    );
  }

  bool isLocationListenStarted = false;

  void handleListeningToOrderStatus(
      OnGoingOrderModelData onGoingOrderModelData) {
    switch (onGoingOrderModelData.orderStatus!.orderStatus) {
      case OrderStatus.cancelled:
        onOrderCancel(null, message: "Order Has Been Cancelled");
        break;
      case OrderStatus.completed:
        onOrderCompleted();
        break;
      case OrderStatus.accepted:
      case OrderStatus.onTheWay:
        var order = HiveHelper.getGQLOnGoingOrder();
        if (order == null) {
          HiveHelper.saveGQLOnGoingOrder(onGoingOrderModelData);
        }
        if (!isLocationListenStarted) {
          listenToLocationUpdate();
          isLocationListenStarted = true;
        }
        break;
      default:
        break;
    }
  }

  ///GraphQL Helper-------end------>

  ///Helper Functions------>
  void checkBookNowDataAdded() {
    String? vCapacity = _getValidCapacity();
    String? method = _getValidPaymentMethod();
    int? vID = _getSelectedVehicleId();
    bool isAdded = (vCapacity?.isNotEmpty ?? false) &&
        (method?.isNotEmpty ?? false) &&
        (vID != null ? true : false);
    if (isAdded) {
      removeFullScreenDialog();
      openFullScreenDialog(
        OrderSummaryDialogScreen(),
      );
      isDes.value = false;
      showBackIcon.value = false;
      showCurrentLocation.value = false;
      showMenuIcon.value = false;
      showDottedLines.value = false;
      showDestinationField(false);
    } else {
      showMessage(message: 'All Data is Required', showPassedMessage: true);
    }
  }

  void showDestinationField(bool isAddingDestination) {
    isDes.value = isAddingDestination;
  }

  void hideAll() {
    isDes.value = false;
    showLocationSheet.value = false;
    isShowOrderDialog.value = false;
    isConfirmOrderSummary.value = false;
    showCurrentLocation.value = false;
    showBackIcon.value = false;
    showLocationSheet.value = false;
  }

  showMenuCurentNextWidgtes() {
    Get.back();

    showCurrentLocation.value = true;
    showNextButton.value = true;
  }

  String? getSelectedTruck() {
    String? truck;
    for (var item in vehiclesList) {
      if (item.isSelected) {
        truck = item.vehicleName!;
        break;
      }
    }
    return truck;
  }

  void showMessage({required String message, bool showPassedMessage = false}) {
    Get.showSnackbar(Utils.successSnackBar(
        title: 'Alert',
        message: showPassedMessage ? message : 'Please Enter $message Location',
        snackPosition: SnackPosition.TOP));
  }

  bool isCurrentLocationFieldEmpty() {
    return locationController.currentLocationController.value.text
        .trim()
        .isEmpty;
  }

  bool isDesLocationFieldEmpty() {
    return locationController.desLocationController.value.text.trim().isEmpty;
  }

  void onNextBtnClick() {
    if (isCurrentLocationFieldEmpty()) {
      showMessage(message: 'Source');
      return;
    }
    showDestinationField(!isDes.value);
    showDottedLines.value = true;
    showNextButton.value = false;
  }

  void copyText(String data) {
    if (data.trim().isEmpty) return;
    Utils.copyDataToClipboard(data: data);
    _isCoped.value = true;
  }

  String? _getValidCapacity() {
    if (vehicleCapacity.trim().isEmpty) return null;
    return vehicleCapacity
        .replaceAll(RegExp('[A-Za-z, ]'), "")
        .replaceAll('-', '_');
  }

  String? _getValidPaymentMethod() {
    if (paymentMethod.value.trim().isEmpty) return null;
    return paymentMethod.value.replaceAll('Cash', '');
  }

  int? _getSelectedVehicleId() {
    int? id;
    for (var item in vehiclesList) {
      if (item.isSelected) {
        id = item.id;
      }
    }
    return id;
  }

  void confirmBtnClick() {
    if (isCurrentLocationFieldEmpty()) {
      showMessage(message: 'Source');
      return;
    }
    if (isDesLocationFieldEmpty()) {
      showMessage(message: 'Destination');
      return;
    }
    showLocationSheet.value = true;
    showBackIcon.value = true;
    showMenuIcon.value = false;
    showCurrentLocation.value = false;
    showDottedLines.value = false;
    Get.bottomSheet(
      LocationBottomSheet(),
      enableDrag: false,
      isDismissible: false,
    );
  }

  late Timer timer;
  RxInt start = 20.obs;
  final int _end = 0;

  void startTimer(void Function(bool isEnd) onEndTimer) {
    start.value = 20;
    const oneSec = Duration(seconds: 1);
    timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (start.value == _end) {
          stopTimer();
          onEndTimer(true);
        } else {
          start.value--;
          log(start.value.toString());
        }
      },
    );
  }

  void stopTimer() {
    if (timer.isActive) {
      timer.cancel();
    }
  }

  void clearAllFilledData() {
    for (var item in vehiclesList) {
      item.isSelected = false;
    }
    paymentMethod.value = '';
    selectedLanguage.value = '';
    vehicleCapacity.value = '';
  }

  ///Helper Functions------end------>
  void _cancelStreams() {
    orderStatusSubscription?.cancel();
    locationSubscription?.cancel();
  }

  Future<void> cancelOrderAfter40Seconds() async {
    await Future.delayed(const Duration(seconds: 20), () {
      cancelOrder(messageToShow: "No Vendor Found Try After Few Minutes");
    });
  }
}
