import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart' as pl;
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:transporter_customer/config/map_icon_controller.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/utils/utils.dart';
import 'package:uuid/uuid.dart';

class LocationController extends GetxController {
  late GoogleMapController mapController;
  late Rx<TextEditingController> currentLocationController;
  late Rx<TextEditingController> desLocationController;
  late pl.PolylinePoints polylinePoints = pl.PolylinePoints();
  Set<Polyline> polyLines = <Polyline>{};
  List<LatLng> polylineCoordinates = [];
  RxBool isCurrentLocationNotEmpty = false.obs;
  RxBool isDestinationLocationNotEmpty = false.obs;

  final Completer<GoogleMapController> completerController = Completer();
  List<Marker> markers = [];
  String? currentAddress;

  ///CurrentSearched LatLng
  late LatLng cLatLng;

  ///DestinationSearched LatLng
  late LatLng dLatLng;

  ///Google map api key for maps places
  final GoogleMapsPlaces places =
      GoogleMapsPlaces(apiKey: AppStrings.googleKey);

  ///finding this controller for getting Icon
  late MapIconController mapIconController;

  @override
  onInit() {
    super.onInit();
    currentLocationController = TextEditingController().obs;
    desLocationController = TextEditingController().obs;
    mapIconController = Get.find<MapIconController>();
    currentLocationController.value.addListener(_currentLocationListener);
    desLocationController.value.addListener(_destinationLocationListener);
  }

  @override
  void dispose() {
    mapController.dispose();
    currentLocationController.value.dispose();
    desLocationController.value.dispose();
    super.dispose();
  }

  ///initial Position
  final CameraPosition kGooglePlex = const CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  ///Camera Animate
  Future<void> _animateCameraToCustomLocation(double lat, double lng) async {
    mapController = await completerController.future;
    await mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(lat, lng),
          zoom: 18.7,
        ),
      ),
    );
  }

  Future<Position> getCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    return position;
  }

  ///current Location
  Future<void> determinePosition() async {
    LocationPermission permission;
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await requestLocationPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }
    }
    Position position = await getCurrentLocation();
    currentAddress = await _getAddressFromLocation(position: position);
    cLatLng = LatLng(position.latitude, position.longitude);
    currentLocationController.value.text = currentAddress ?? '';
    markers.add(
      createMarker(
        position.latitude,
        position.longitude,
        markerId: 'currentLocation',
      ),
    );

    update(['googleMap', 'currentLocationHint']);

    _animateCameraToCustomLocation(position.latitude, position.longitude);
  }

  Future<LocationPermission> requestLocationPermission() async {
    return await Geolocator.requestPermission();
  }

  Marker createMarker(double lat, double lng, {String? markerId}) {
    Marker marker = Marker(
      draggable: false,
      markerId: MarkerId(markerId ?? LatLng(lat, lng).toString()),
      position: LatLng(lat, lng),
      icon: mapIconController.locationIcon,
      infoWindow: InfoWindow(title: '$lat $lng}'),
    );
    return marker;
  }

  removeMarker({required String markerId}) {
    Marker? marker = markers.firstWhereOrNull(
      (marker) => marker.markerId.value == markerId,
    );
    if (marker != null) {
      markers.remove(marker);
    }
    update(['googleMap']);
  }

  Future<String> _getAddressFromLocation({required Position position}) async {
    List<Placemark> placemarks =
        await placemarkFromCoordinates(position.latitude, position.longitude);
    Placemark place = placemarks[0];
    String address =
        '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    return address;
  }

  /// for Places search
  Future<void> searchPlaces(BuildContext context, bool isCurrent) async {
    Prediction? p = await PlacesAutocomplete.show(
      context: context,
      apiKey: AppStrings.googleKey,
      onError: _onError,
      types: [],
      strictbounds: false,
      sessionToken: const Uuid().v4(),
      mode: Mode.overlay,
      language: 'en',
      decoration: InputDecoration(
        hintText: 'Location',
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: const BorderSide(
            color: Colors.white,
          ),
        ),
      ),
      components: [],
    );
    if (p != null) {
      await _displayPrediction(p, context, isCurrent);
    }
  }

  ///show places predictions
  Future<void> _displayPrediction(
      Prediction p, BuildContext context, bool isCL) async {
    FocusScope.of(context).requestFocus(FocusNode());
    PlacesDetailsResponse detail = await places.getDetailsByPlaceId(p.placeId!);
    var lat = detail.result.geometry?.location.lat;
    var lng = detail.result.geometry?.location.lng;
    if (isCL) {
      currentLocationController.value.clear();
      currentLocationController.value.text = detail.result.formattedAddress!;
      cLatLng = LatLng(lat!, lng!);
      _animateCameraToCustomLocation(cLatLng.latitude, cLatLng.longitude);
    } else {
      desLocationController.value.text = detail.result.formattedAddress!;
      dLatLng = LatLng(lat!, lng!);
      await setPolyLines(sourceLatLng: cLatLng, destinationLatLng: dLatLng);
    }
    markers.add(
      createMarker(lat, lng,
          markerId: isCL ? 'sourceLocation' : 'destinationLocation'),
    );
    update(['googleMap']);
  }

  ///Camera Animate
  void animateCameraToCurrentLocation() async {
    Position position = await getCurrentLocation();
    await mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(position.latitude, position.longitude),
          zoom: 18,
        ),
      ),
    );
  }

  ///Error in searching places
  void _onError(PlacesAutocompleteResponse value) {
    Get.showSnackbar(
        Utils.errorSnackBar(title: 'Error', message: value.errorMessage!));
  }

  Future<void> setPolyLines({
    required LatLng sourceLatLng,
    required LatLng destinationLatLng,
  }) async {
    addPolyLines(sourceLatLng, destinationLatLng);
    animateCameraToMakeVisibleBothMarkers(
      startLatitude: sourceLatLng.latitude,
      startLongitude: sourceLatLng.longitude,
      destinationLatitude: destinationLatLng.latitude,
      destinationLongitude: destinationLatLng.longitude,
    );
    update(['googleMap']);
  }

  Future<void> addPolyLines(LatLng sourceLocation, LatLng desLocation) async {
    removeMarker(markerId: 'currentLocation');
    pl.PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      AppStrings.googleKey,
      pl.PointLatLng(sourceLocation.latitude, sourceLocation.longitude),
      pl.PointLatLng(desLocation.latitude, desLocation.longitude),
      travelMode: pl.TravelMode.driving,
    );
    if (polylineCoordinates.isNotEmpty) {
      polylineCoordinates.clear();
    }
    for (var point in result.points) {
      ///adding points to coordinates
      polylineCoordinates.add(LatLng(point.latitude, point.longitude));
    }

    ///adding to polyLines
    polyLines.add(Polyline(
      width: 5,
      visible: true,
      polylineId: const PolylineId('polyLine'),
      color: AppColors.textBlack,
      points: polylineCoordinates,
    ));
    update(['googleMap']);
  }

  Future<void> clearPolyLines() async {
    polyLines.clear();
    polylineCoordinates.clear();
    polylinePoints = pl.PolylinePoints();
    // currentLocationController.value.clear();
    // desLocationController.value.clear();
    removeMarker(markerId: 'sourceLocation');
    removeMarker(markerId: 'destinationLocation');
    update(['googleMap']);
  }

  void animateCameraToMakeVisibleBothMarkers(
      {required double startLatitude,
      required double startLongitude,
      required double destinationLatitude,
      required double destinationLongitude}) {
    double miny = (startLatitude <= destinationLatitude)
        ? startLatitude
        : destinationLatitude;
    double minx = (startLongitude <= destinationLongitude)
        ? startLongitude
        : destinationLongitude;
    double maxy = (startLatitude <= destinationLatitude)
        ? destinationLatitude
        : startLatitude;
    double maxx = (startLongitude <= destinationLongitude)
        ? destinationLongitude
        : startLongitude;
    double southWestLatitude = miny;
    double southWestLongitude = minx;
    double northEastLatitude = maxy;
    double northEastLongitude = maxx;
    mapController.animateCamera(
      CameraUpdate.newLatLngBounds(
        LatLngBounds(
          northeast: LatLng(northEastLatitude, northEastLongitude),
          southwest: LatLng(southWestLatitude, southWestLongitude),
        ),
        100.0,
      ),
    );
  }

  Future<void> moveMarkerOnMap(
      {required LatLng location, required String markerId}) async {
    _animateCameraToCustomLocation(location.latitude, location.longitude);
    removeMarker(markerId: markerId);
    markers.add(createMarker(location.latitude, location.longitude,
        markerId: markerId));
    update(['googleMap']);
  }

  void _currentLocationListener() {
    if (currentLocationController.value.text.trim().isNotEmpty) {
      isCurrentLocationNotEmpty.value = true;
    } else {
      isCurrentLocationNotEmpty.value = false;
    }
  }

  void _destinationLocationListener() {
    if (desLocationController.value.text.trim().isNotEmpty) {
      isDestinationLocationNotEmpty.value = true;
    } else {
      isDestinationLocationNotEmpty.value = false;
    }
  }
}
