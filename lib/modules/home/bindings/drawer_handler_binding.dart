import 'package:get/get.dart';
import 'package:transporter_customer/modules/home/controllers/drawer_controller.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/modules/home/controllers/location_controller.dart';

class DrawerHandlerBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(DrawerHandlerController());
  }
}
