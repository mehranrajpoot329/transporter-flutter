import 'package:get/instance_manager.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/modules/home/controllers/location_controller.dart';

class LocationBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(LocationController());
  }
}
