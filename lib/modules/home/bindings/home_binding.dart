import 'package:get/instance_manager.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(HomeController());
  }
}
