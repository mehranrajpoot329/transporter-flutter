import 'dart:async';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide FormData;
import 'package:messenger/config/Glob.dart';
import 'package:messenger/controllers/user_controller.dart';
import 'package:messenger/model/Availability.dart';
import 'package:messenger/model/UserWrapper.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:transporter_customer/config/firebase_api.dart';
import 'package:transporter_customer/local/db/local_storage.dart';
import 'package:transporter_customer/models/user_model.dart' as customer;
import 'package:transporter_customer/network/base_response.dart';
import 'package:transporter_customer/network/end_points.dart';
import 'package:transporter_customer/network/requests.dart';
import 'package:transporter_customer/routes/app_pages.dart';
import 'package:transporter_customer/utils/utils.dart';

import '../../constants/app_strings.dart';

class OTPController extends GetxController {
  RxBool isCompleted = false.obs;
  late StreamController<ErrorAnimationType> errorController;
  late TextEditingController otpController;
  final formKey = GlobalKey<FormState>();
  late Timer _timer;
  RxInt start = 40.obs;
  get isDisableOTP => FirebaseApi.isDisableResendButton.value;

  @override
  void onInit() {
    errorController = StreamController<ErrorAnimationType>();
    otpController = TextEditingController();
    super.onInit();
  }

  @override
  void dispose() {
    errorController.close();
    otpController.dispose();
    stopTimer();
    super.dispose();
  }

  Future<void> validateOTP(
      {required String phoneNumber, required String verificationId}) async {
    formKey.currentState?.validate();
    if (otpController.text.length < 6 || otpController.text.trim().isEmpty) {
      errorController.add(ErrorAnimationType.shake);
    } else {
      await signInWithPhoneNumber(
          phoneNumber: phoneNumber, vID: verificationId);
    }
  }

  ///when user will add otp code receive on phone Number
  Future<void> signInWithPhoneNumber({
    required String phoneNumber,
    required String vID,
  }) async {
    Utils.showProgressBar();
    try {
      var data = [
        {'phone': phoneNumber},
      ];

      /// 1-> Verify OTP
      /// 2-> hit Login if user is not available then
      /// then response will container error take hom to profile page
      /// else take him to home screen
      User? user =
          await FirebaseApi.mVerifyPhoneNumber(smsCode: otpController.text);
      if (user?.uid != null) {
        var loginResponse = await login(phoneNumber: phoneNumber);
        if (loginResponse.error == false) {
          customer.User? apiUser = customer.User.fromJson(loginResponse.data);
          await HiveHelper.saveLoggIn(isLoggedIn: true);
          await HiveHelper.saveIsNew(isNew: false);
          await HiveHelper.setAuthToken(apiUser.token!);
          await HiveHelper.saveUser(apiUser);
          Utils.dismissProgressBar();
          await authenticateUserWithMessenger(apiUser, user!);
          Get.offAllNamed(
            AppPages.home,
          );
        } else if (loginResponse.error == true) {
          Utils.dismissProgressBar();
          Future.delayed(1.seconds, () {
            Get.toNamed(AppPages.profile, arguments: data);
          });
        } else {
          Utils.dismissProgressBar();
        }
      }
    } on FirebaseAuthException catch (e) {
      showFirebaseMessage(e);
    }
  }

  void resendOTP({
    required String phoneNumber,
    required int token,
    required bool isNavigation,
  }) {
    FirebaseApi.resendOTP(pNumber: phoneNumber, isNavReq: isNavigation);
    restartTimer();
  }

  Future<BaseResponse> login({required String phoneNumber}) async {
    try {
      BaseResponse response = await DioClient().postRequest(
        endPoint: EndPoints.customerLogin,
        body: FormData.fromMap({'phone_no': phoneNumber}),
      );
      return response;
    } catch (e) {
      Utils.dismissProgressBar();
      return BaseResponse(
        message: 'Error',
        error: true,
      );
    }
  }

  /// Firebase Chat auth Setup
  Future<void> authenticateUserWithMessenger(
      customer.User user, User fbCratedUser) async {
    if (fbCratedUser.uid.isNotEmpty) {
      updateUserInfoOnFirebase(user);
    } else {
      Get.showSnackbar(
          Utils.errorSnackBar(message: 'Cannot authenticate with messenger'));
    }
  }

  Future<void> updateUserInfoOnFirebase(customer.User user) async {
    final userWrapper = UserWrapper.toServerObject(
      name: user.getFullName(),
      phone: user.phoneNo,
      userType: user.userType,
      pictureURL: user.imageUrl,
      email: user.email,
      availability: Availability.Available,
    );
    userWrapper.entityId = user.firebaseKey;
    userWrapper.fcm = HiveHelper.getFcmToken();
    Glob().currentUserKey = user.firebaseKey;

    ///this will create user on firebase
    await UserController.authenticateUser(userWrapper);
    await UserController.setUserOnline(HiveHelper.getUser().firebaseKey);
  }

  void showFirebaseMessage(FirebaseAuthException e) {
    Utils.dismissProgressBar();
    if (e.toString().contains(AppStrings.firebaseAuthE)) {
      Get.showSnackbar(Utils.errorSnackBar(message: 'Invalid Code'));
    } else if (e.toString().contains(AppStrings.firebaseExp)) {
      Get.showSnackbar(Utils.errorSnackBar(
          message:
              'The sms code has expired. Please re-send the verification code to try again.'));
    }
    log(e.toString());
  }

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (start.value == 0) {
          timer.cancel();
        } else {
          start.value--;
        }
      },
    );
  }

  void restartTimer() {
    _timer.cancel();
    startTimer();
  }

  stopTimer() {
    _timer.cancel();
  }
}
