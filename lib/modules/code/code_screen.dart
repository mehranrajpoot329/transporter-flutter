import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/modules/code/otp_code_controller.dart';
import 'package:transporter_customer/modules/code/otp_code_field.dart';
import 'package:transporter_customer/modules/login/parent_screen.dart';
import 'package:transporter_customer/widgets/buttons/custome_button.dart';

import 'otp_code_field.dart';

// ignore: must_be_immutable
class CodeScreen extends GetView<OTPController> {
  CodeScreen({Key? key}) : super(key: key);
  String phoneNumber = Get.arguments[0]['phone'].toString();
  String vId = Get.arguments[1]['verificationId'].toString();
  int fToken = Get.arguments[2]['fToken'] as int;

  @override
  Widget build(BuildContext context) {
    controller.startTimer();
    return ParentScreen(
      padding: EdgeInsets.only(top: 3.h, left: 4.w, right: 4.w, bottom: 0.h),
      resizeToAvoidBottomInset: true,
      topWidget: Positioned(
        top: 18.h,
        left: 0,
        right: 0,
        child: Container(
          height: 10.h,
          alignment: Alignment.topCenter,
          child: Image.asset(
            AppImages.appIcon,
            fit: BoxFit.cover,
          ),
        ),
      ),
      children: [
        Text(
          AppStrings.otp,
          style: TextStyles.boldStyle.copyWith(fontSize: 14.sp),
        ),
        Dimensions.y2,
        Text(
          AppStrings.pleaseEnterVerificationCodeWeJustSendToYourPhoneNumber,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontFamily: AppFonts.sarabunMedium,
              fontSize: 10.sp,
              color: AppColors.textBlack.withOpacity(0.5)),
        ),
        Dimensions.y6,
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            AppStrings.verificationCode,
            textAlign: TextAlign.left,
            style: TextStyles.boldStyle.copyWith(fontSize: 10.sp),
          ),
        ),
        Dimensions.y1,
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 2.w),
          child: OTPCodeField(
            formKey: controller.formKey,
            errorController: controller.errorController,
            onSubmit: (value) {},
            otpTextController: controller.otpController,
          ),
        ),
        Dimensions.y3,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SvgPicture.asset(AppImages.chat),
                Dimensions.x3,
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Dimensions.getCustomeHeight(0.5),
                    Obx(
                      () => InkWell(
                        splashColor: AppColors.gray,
                        highlightColor: AppColors.gray,
                        onTap: () {
                          if (controller.isDisableOTP) {
                            return;
                          } else {
                            controller.resendOTP(
                              phoneNumber: phoneNumber,
                              token: fToken,
                              isNavigation: false,
                            );
                          }
                        },
                        child: Text(
                          AppStrings.iDidNotReceiveCode,
                          style: TextStyles.boldStyle.copyWith(
                            fontSize: 9.sp,
                            color: controller.isDisableOTP
                                ? AppColors.gray
                                : AppColors.textBlack,
                          ),
                        ),
                      ),
                    ),
                    Dimensions.y1,
                    InkWell(
                      splashColor: AppColors.gray,
                      highlightColor: AppColors.gray,
                      onTap: () => Get.back(),
                      child: Text(
                        AppStrings.editPhoneNumber,
                        style: TextStyles.boldStyle.copyWith(fontSize: 9.sp),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            GestureDetector(
              onTap: () {
                controller.otpController.clear();
              },
              child: Padding(
                padding: EdgeInsets.only(right: 2.w, bottom: 2.h),
                child: Text(
                  AppStrings.clear,
                  // textAlign: TextAlign.left,
                  style: TextStyles.boldStyle.copyWith(
                    fontSize: 14.sp,
                  ),
                ),
              ),
            ),
          ],
        ),
        Dimensions.y1,
        Obx(
          () => Text(
            '${controller.start.value.toStringAsFixed(0)}s',
            style: TextStyles.boldStyle.copyWith(
              fontSize: 14.sp,
            ),
          ),
        ),
        Dimensions.y4,
        CustomeButton(
          title: AppStrings.verify,
          borderRadius: BorderRadius.circular(10.h),
          buttonWidth: Get.width,
          fontSize: 12.sp,
          titleColor: AppColors.yellow,
          onTap: () {
            controller.validateOTP(
                phoneNumber: phoneNumber, verificationId: vId);
          },
        ),
        Dimensions.y3,
      ],
    );
  }
}
