import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:transporter_customer/config/text_styles.dart';
import 'package:transporter_customer/constants/app_colors.dart';

class OTPCodeField extends StatelessWidget {
  final TextEditingController otpTextController;
  final void Function(String)? onSubmit;
  final StreamController<ErrorAnimationType>? errorController;
  final GlobalKey<FormState> formKey;

  const OTPCodeField({
    Key? key,
    required this.otpTextController,
    required this.onSubmit,
    required this.errorController,
    required this.formKey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: PinCodeTextField(
        appContext: context,
        length: 6,
        onAutoFillDisposeAction: AutofillContextAction.commit,
        obscureText: false,
        animationType: AnimationType.fade,
        pinTheme: PinTheme(
            shape: PinCodeFieldShape.underline,
            borderRadius: BorderRadius.circular(5),
            fieldHeight: 50,
            fieldWidth: 40,
            activeColor: AppColors.black,
            selectedColor: AppColors.black,
            inactiveColor: AppColors.black
            // activeFillColor: Colors.white,
            ),
        animationDuration: const Duration(milliseconds: 300),
        backgroundColor: AppColors.white,
        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
        controller: otpTextController,
        errorAnimationController: errorController,
        keyboardType: TextInputType.number,
        cursorColor: Colors.black,
        textStyle: TextStyles.mediumStyle,
        useExternalAutoFillGroup: true,
        onCompleted: onSubmit,
        autoFocus: true,
        autoDismissKeyboard: true,
        
        onChanged: (value) {
          // print(value);
        },
        beforeTextPaste: (text) {
          //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
          //but you can show anything you want here, like your pop up saying wrong paste format or etc
          return true;
        },
      ),
    );
  }
}
