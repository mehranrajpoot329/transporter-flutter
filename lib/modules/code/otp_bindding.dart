import 'package:get/instance_manager.dart';
import 'package:transporter_customer/modules/code/otp_code_controller.dart';

class OTPBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(OTPController());
  }
}
