import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:transporter_customer/config/app_dimensions.dart';
import 'package:transporter_customer/constants/app_colors.dart';
import 'package:transporter_customer/constants/app_fonts.dart';
import 'package:transporter_customer/constants/app_images.dart';
import 'package:transporter_customer/constants/app_strings.dart';
import 'package:transporter_customer/models/api_models/orders_model.dart';
import 'package:transporter_customer/modules/orders/orders_controller.dart';
import 'package:transporter_customer/widgets/customer_appbar.dart';
import 'package:transporter_customer/widgets/dialogs/order_summary_details_dialog.dart';

class OrdersScreen extends GetView<MyOrdersController> {
  const OrdersScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomeAppBar(
        iconUrl: AppImages.back,
        title: AppStrings.myOrders,
        onBack: () => Get.back(),
      ),
      body: GetBuilder<MyOrdersController>(
        builder: (_) {
          return (controller.ordersList.isEmpty)
              ? const Center(
                  child: Text(
                    'No Orders',
                  ),
                )
              : ListView.builder(
                  itemCount: controller.ordersList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        Get.dialog(
                            OrderSummaryDialog(
                                orderId: controller.ordersList[index].id!),
                            transitionCurve: Curves.ease,
                            transitionDuration: 500.milliseconds);
                      },
                      child: OrderSingleItemWidget(
                        data: controller.ordersList[index],
                      ),
                    );
                  },
                );
        },
      ),
    );
  }
}

class OrderSingleItemWidget extends GetWidget<MyOrdersController> {
  final OrdersModelData data;
  const OrderSingleItemWidget({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      decoration: BoxDecoration(
        color: AppColors.gray.withOpacity(0.8),
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.symmetric(horizontal: 5.w, vertical: 1.h),
      padding: EdgeInsets.only(bottom: 2.h),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          OrderListTileWidget(
            title: '${data.status}',
            subtitle: '${data.completedAt}',
            decoration: const BoxDecoration(
              color: AppColors.yellow,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              ),
            ),
            titleStyle: TextStyle(
              color: AppColors.textBlack,
              fontFamily: AppFonts.sarabunMedium,
              fontSize: 10.sp,
            ),
            subTitleStyle: TextStyle(
              color: AppColors.textBlack,
              fontFamily: AppFonts.sarabunMedium,
              fontSize: 10.sp,
            ),
          ),
          OrderListTileWidget(
            title: 'Vehicle:',
            subtitle: '${data.vehicle}',
            decoration: const BoxDecoration(
              color: AppColors.transparent,
            ),
            titleStyle: TextStyle(
              color: AppColors.textBlack,
              fontFamily: AppFonts.sarabunSemiBold,
              fontWeight: FontWeight.w500,
              fontSize: 11.sp,
            ),
            subTitleStyle: TextStyle(
              color: AppColors.textBlack,
              fontFamily: AppFonts.sarabunRegular,
              fontSize: 10.sp,
            ),
          ),
          Dimensions.y2,
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20.w,
            ),
            child: Divider(
              color: AppColors.black.withOpacity(0.5),
            ),
          ),
          OrderListTileWidget(
            title: 'Total:',
            subtitle: '${data.totalPrice ?? 0}',
            decoration: const BoxDecoration(
              color: AppColors.transparent,
            ),
            titleStyle: TextStyle(
              color: AppColors.textBlack,
              fontFamily: AppFonts.sarabunSemiBold,
              fontWeight: FontWeight.w500,
              fontSize: 11.sp,
            ),
            subTitleStyle: TextStyle(
              color: AppColors.textBlack,
              fontFamily: AppFonts.sarabunMedium,
              fontWeight: FontWeight.w900,
              fontSize: 11.sp,
            ),
          ),
        ],
      ),
    );
  }
}

class OrderListTileWidget extends StatelessWidget {
  final String title;
  final String subtitle;
  final TextStyle subTitleStyle;
  final TextStyle titleStyle;
  final BoxDecoration decoration;
  const OrderListTileWidget(
      {Key? key,
      required this.title,
      required this.subtitle,
      required this.titleStyle,
      required this.subTitleStyle,
      required this.decoration})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 1.h),
      decoration: decoration,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 7.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            Text(title, textAlign: TextAlign.start, style: titleStyle),
            Text(subtitle, style: subTitleStyle),
          ],
        ),
      ),
    );
  }
}
