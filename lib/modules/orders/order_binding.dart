import 'package:get/get.dart';
import 'package:transporter_customer/modules/orders/orders_controller.dart';

class MyOrdersBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(MyOrdersController());
  }
}
