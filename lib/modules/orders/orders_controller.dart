import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:transporter_customer/models/api_models/orders_model.dart';
import 'package:transporter_customer/modules/home/controllers/home_controller.dart';
import 'package:transporter_customer/network/base_response.dart';
import 'package:transporter_customer/network/end_points.dart';
import 'package:transporter_customer/network/requests.dart';
import 'package:transporter_customer/utils/utils.dart';

class MyOrdersController extends GetxController {
  List<OrdersModelData> ordersList = <OrdersModelData>[];
  late OrdersModelData orderData;
  bool isLoading = false;
  @override
  onInit() {
    super.onInit();
    Future.delayed(500.milliseconds, () {
      getAllMyOrders();
    });
  }

  String? getVehicleName({required int id}) {
    var homeController = HomeController().getHomeController;
    String? vName;
    for (var item in homeController.vehiclesList) {
      if (item.id == id) {
        vName = item.vehicleName;
      }
    }
    return vName;
  }

  String _getValidCapacity(String? vC) {
    if (vC?.isNotEmpty ?? false) {
      List<String> splitter = vC!.split('_');
      String first = '${splitter.first} Kg - ';
      String last = '${splitter.last} Kg';
      return '$first $last';
    } else {
      return '';
    }
  }

  Future<void> getAllMyOrders() async {
    BaseResponse response = await DioClient().getRequest(
      endPoint: EndPoints.orders,
    );
    if (response.error == false) {
      ordersList.clear();
      if (response.data != null) {
        for (var item in response.data) {
          var i = OrdersModelData.fromJson(item);
          i.vehicle = getVehicleName(id: i.vehicleId!);
          ordersList.add(i);
        }
      }
      update();
    }
  }

  Future<void> getOrderById({required int orderId}) async {
    isLoading = true;
    update(['singleOrder']);
    Future.delayed(10.milliseconds, () async {
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.ordersById + '/$orderId',
      );
      if (response.error == false) {
        var data = OrdersModelData.fromJson(response.data);
        data.vehicle = getVehicleName(id: data.vehicleId!);
        data.mSourceLocationAddress = await Utils.findAddress(
            latLng: const LatLng(33.65590903398993, 73.06924781000208));
        data.mDestinationLocationAddress = await Utils.findAddress(
            latLng: const LatLng(33.65590903398993, 73.06924781000208));
        data.capacity = _getValidCapacity(data.capacity);
        orderData = data;
        isLoading = false;
        update(['singleOrder']);
      }
    });
  }
}
