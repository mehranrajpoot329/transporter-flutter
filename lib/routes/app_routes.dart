import 'package:get/get.dart';
import 'package:transporter_customer/modules/about_us/about_us.dart';
import 'package:transporter_customer/modules/chat/chat_screen.dart';
import 'package:transporter_customer/modules/code/code_screen.dart';
import 'package:transporter_customer/modules/code/otp_bindding.dart';
import 'package:transporter_customer/modules/contact_us/contact_us.dart';
import 'package:transporter_customer/modules/contact_us/contact_us_binding.dart';
import 'package:transporter_customer/modules/home/bindings/drawer_handler_binding.dart';
import 'package:transporter_customer/modules/home/bindings/home_binding.dart';
import 'package:transporter_customer/modules/home/bindings/location_binding.dart';
import 'package:transporter_customer/modules/home/views/book_now_screen.dart';
import 'package:transporter_customer/modules/home/views/drawer_handler_screen.dart';
import 'package:transporter_customer/modules/login/login.dart';
import 'package:transporter_customer/modules/login/login_binding.dart';
import 'package:transporter_customer/modules/notifications/notification_binding.dart';
import 'package:transporter_customer/modules/notifications/notifications_screens.dart';
import 'package:transporter_customer/modules/orders/order_binding.dart';
import 'package:transporter_customer/modules/orders/orders_screen.dart';
import 'package:transporter_customer/modules/profile/profile_binding.dart';
import 'package:transporter_customer/modules/profile/profile_screen.dart';
import 'package:transporter_customer/modules/profile/profile_update.dart';
import 'package:transporter_customer/modules/share_options/share_options.dart';
import 'package:transporter_customer/modules/splash/splash.dart';
import 'package:transporter_customer/modules/terms_conditions/terms_conditions.dart';
import 'package:transporter_customer/modules/view_rates/view_rates.dart';
import 'package:transporter_customer/modules/view_rates/view_rates_binding.dart';
import 'package:transporter_customer/routes/app_pages.dart';

class AppRoutes {
  static String initialRoute = AppPages.splash;
  static final routes = [
    GetPage(
      name: AppPages.splash,
      page: () => const SplashScreen(),
      binding: LoginBinding(),
    ),
    GetPage(
        name: AppPages.home,
        page: () => const DrawerHandlerScreen(),
        bindings: [
          LocationBinding(),
          DrawerHandlerBinding(),
          HomeBinding(),
        ]),
    GetPage(
      name: AppPages.myOrder,
      page: () => const OrdersScreen(),
      binding: MyOrdersBinding(),
    ),
    GetPage(
      name: AppPages.profileUpdate,
      page: () => const ProfileUpdateScreen(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: AppPages.notifications,
      page: () => const NotificationsScreen(),
      binding: NotificationsBinding(),
    ),
    GetPage(
        name: AppPages.viewRates,
        page: () => const ViewRatesScreen(),
        binding: ViewRatesBinding()),
    GetPage(
      name: AppPages.termsAndSConditions,
      page: () => const TermsAndConditionsScreen(),
    ),
    GetPage(
      name: AppPages.aboutUs,
      page: () => const AboutUsScreen(),
    ),
    GetPage(
        name: AppPages.contactUs,
        page: () => ContactUsScreen(),
        binding: ContactUsBinding()),
    GetPage(
      name: AppPages.shareOptions,
      page: () => const ShareOptionsScreen(),
    ),
    GetPage(
      name: AppPages.profile,
      page: () => ProfileScreen(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: AppPages.phoneLogin,
      page: () => PhoneLoginScreen(),
      preventDuplicates: true,
      binding: LoginBinding(),
    ),
    GetPage(
        name: AppPages.otpCode,
        page: () => CodeScreen(),
        binding: OTPBinding()),
    GetPage(
      name: AppPages.chat,
      page: () => const ChatScreen(),
    ),
    GetPage(
      name: AppPages.bookNow,
      page: () => const BookNowDialogScreen(),
    ),
  ];
}
