class AppPages {
  static const String splash = '/splash';
  static const String phoneLogin = '/phoneLogin';
  static const String otpCode = '/otpCode';
  static const String profile = '/profile';
  static const String home = '/home';
  static const String chat = '/chat';
  static const String notifications = '/notifications';
  static const String myOrder = '/myOrder';
  static const String viewRates = '/viewRates';
  static const String termsAndSConditions = '/termsAndConditions';
  static const String aboutUs = '/aboutUs';
  static const String contactUs = '/contactUs';
  static const String shareOptions = '/shareOptions';
  static const String signOUt = '/signOUt';
  static const String searchLocation = '/searchLocation';
  static const String bookNow = '/bookNow';
  static const String profileUpdate = '/profileUpdate';
}
