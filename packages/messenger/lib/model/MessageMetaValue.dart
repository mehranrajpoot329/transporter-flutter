import 'package:json_annotation/json_annotation.dart';
import 'package:messenger/controllers/json_controller.dart';

import './Message.dart';

part 'MessageMetaValue.g.dart';

@JsonSerializable()
class MessageMetaValue {
  String? key;
  String? value;
  double? messageId;
  Message? message;
  String? text;
  String? videoUrl;
  String? fileUrl;
  String? audioUrl;
  Map<String, dynamic>? extraMap;
  int? type;

  MessageMetaValue({
    this.key,
    this.value,
    this.messageId,
    this.message,
    this.text,
    this.videoUrl,
    this.fileUrl,
    this.audioUrl,
    this.extraMap,
    this.type,
  });

  factory MessageMetaValue.fromJson(Map<String, dynamic>? json) =>
      _$MessageMetaValueFromJson(JsonController.convertToJson(json));

  Map<String, dynamic> toJson() => _$MessageMetaValueToJson(this);
}
